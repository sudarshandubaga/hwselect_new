<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use App\Traits\CountryStateCity;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Country;
use Illuminate\Database\Eloquent\Model;

class Action_on_shortlisted_candidated extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'action_on_shortlisted_candidated';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function get_country($id)
    {
        return $country = Country::where('id', $id)->first();
    }


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
