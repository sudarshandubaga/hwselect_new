<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $with = ['cate'];


    public function printBlogImage($width = 0, $height = 0)
    {
        $logo = (string)$this->image;
        $logo = (!empty($logo)) ? $logo : 'no-no-image.gif';
        if(file_exists('uploads/blogs/'.$logo)){
        	$file_path = 'uploads/blogs/'.$logo;
        }
		else{
			$file_path = '/admin_assets/no-image.png';
		}
        return \ImgUploader::print_image($file_path, $width, $height, $file_path, $this->image);
    }

    public function cate()
    {
        return $this->belongsTo(Blog_category::class);
    }
}