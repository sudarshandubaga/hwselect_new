<?php



namespace App;



use App;

use App\Traits\Lang;

use App\Traits\IsDefault;

use App\Traits\Active;

use App\Traits\Sorted;

use Illuminate\Database\Eloquent\Model;



class Country extends Model

{



    use Lang;

    use IsDefault;

    use Active;

    use Sorted;



    protected $table = 'countries';

    protected $appends = ['country_short_name'];

    public $timestamps = true;

    protected $guarded = ['id'];

    //protected $dateFormat = 'U';

    protected $dates = ['created_at', 'updated_at'];

    public function getCountryShortNameAttribute()
    {
        return empty($this->country_details->sort_name) ? null : trim($this->country_details->sort_name) ;
    }

    public function country_details()
    {
        return $this->hasOne('App\CountryDetail', 'country_id', 'country_id');
    }

    public function states()

    {

        return $this->hasMany('App\State', 'country_id', 'id');

    }



}

