<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Input;
use File;
use Carbon\Carbon;
use ImgUploader;
use Redirect;
use App\Benifits;
use App\Language;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\BenifitsFormRequest;
use App\Http\Controllers\Controller;

class BenifitsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexbenifitss()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.benifits.index')->with('languages', $languages);
    }

    public function createBenifits()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $benifitss = DataArrayHelper::langBenifitsArray();
        return view('admin.benifits.add')
                        ->with('languages', $languages)
                        ->with('benifitss', $benifitss);
    }

    public function storeBenifits(benifitsFormRequest $request)
    {
        $benifits = new Benifits();
        $benifits->benifits = $request->input('benifits');
        $benifits->is_active = $request->input('is_active');
        $benifits->lang = $request->input('lang');
        $benifits->is_default = $request->input('is_default');
        $benifits->save();
        /*         * ************************************ */
        $benifits->sort_order = $benifits->id;
        if ((int) $request->input('is_default') == 1) {
            $benifits->benifit_id = $benifits->id;
        } else {
            $benifits->benifit_id = $request->input('benifit_id');
        }
        $benifits->update();
        flash('benifits has been added!')->success();
        return \Redirect::route('edit.benifits', array($benifits->id));
    }

    public function editBenifits($id)
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $benifitss = DataArrayHelper::langBenifitsArray();
        $benifits = Benifits::findOrFail($id);
        return view('admin.benifits.edit')
                        ->with('languages', $languages)
                        ->with('benifits', $benifits)
                        ->with('benifitss', $benifitss);
    }

    public function updateBenifits($id, benifitsFormRequest $request)
    {
        $benifits = Benifits::findOrFail($id);
        $benifits->benifits = $request->input('benifits');
        $benifits->is_active = $request->input('is_active');
        $benifits->lang = $request->input('lang');
        $benifits->is_default = $request->input('is_default');
        if ((int) $request->input('is_default') == 1) {
            $benifits->benifit_id = $benifits->id;
        } else {
            $benifits->benifit_id = $request->input('benifit_id');
        }
        $benifits->update();
        flash('benifits has been updated!')->success();
        return \Redirect::route('edit.benifits', array($benifits->id));
    }

    public function deleteBenifits(Request $request)
    {
        $id = $request->input('id');
        try {
            $benifits = Benifits::findOrFail($id);
            if ((bool) $benifits->is_default) {
                Benifits::where('benifit_id', '=', $benifits->benifit_id)->delete();
            } else {
                $benifits->delete();
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    public function fetchbenifitssData(Request $request)
    {
        $benifitss = Benifits::select(['benifits.id', 'benifits.benifits', 'benifits.is_active', 'benifits.lang', 'benifits.is_default', 'benifits.created_at', 'benifits.updated_at'])->sorted();
        return Datatables::of($benifitss)
                        ->filter(function ($query) use ($request) {
                            if ($request->has('benifits') && !empty($request->benifits)) {
                                $query->where('benifits.benifits', 'like', "%{$request->get('benifits')}%");
                            }
                            if ($request->has('lang') && !empty($request->get('lang'))) {
                                $query->where('benifits.lang', 'like', "%{$request->get('lang')}%");
                            }
                            if ($request->has('is_active') && $request->get('is_active') != -1) {
                                $query->where('benifits.is_active', '=', "{$request->get('is_active')}");
                            }
                        })
                        ->addColumn('benifits', function ($benifitss) {
                            $benifits = \Illuminate\Support\Str::limit($benifitss->benifits, 100, '...');
                            $direction = MiscHelper::getLangDirection($benifitss->lang);
                            return '<span dir="' . $direction . '">' . $benifits . '</span>';
                        })
                        ->addColumn('action', function ($benifitss) {
                            /*                             * ************************* */
                            $activeTxt = 'Make Active';
                            $activeHref = 'makeActive(' . $benifitss->id . ');';
                            $activeIcon = 'square-o';
                            if ((int) $benifitss->is_active == 1) {
                                $activeTxt = 'Make InActive';
                                $activeHref = 'makeNotActive(' . $benifitss->id . ');';
                                $activeIcon = 'square-o';
                            }
                            $title = "'".$benifitss->benifits."'";
                            return '
				<div class="btn-group">
					<button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu">
						<li>
							<a href="' . route('edit.benifits', ['id' => $benifitss->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
						</li>						
						<li>
							<a href="javascript:void(0);" onclick="deletebenifits(' . $benifitss->id . ', ' . $benifitss->is_default . ','.$title.');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
						</li>
						<li>
						<a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $benifitss->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
						</li>																																		
					</ul>
				</div>';
                        })
                        ->rawColumns(['benifits', 'action'])
                        ->setRowId(function($benifitss) {
                            return 'benifitsDtRow' . $benifitss->id;
                        })
                        ->make(true);
        //$query = $dataTable->getQuery()->get();
        //return $query;
    }

    public function makeActiveBenifits(Request $request)
    {
        $id = $request->input('id');
        try {
            $benifits = Benifits::findOrFail($id);
            $benifits->is_active = 1;
            $benifits->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotActiveBenifits(Request $request)
    {
        $id = $request->input('id');
        try {
            $benifits = Benifits::findOrFail($id);
            $benifits->is_active = 0;
            $benifits->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function sortbenifitss()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.benifits.sort')->with('languages', $languages);
    }

    public function benifitssortData(Request $request)
    {
        $lang = $request->input('lang');
        $benifitss = Benifits::select('benifits.id', 'benifits.benifits', 'benifits.sort_order')
                ->where('benifits.lang', 'like', $lang)
                ->orderBy('benifits.sort_order')
                ->get();
        $str = '<ul id="sortable">';
        if ($benifitss != null) {
            foreach ($benifitss as $benifits) {
                $str .= '<li id="' . $benifits->id . '"><i class="fa fa-sort"></i>' . $benifits->benifits . '</li>';
            }
        }
        echo $str . '</ul>';
    }

    public function benifitssortUpdate(Request $request)
    {
        $benifitsOrder = $request->input('benifitsOrder');
        $benifitsOrderArray = explode(',', $benifitsOrder);
        $count = 1;
        foreach ($benifitsOrderArray as $benifitsId) {
            $benifits = Benifits::find($benifitsId);
            $benifits->sort_order = $count;
            $benifits->update();
            $count++;
        }
    }

}
