<?php



namespace App\Http\Controllers\Company\Auth;



use Auth;
use Redirect;

use App\Company;
use App\Subscription;
use App\Verify_tokens;
use App\SiteSetting;

use App\Http\Requests;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\RegistersUsers;

use Jrean\UserVerification\Traits\VerifiesUsers;

use Jrean\UserVerification\Facades\UserVerification;

use App\Http\Requests\Front\CompanyFrontRegisterFormRequest;

use Illuminate\Auth\Events\Registered;

use App\Events\CompanyRegistered;
use App\Mail\Newsletter;
use Mail;



class RegisterController extends Controller

{

    /*

      |--------------------------------------------------------------------------

      | Register Controller

      |--------------------------------------------------------------------------

      |

      | This controller handles the registration of new users as well as their

      | validation and creation. By default this controller uses a trait to

      | provide this functionality without requiring any additional code.

      |

     */



use RegistersUsers;

    use VerifiesUsers;



    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = '/company-home';

    protected $userTable = 'companies';

    protected $redirectIfVerified = '/company-home';

    protected $redirectAfterVerification = '/company-home';



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('company.guest', ['except' => ['getVerification', 'getVerificationError']]);

    }



    /**

     * Get the guard to be used during registration.

     *

     * @return \Illuminate\Contracts\Auth\StatefulGuard

     */

    protected function guard()

    {

        return Auth::guard('company');

    }

    public function saveForm(Request $request)

    {
        $request->session()->put('emp_form_data', $request->all());
        //dd($request);
        echo 'done';

    }

    public function register(CompanyFrontRegisterFormRequest $request)

    {
        $verif = Verify_tokens::where('auth_time','like',trim($request->auth_time))->first();
        //dd($request);

        $siteSetting = SiteSetting::findOrFail(1272);
        if(null!==($siteSetting->verify_registration_phone_number)){
          if(null==($verif)){
            return redirect()->back()
                    ->withInput($request->input())
                    ->with('message.not_verified_emp','success');
          }
        }
        
        $company = new Company();

        $company->name = $request->input('name');

        $company->email = $request->input('email');
        $company->ceo = $request->input('ceo');

        $company->phone = $request->input('phone');

        $company->phone_code = $request->input('mobile_num_code');
        $company->phone_num_code = $request->input('mobile_num_code');

        $company->password = bcrypt($request->input('password'));

        if(null==($siteSetting->verify_registration_phone_number)){
            $company->phone_verify = 'yes';
        }

        $company->is_active = 0;
        if($request->input('is_subscribed')){
            $company->is_subscribed = 1;
        }

        


        $company->verified = 0;

        $company->save();

        /*         * ******************** */

        $company->slug = \Illuminate\Support\Str::slug($company->name, '-') . '-' . $company->id;

        $company->update();

        /*         * ******************** */
        //dd($company);
        $request->session()->put('emp_form_data', '');

        event(new Registered($company));

        event(new CompanyRegistered($company));

        //$this->guard()->login($company);

        UserVerification::generate($company);

        UserVerification::send($company, 'Company Verification', config('mail.recieve_to.address'), config('mail.recieve_to.name'));

        if($request->input('is_subscribed')){
            $subscription = new Subscription();
            $subscription->email = $request->input('email');
            $subscription->name = $request->input('name');
            $subscription->type = 'company';
            $subscription->save();
            $data['name'] = $request->input('name');
            $data['email'] = $request->input('email');
            $data['type'] = 'company';
            //Mail::send(new Newsletter($data));
        }

        //$request->session()->flash('message.added', 'success');
         return Redirect::route('login')->with('message.added','success')->with('email',$request->input('email'));

    }



}

