<?php

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Modules_data;
use App\Cms;
use App\Menu;


if (! function_exists('unique_slug')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function unique_slug($string,$table,$field='slug',$key=NULL,$value=NULL)
    {
            $slug = url_title($string);
            $slug = strtolower($slug);
            $i = 0;
            $params = array ();
            $params[$field] = $slug;

            if($key)$params["$key !="] = $value;

            while (DB::table($table)->where('slug',$params)->first())
            {  
                if (!preg_match ('/-{1}[0-9]+$/', $slug ))
                    $slug .= '-' . ++$i;
                else
                    $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );

                $params[$field] = $slug;
            }  
            return $slug;  
    }
}
if (! function_exists('url_title')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function url_title($str, $separator = '-', $lowercase = FALSE)
    {
            if ($separator === 'dash')
        {
            $separator = '-';
        }
        elseif ($separator === 'underscore')
        {
            $separator = '_';
        }

        $q_separator = preg_quote($separator, '#');

        $trans = array(
            '&.+?;'         => '',
            '[^a-z0-9 _-]'      => '',
            '\s+'           => $separator,
            '('.$q_separator.')+'   => $separator
        );

        $str = strip_tags($str);
        foreach ($trans as $key => $val)
        {
            $str = preg_replace('#'.$key.'#i', $val, $str);
        }

        if ($lowercase === TRUE)
        {
            $str = strtolower($str);
        }

        return trim(trim($str, $separator));
    }
} 
//if (! function_exists('get_menus')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    /*function get_menus()
    {
        $manues = '';
        $classli='';
        $spancart='';
        $parent_menus = Modules_data::where('modules_id',10)->where('status','active')->orderBy('id', 'asc')->get();
        if(null!==($parent_menus)){
        foreach($parent_menus as $menu){
                   $manues.='<li>';
                   $manues.='<a href="'.$menu->description.'">'.$menu->title.'</a>';
                   $manues.='</li>';
              }
        }      
               

        return $manues;
    }
}*/


if (! function_exists('get_menus')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function get_menus()
    {
        $manues = '';
        $classli='';
        $spancart='';
        $parent_menus = Menu::where('parent_id',0)->orderBy('order', 'asc')->get();

        foreach($parent_menus as $menu){
             if (is_child($menu->id) == TRUE) {
                $classli='class="dropdown dropnav"';
                $spancart='<i class="fas fa-caret-down"></i>';
            }
            else{
                $classli='class="nav-item"';
                $spancart='';
            }
            if($menu->is_external=='N'){
                $menu_url=url('/').'/'.$menu->url;
            }else{
                $menu_url=url('/').'/'.$menu->url;
            }
          $manues.='<li '.$classli.'>';
          $manues.='<a href="'.$menu_url.'" class="nav-link">'.$menu->title.''.$spancart.'</a>';
          if (is_child($menu->id) == TRUE) {
              $manues.='<ul class="dropdown-menu">';
              if(null!==($menu->submenus)){
                foreach($menu->submenus as $submenu){
                  if($submenu->is_external=='N'){
                        $submenu_url=url('/').'/'.$submenu->url;
                    }else{
                        $submenu_url=url('/').'/'.$submenu->url;
                    }
                   $manues.='<li>';
                   $manues.='<a href="'.$submenu_url.'" class="nav-link">'.$submenu->title.'</a>';
                   $manues.='</li>';
                }
              }
              
              $manues.='</ul>'; 
          }
         $manues.='</li>';
        }         

        return $manues;
    }
}
if (! function_exists('is_child')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function is_child($id)
    {
        $child_menus = Menu::where('parent_id',$id)->first();
        if ($child_menus) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

if (! function_exists('dropdown')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function dropdown($id)
    {
        $array = Modules_data::select('title')->where('modules_id',$id)->where('status','active')->pluck('title', 'title')->toArray();
        return $array;
    }
} 

if (! function_exists('get_menus_seeker')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function get_menus_seeker()
    {
        $manues = '';
        $classli='';
        $spancart='';
        $parent_menus = Modules_data::where('modules_id',11)->where('status','active')->orderBy('id', 'asc')->get();
        if(null!==($parent_menus)){
        foreach($parent_menus as $menu){
            if($menu->id==30){
                if(Auth::user()){
                    $m = url('/my-profile#cvs');
                }else{
                    $m = $menu->description;
                }

                $manues.='<li>';  
                $manues.='<a href="'.$m.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }else{
                 $manues.='<li>';  
                $manues.='<a href="'.$menu->description.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }


                   
              }
        }      
               

        return $manues;
    }
}

if (! function_exists('get_menus_footer')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function get_menus_footer()
    {
        $manues = '';
        $classli='';
        $spancart='';
        $parent_menus = Modules_data::where('modules_id',10)->where('status','active')->orderBy('id', 'asc')->get();
        if(null!==($parent_menus)){
        foreach($parent_menus as $menu){
            if($menu->id==30){
                if(Auth::user()){
                    $m = url('/').'/'.'my-profile#cvs'; 
                    //url('/my-profile#cvs');
                }else{
                    //$m = $menu->description;
                     $m=url('/').'/'.$menu->description;
                }

                $manues.='<li>';  
                $manues.='<a href="'.$m.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }else{
                 $manues.='<li>';  
                $manues.='<a href="'.$menu->description.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }


                   
              }
        }      
               

        return $manues;
    }
}

if (! function_exists('get_menus_employerr')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function get_menus_employerr()
    {
        $manues = '';
        $classli='';
        $spancart='';
        $parent_menus = Modules_data::where('modules_id',12)->where('status','active')->orderBy('id', 'asc')->get();
        if(null!==($parent_menus)){
        foreach($parent_menus as $menu){


            
                   if($menu->id==32){
                if(Auth::guard('company')->user()){
                    //$m = url('/post-job');
                    $m = url('/').'/'.'post-job'; 
                }else{
                    //$m = $menu->description;
                    $m = url('/').'/'.$menu->description; 
                }

                $manues.='<li>';  
                $manues.='<a href="'.$m.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }else{
                 $manues.='<li>';  
                $manues.='<a href="'.$menu->description.'">'.$menu->title.'</a>';
                $manues.='</li>';
            }
              }
        }      
               

        return $manues;
    }
}

if (! function_exists('is_child')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function is_child($id)
    {
        $child_menus = Menu::where('parent_id',$id)->first();
        if ($child_menus) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
if (! function_exists('get_widget')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param  array  $array
     * @return array
     */
    function get_widget($id)
    {
        $array = DB::table('widgets')->where("id",$id)->first();
        return $array;
    }
} 
