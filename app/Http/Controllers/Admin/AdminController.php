<?php

namespace App\Http\Controllers\Admin;

use Config;
use Auth;
use DB;
use App\Admin;
use App\ContactMessage;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model0tFoundException;
use DataTables;
use App\Role;
use App\Http\Requests\AdminFormRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexContactEnquiries(Request $request)
    {
        $enquiries = ContactMessage::where('type', $request->type)->latest()->paginate(20);
        return view('admin.admin.contact_enquiry', compact('enquiries'));
    }

    public function multipleDeletesContactEnquiries(Request $request)
    {
        foreach ($request->check as $key => $id) {
            ContactMessage::find($id)->delete();
        }

        return redirect()->back()->with('success', 'Success! Selected record(s) found.');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAdminUsers()
    {
        return view('admin.admin.index');
    }

    public function createAdminUser()
    {
        $roles = Role::select('role_name', 'id')->orderBy('role_name')->pluck('role_name', 'id')->toArray();
        /*
          print_r($roles);
          print_r(['0' => 'Select a Role']+$roles);exit;
         */
        return view('admin.admin.create')->with('roles', $roles);
    }

    public function storeAdminUser(AdminFormRequest $request)
    {
        $user = new Admin;
        $user->name = $request->name;
        $user->email = $request->email;
        if(isset($request->slider)){
            $user->slider = $request->slider;
        }else{
            $user->slider = '0';
        }

        if(isset($request->admin_users)){
            $user->admin_users = $request->admin_users;
        }else{
            $user->admin_users = '0';
        }

        if(isset($request->job)){
            $user->job = $request->job;
        }else{
            $user->job = '0';
        }

        if(isset($request->company)){
            $user->company = $request->company;
        }else{
            $user->company = '0';
        }

        if(isset($request->site_user)){
            $user->site_user = $request->site_user;
        }else{
            $user->site_user = '0';
        }

        if(isset($request->cms)){
            $user->cms = $request->cms;
        }else{
            $user->cms = '0';
        }

        if(isset($request->blogs)){
            $user->blogs = $request->blogs;
        }else{
            $user->blogs = '0';
        }

        if(isset($request->widget)){
            $user->widget = $request->widget;
        }else{
            $user->widget = '0';
        }

        if(isset($request->seo)){
            $user->seo = $request->seo;
        }else{
            $user->seo = '0';
        }

        if(isset($request->faq)){
            $user->faq = $request->faq;
        }else{
            $user->faq = '0';
        }

        if(isset($request->video)){
            $user->video = $request->video;
        }else{
            $user->video = '0';
        }

        if(isset($request->testimonial)){
            $user->testimonial = $request->testimonial;
        }else{
            $user->testimonial = '0';
        }

        if(isset($request->slider)){
            $user->slider = $request->slider;
        }else{
            $user->slider = '0';
        }

        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();
        /*         * ******************** */
        Mail::send('admin.admin.emails.new_admin_user_created', ['user' => $user], function ($msg) use ($user) {
            $msg->from(config('mail.recieve_to.address'), config('mail.recieve_to.name'));
            $msg->to($user->email, $user->name)->subject('Please set your password to ' . config('app.name') . ' admin panel.');
        });
        /*         * ******************** */
        flash('New Admin User has been created!')->success();
        return \Redirect::route('edit.admin.user', array($user->id));
    }

    public function editAdminUser($id)
    {
        $user = Admin::findOrFail($id);
        $roles = Role::select('role_name', 'id')->orderBy('role_name')->pluck('role_name', 'id')->toArray();
        
        return view('admin.admin.edit')->with('roles', $roles)->with('user', $user);
    }

    public function updateAdminUser($id, AdminFormRequest $request)
    {

        //dd($request);
        $user = Admin::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if(isset($request->slider)){
            $user->slider = $request->slider;
        }else{
            $user->slider = '0';
        }

        if(isset($request->admin_users)){
            $user->admin_users = $request->admin_users;
        }else{
            $user->admin_users = '0';
        }

        if(isset($request->job)){
            $user->job = $request->job;
        }else{
            $user->job = '0';
        }

        if(isset($request->company)){
            $user->company = $request->company;
        }else{
            $user->company = '0';
        }

        if(isset($request->site_user)){
            $user->site_user = $request->site_user;
        }else{
            $user->site_user = '0';
        }

        if(isset($request->cms)){
            $user->cms = $request->cms;
        }else{
            $user->cms = '0';
        }

        if(isset($request->blogs)){
            $user->blogs = $request->blogs;
        }else{
            $user->blogs = '0';
        }

        if(isset($request->widget)){
            $user->widget = $request->widget;
        }else{
            $user->widget = '0';
        }

        if(isset($request->seo)){
            $user->seo = $request->seo;
        }else{
            $user->seo = '0';
        }

        if(isset($request->faq)){
            $user->faq = $request->faq;
        }else{
            $user->faq = '0';
        }

        if(isset($request->video)){
            $user->video = $request->video;
        }else{
            $user->video = '0';
        }

        if(isset($request->testimonial)){
            $user->testimonial = $request->testimonial;
        }else{
            $user->testimonial = '0';
        }

        if(isset($request->slider)){
            $user->slider = $request->slider;
        }else{
            $user->slider = '0';
        }

        
        
        if (!empty($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->role_id = $request->role_id;
        $user->save();
        flash('Admin User has been updated!')->success();
        return \Redirect::route('edit.admin.user', array($user->id));
    }

    public function deleteAdminUser(Request $request)
    {
        $id = $request->input('id');
        try {
            $user = Admin::findOrFail($id);
            $user->delete();
            echo 'ok';
        } catch (Model0tFoundException $e) {
            echo '0tok';
        }
    }

    public function fetchAdminUsersData()
    {
        $users = Admin::join('roles', 'admins.role_id', '=', 'roles.id')
                ->select('admins.id', 'admins.name', 'admins.email', 'roles.role_name');
        return Datatables::of($users)
                        ->addColumn('action', function ($user) {
                            $name = "'".$user->name."'";
                            $role = "'".$user->role_name."'";
                            return '<a href="' . route('edit.admin.user', ['id' => $user->id]) . '" class="btn blue"><i class="glyphicon glyphicon-edit"></i> Edit</a><a href="javascript:void(0);" onclick="delete_user(' . $user->id . ','.$name.','.$role.');" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>';
                        })
                        ->removeColumn('password')
                        ->setRowId(function($user) {
                            return 'admin_user_dt_row_' . $user->id;
                        })
                        ->make(true);
    }

}
