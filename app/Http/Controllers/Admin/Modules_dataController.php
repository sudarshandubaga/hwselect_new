<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modules;
use App\Modules_data;
use App\Country;
use App\Countries;
use Image;
use Str;
use Yajra\DataTables\DataTables;

use Illuminate\Validation\Rule;

class Modules_dataController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = '')
    {
        $data = array();
        $data['module'] = Modules::where('slug', $slug)->first();
        return view('admin.modules_data.view_modules_data')->with($data);
    }
    public function add_module_data($slug)
    {
        $data = array();
        $data['module'] = Modules::where('slug', $slug)->first();
        $data['countries'] = Countries::select('allcountries.country', 'allcountries.country')->isDefault()->active()->sorted()->pluck('allcountries.country', 'allcountries.country')->toArray();
        return view('admin.modules_data.add_module_data')->with($data);
    }

    public function edit_data($id)
    {
        $data = array();
        $data['data'] = Modules_data::where('id', $id)->first();
        $data['module'] = Modules::findorfail($data['data']->modules_id);
        $data['countries'] = Countries::select('allcountries.country', 'allcountries.country')->isDefault()->active()->sorted()->pluck('allcountries.country', 'allcountries.country')->toArray();
        return view('admin.modules_data.edit_module_data')->with($data);
    }
    public function post_data(Request $request)
    {
        //dd($request);
        $module_cms = Modules::findorfail($request->module_id);
        if ($module_cms->page_description == 'on') {
            $this->validate($request, [
                'title' => [
                    'required',
                    Rule::unique('modules_data')->where(function ($q) use ($request) {
                        $q->where('modules_id', $request->module_id);
                    })
                ],
                'description' => 'required',
                'module_id' => 'required',
            ], [
                'title.required' => 'Title is required.',
                'description.required' => 'Description is required.',
            ]);
        } else {
            $this->validate($request, [
                'title' => [
                    'required',
                    Rule::unique('modules_data')->where(function ($q) use ($request) {
                        $q->where('modules_id', $request->module_id);
                    })
                ],
                'module_id' => 'required',
            ], [
                'title.required' => 'Title is required.',
            ]);
        }
        //dd($request); 

        $slug = Str::slug($request->title, '-');
        $slugs = unique_slug($slug, 'modules_data', $field = 'slug', $key = NULL, $value = NULL);
        $module = new Modules_data();
        $module->title = $request->title;
        if ($module_cms->page_description == 'on') {
            $module->description = $request->description;
        }
        if($request->module_id==1){
            $module->status = 'blocked';
        }
        $module->modules_id = $request->module_id;
        $module->slug = $slugs;
        $image = $request->file('image');
        if ($image != '') {
            $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());
            $input['imagename'] = $nameonly . '_' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize($request->thumbnail_height, $request->thumbnail_width, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['imagename']);

            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $input['imagename']);
            $module->image = $input['imagename'];
        }
        $module->save();
        // $data['module'] = $module;
        //dd($module);
        if ($module->save() == true) {
            $request->session()->flash('message.added', 'success');
            $request->session()->flash('message.content', 'A ' . $module_cms->module_term . ' has been successfully Created!');
        }
        return redirect(route('modules-data', $module_cms->slug));
    }
    public function update_data(Request $request)
    {
        $module_cms = Modules::findorfail($request->module_id);
        if ($module_cms->page_description == 'on') {
            $this->validate($request, [
                'title' => [
                    'required',
                    Rule::unique('modules_data')->where(function ($q) use ($request) {
                        $q->where('modules_id', $request->module_id)->where('id', '!=', $request->id);
                    })
                ],
                'description' => 'required',
                'module_id' => 'required',
            ], [
                'title.required' => 'Title is required.',
                'description.required' => 'Description is required.',
            ]);
        } else {
            $this->validate($request, [
                'title' => [
                    'required',
                    Rule::unique('modules_data')->where(function ($q) use ($request) {
                        $q->where('modules_id', $request->module_id)->where('id', '!=', $request->id);
                    })
                ],
                'module_id' => 'required',
            ], [
                'title.required' => 'Title is required.',
            ]);
        }
        $slug = Str::slug($request->title, '-');
        $slugs = unique_slug($slug, 'modules_data', $field = 'slug', $key = NULL, $value = NULL);
        $module = Modules_data::findorfail($request->id);
        $module->title = $request->title;
        if ($module_cms->page_description == 'on') {
            $module->description = $request->description;
        }
        $module->modules_id = $request->module_id;
        $module->slug = $slugs;
        $image = $request->file('image');
        if ($image != '') {
            $nameonly = preg_replace('/\..+$/', '', $request->image->getClientOriginalName());
            $input['imagename'] = $nameonly . '_' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize($request->thumbnail_height, $request->thumbnail_width, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['imagename']);

            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $input['imagename']);
            $module->image = $input['imagename'];
        }
        $module->update();
        // $data['module'] = $module;
        //dd($module);
        if ($module->update() == true) {
            $request->session()->flash('message.added', 'success');
            $request->session()->flash('message.content', 'A ' . $module_cms->module_term . ' has been successfully Updated!');
        }
        return redirect(route('modules-data', $module_cms->slug));
    }
    public function destroy($id)
    {
        $module = Modules_data::findOrFail($id);
        $module->delete();
        $module_cms = Modules::findorfail($module->modules_id);
        return redirect(route('modules-data', $module_cms->slug))->with('warning', 'A ' . $module_cms->module_term . ' has been successfully Deleted!');
    }

    public function update_status($id = '', $current_staus = '')
    {
        if ($id == '') {
            echo 'error';
            exit;
        }
        if ($current_staus == '') {
            echo 'invalid current status provided.';
            exit;
        }
        if ($current_staus == 'active')
            $new_status = 'blocked';
        else
            $new_status = 'active';
        $module = Modules_data::findOrFail($id);
        $module->status = $new_status;
        $module->update();
        echo $new_status;
        exit;
    }

    public function ips()
    {
        $data = array();
        $data['module'] = Modules::where( 'slug', 'ips')->first();
        return view('admin.modules_data.view_ips')->with($data);
    }

    public function fetchIps(Request $request)
    {
        $countries = Modules_data::whereHas('modules', function ($q) {
            $q->where('slug', 'ips');
        })->select(['id', 'title', 'status', 'created_at'])->orderBy('title');

        return Datatables::of($countries)

            ->filter(function ($query) use ($request) {

                if ($request->has('country') && !empty($request->country)) {

                    $query->where('title', 'like', "{$request->get('country')}%");

                }

                if ($request->has('status') && !empty($request->status)) {

                    $query->where('status', 'like', "{$request->get('status')}");

                }

            })

            ->addColumn('status', function ($ips) {
                $html = '<a class="waves-effect status waves-light" onclick="update_status('.$ips->id.');" href="javascript:void(0);" id="sts_'.$ips->id.'">';
                if($ips->status == 'active') {
                    $html .= '<span class="btn btn-success">'. $ips->status .'</span>';
                }
                else {
                    $html .= '<span class="btn btn-warning">'. $ips->status .'</span>';

                }
                $html .= '</a>';

                return $html;
            })

            ->addColumn('created_date', function ($ips) {
                return date('F d, Y', strtotime($ips->created_at));
            })

            ->addColumn('action', function ($ips) {

                /*                             * ************************* */

                $activeTxt = 'Make Active';

                $activeHref = 'makeActive(' . $ips->id . ');';

                $activeIcon = 'square-o';

                if ((int) $ips->is_active == 1) {

                    $activeTxt = 'Make InActive';

                    $activeHref = 'makeNotActive(' . $ips->id . ');';

                    $activeIcon = 'square-o';

                }

                return '

    <div class="btn-group">

        <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action

            <i class="fa fa-angle-down"></i>

        </button>

        <ul class="dropdown-menu">

            <li>

                <a href="' . route('edit-module-data',$ips->id) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>

            </li>						

            <li>

                <a class="delete" data-title="{{$data->title}}" href="' . route('delete-data',$ips->id) .'" role="button"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a>

            </li>
        </ul>

    </div>';

            })

            ->rawColumns(['created_date', 'action', 'status'])

            ->setRowId(function($countries) {

                return 'countryDtRow' . $countries->id;

            })

            ->setRowData([
                'data-id'   => function($countries) {

                    return $countries->id;

                }
            ])

            ->make(true);
    }
}
