<?php



namespace App\Http\Controllers\Admin;



use Auth;

use DB;

use Input;

use File;

use Carbon\Carbon;

use ImgUploader;

use Redirect;

use App\Country;

use App\CountryDetail;

use App\SiteSetting;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use DataTables;

use App\Http\Requests\SiteSettingFormRequest;

use App\Http\Controllers\Controller;

use App\Helpers\DataArrayHelper;



class SiteSettingController extends Controller

{



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //

    }



    public function editSiteSetting()

    {

        $id = 1272;

        $countries = DataArrayHelper::defaultCountriesArray();

        $currency_codes = CountryDetail::select('countries_details.code')->orderBy('countries_details.code')->pluck('countries_details.code', 'countries_details.code')->toArray();

        $all_countries = CountryDetail::get()->sortBy('country_name')->pluck('country_name', 'country_id');

        $mail_drivers = [

            'smtp' => 'SMTP',

            'mail' => 'Mail',

            'sendmail' => 'SendMail',

            'mailgun' => 'MailGun',

            'mandrill' => 'Mandrill',

            'ses' => 'Amazon SES',

            'sparkpost' => 'Sparkpost',

            'log' => 'Log'

        ];

        $siteSetting = SiteSetting::findOrFail($id);

        return view('admin.site_setting.edit')

                        ->with('siteSetting', $siteSetting)

                        ->with('mail_drivers', $mail_drivers)

                        ->with('countries', $countries)

                        ->with('all_countries', $all_countries)

                        ->with('currency_codes', $currency_codes);

    }



    public function updateSiteSetting(SiteSettingFormRequest $request)

    {

        $id = 1272;

        $siteSetting = SiteSetting::findOrFail($id);

        if ($request->hasFile('image')) {

            $this->deleteSiteSettingImage($id);

            $image_name = $request->input('site_name');

            $fileName = ImgUploader::UploadImage('sitesetting_images', $request->file('image'), $image_name);

            $siteSetting->site_logo = $fileName;

        }

        if ($request->hasFile('favicon')) {

            $file = $request->file('favicon');

            $file->move(public_path(), 'favicon.ico');

        }

        $siteSetting->site_name = $request->input('site_name');

        $siteSetting->site_slogan = $request->input('site_slogan');

        $siteSetting->site_phone_primary = $request->input('site_phone_primary');

        $siteSetting->site_phone_secondary = $request->input('site_phone_secondary');

        $siteSetting->mail_from_address = $request->input('mail_from_address');

        $siteSetting->mail_from_name = $request->input('mail_from_name');

        $siteSetting->mail_to_address = $request->input('mail_to_address');

        $siteSetting->mail_to_name = $request->input('mail_to_name');

        $siteSetting->mail_cc_address = $request->input('mail_cc_address');
        
        $siteSetting->mail_cc_name = $request->input('mail_cc_name');

        $siteSetting->default_country_id = $request->input('default_country_id');

        $siteSetting->country_specific_site = $request->input('country_specific_site');

        $siteSetting->default_currency_code = $request->input('default_currency_code');

        $siteSetting->site_street_address = $request->input('site_street_address');

        $siteSetting->site_google_map = $request->input('site_google_map');

        $siteSetting->mail_driver = $request->input('mail_driver');

        $siteSetting->mail_host = $request->input('mail_host');

        $siteSetting->mail_port = $request->input('mail_port');

        $siteSetting->mail_encryption = $request->input('mail_encryption');

        $siteSetting->mail_username = $request->input('mail_username');

        $siteSetting->mail_password = $request->input('mail_password');

        $siteSetting->mail_sendmail = $request->input('mail_sendmail');

        $siteSetting->mail_pretend = $request->input('mail_pretend');

        $siteSetting->mailgun_domain = $request->input('mailgun_domain');

        $siteSetting->mailgun_secret = $request->input('mailgun_secret');

        $siteSetting->mandrill_secret = $request->input('mandrill_secret');

        $siteSetting->sparkpost_secret = $request->input('sparkpost_secret');

        $siteSetting->ses_key = $request->input('ses_key');

        $siteSetting->ses_secret = $request->input('ses_secret');

        $siteSetting->ses_region = $request->input('ses_region');

        $siteSetting->facebook_address = $request->input('facebook_address');

        $siteSetting->twitter_address = $request->input('twitter_address');

        $siteSetting->google_plus_address = $request->input('google_plus_address');

        $siteSetting->youtube_address = $request->input('youtube_address');

        $siteSetting->instagram_address = $request->input('instagram_address');

        $siteSetting->pinterest_address = $request->input('pinterest_address');

        $siteSetting->linkedin_address = $request->input('linkedin_address');

        $siteSetting->tumblr_address = $request->input('tumblr_address');

        $siteSetting->linkedin_app_id = $request->input('linkedin_app_id');

        $siteSetting->linkedin_app_secret = $request->input('linkedin_app_secret');

        
        $siteSetting->ganalytics = $request->input('ganalytics');
        $siteSetting->google_tag_manager_for_head = $request->input('google_tag_manager_for_head');
        $siteSetting->google_tag_manager_for_body = $request->input('google_tag_manager_for_body');

        $siteSetting->flickr_address = $request->input('flickr_address');

        $siteSetting->index_page_below_top_employes_ad = $request->input('index_page_below_top_employes_ad');

        $siteSetting->above_footer_ad = $request->input('above_footer_ad');

        $siteSetting->dashboard_page_ad = $request->input('dashboard_page_ad');

        $siteSetting->cms_page_ad = $request->input('cms_page_ad');

        $siteSetting->listing_page_vertical_ad = $request->input('listing_page_vertical_ad');

        $siteSetting->listing_page_horizontal_ad = $request->input('listing_page_horizontal_ad');

        $siteSetting->nocaptcha_sitekey = $request->input('nocaptcha_sitekey');

        $siteSetting->nocaptcha_secret = $request->input('nocaptcha_secret');

        $siteSetting->facebook_app_id = $request->input('facebook_app_id');

        $siteSetting->facebeek_app_secret = $request->input('facebeek_app_secret');

        $siteSetting->google_app_id = $request->input('google_app_id');

        $siteSetting->google_app_secret = $request->input('google_app_secret');

        $siteSetting->twitter_app_id = $request->input('twitter_app_id');

        $siteSetting->twitter_app_secret = $request->input('twitter_app_secret');

        $siteSetting->paypal_account = $request->input('paypal_account');

        $siteSetting->paypal_client_id = $request->input('paypal_client_id');

        $siteSetting->paypal_secret = $request->input('paypal_secret');

        $siteSetting->paypal_live_sandbox = $request->input('paypal_live_sandbox');

        $siteSetting->stripe_key = $request->input('stripe_key');

        $siteSetting->stripe_secret = $request->input('stripe_secret');
        
        $siteSetting->is_paypal_active = $request->input('is_paypal_active');

        $siteSetting->is_stripe_active = $request->input('is_stripe_active');
        $siteSetting->is_newsletter_active = $request->input('is_newsletter_active');
        if($request->input('is_jobseeker_package_active'))
        $siteSetting->is_jobseeker_package_active = $request->input('is_jobseeker_package_active');

        if($request->input('is_company_package_active'))
		$siteSetting->is_company_package_active = $request->input('is_company_package_active');

        $siteSetting->is_slider_active = $request->input('is_slider_active');
        $siteSetting->is_testimonial_active = $request->input('is_testimonial_active');
        $siteSetting->is_blog_active = $request->input('is_blog_active');

		$siteSetting->mailchimp_api_key = $request->input('mailchimp_api_key');

        $siteSetting->mailchimp_list_name = $request->input('mailchimp_list_name');

		$siteSetting->mailchimp_list_id = $request->input('mailchimp_list_id');
		$siteSetting->verify_registration_phone_number = $request->input('verify_registration_phone_number');
        $siteSetting->default_months = $request->input('default_months');

        $siteSetting->show_social_links = $request->input('show_social_links') ?: 0;



         if(isset($request->index_page_below_top_employes_ads)){
            $siteSetting->index_page_below_top_employes_ads = $request->index_page_below_top_employes_ads;
        }else{
            $siteSetting->index_page_below_top_employes_ads = '0';
        }


         if(isset($request->index_page_below_cities_ads)){
            $siteSetting->index_page_below_cities_ads = $request->index_page_below_cities_ads;
        }else{
            $siteSetting->index_page_below_cities_ads = '0';
        }


         if(isset($request->hire_it_talent_ads)){
            $siteSetting->hire_it_talent_ads = $request->hire_it_talent_ads;
        }else{
            $siteSetting->hire_it_talent_ads = '0';
        }

         if(isset($request->why_us_ads)){
            $siteSetting->why_us_ads = $request->why_us_ads;
        }else{
            $siteSetting->why_us_ads = '0';
        }

         if(isset($request->jobs_ads)){
            $siteSetting->jobs_ads = $request->jobs_ads;
        }else{
            $siteSetting->jobs_ads = '0';
        }



        if(isset($request->jobs_detail_ads)){
            $siteSetting->jobs_detail_ads = $request->jobs_detail_ads;
        }else{
            $siteSetting->jobs_detail_ads = '0';
        }


         if(isset($request->contact_us_ads)){
            $siteSetting->contact_us_ads = $request->contact_us_ads;
        }else{
            $siteSetting->contact_us_ads = '0';
        }


         if(isset($request->blog_ads)){
            $siteSetting->blog_ads = $request->blog_ads;
        }else{
            $siteSetting->blog_ads = '0';
        }



         if(isset($request->blog_details_ads)){
            $siteSetting->blog_details_ads = $request->blog_details_ads;
        }else{
            $siteSetting->blog_details_ads = '0';
        }

         if(isset($request->login_ads)){
            $siteSetting->login_ads = $request->login_ads;
        }else{
            $siteSetting->login_ads = '0';
        }



         if(isset($request->register_ads)){
            $siteSetting->register_ads = $request->register_ads;
        }else{
            $siteSetting->register_ads = '0';
        }



         if(isset($request->dashboard_below_menu_verticle_ads)){
            $siteSetting->dashboard_below_menu_verticle_ads = $request->dashboard_below_menu_verticle_ads;
        }else{
            $siteSetting->dashboard_below_menu_verticle_ads = '0';
        }


         if(isset($request->cms_page_below_content_ads)){
            $siteSetting->cms_page_below_content_ads = $request->cms_page_below_content_ads;
        }else{
            $siteSetting->cms_page_below_content_ads = '0';
        }



         if(isset($request->listing_page_sidebar_verticle_ads)){
            $siteSetting->listing_page_sidebar_verticle_ads = $request->listing_page_sidebar_verticle_ads;
        }else{
            $siteSetting->listing_page_sidebar_verticle_ads = '0';
        }


         if(isset($request->listing_page_below_listings_horizantal_ads)){
            $siteSetting->listing_page_below_listings_horizantal_ads = $request->listing_page_below_listings_horizantal_ads;
        }else{
            $siteSetting->listing_page_below_listings_horizantal_ads = '0';
        }

        $siteSetting->company_details = $request->company_details;

        $siteSetting->country_1 = $request->country_1;
        $siteSetting->country_2 = $request->country_2;
        $siteSetting->country_3 = $request->country_3;

        $siteSetting->job_id_initials = $request->job_id_initials;
        $siteSetting->autocomplete_api = $request->autocomplete_api;

        

        $siteSetting->update();

        flash('Site Setting has been updated!')->success();

        return \Redirect::route('edit.site.setting');

    }



    private function deleteSiteSettingImage($id)

    {

        try {

            $siteSetting = SiteSetting::findOrFail($id);

            $image = $siteSetting->image;

            if (!empty($image)) {

                File::delete(ImgUploader::real_public_path() . 'sitesetting_images/thumb/' . $image);

                File::delete(ImgUploader::real_public_path() . 'sitesetting_images/mid/' . $image);

                File::delete(ImgUploader::real_public_path() . 'sitesetting_images/' . $image);

            }

            return 'ok';

        } catch (ModelNotFoundException $e) {

            return 'notok';

        }

    }



}

