<?php



namespace App\Http\Controllers;



use DB;

use Input;
use App\Alert;
use Form;

use App\Helpers\MiscHelper;

use App\Helpers\DataArrayHelper;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\Controller;

use App\Traits\CountryStateCity;

use App\Company;

use App\User;

use App\Benifits;

use App\Bonus;

use App\Job;

use App\CareerLevel;

use App\FunctionalArea;

use App\JobType;

use App\JobShift;

use App\DegreeLevel;

use App\JobExperience;

use Auth;

use App\JobSkill;

use App\JobSkillManager;

use App\Unlocked_users;

use App\SalaryPeriod;



class AjaxController extends Controller

{



    use CountryStateCity;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //

    }



    public function filterDefaultStates(Request $request)

    {

        $country_id = $request->input('country_id');

        $state_id = $request->input('state_id');

        $new_state_id = $request->input('new_state_id', 'state_id');

        $states = DataArrayHelper::defaultStatesArray($country_id);

        $dd = Form::select('state_id', ['' => __('Select County / State / Province / District')] + $states, $state_id, array('id' => $new_state_id, 'class' => 'form-control'));

        echo $dd;

    }

    public function checkPhone(Request $request)
    {
        $phone = trim($request->phone);
       $type = $request->type;
       if($type=='emp'){
        $company = Company::where('phone',$phone)->first();
        if(!empty($company->phone)){
            echo 'false';
        }else{
            echo 'true';
        }
       }else{
        $user = User::where('phone',$phone)->first();
        if(!empty($user->phone)){
            echo 'false';
        }else{
             echo 'true';
        }
       }
    }


    public function checkEmail(Request $request)

    {

       $email = trim($request->email);
       $type = $request->type;
       if($type=='emp'){
        $company = Company::where('email',$email)->first();
        if(!empty($company->email)){
            echo 'false';
        }else{
            echo 'true';
        }
       }else{
        $user = User::where('email',$email)->first();
        if(!empty($user->email)){
            echo 'false';
        }else{
             echo 'true';
        }
       }

    }

    public function checkSkill(Request $request)

    {

        $job_skill = trim($request->job_skill);
        $jobSkill = JobSkill::where('job_skill',$job_skill)->first();
        if(!empty($jobSkill->job_skill)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function career_level(Request $request)

    {

        $career_level = trim($request->career_level);
        $careerLevel = CareerLevel::where('career_level',$career_level)->first();
        if(!empty($careerLevel->career_level)){
            echo 'false';
        }else{
            echo 'true';
        }

    }

    public function functional_area(Request $request)

    {

        $functional_area = trim($request->functional_area);
        $functionalArea = FunctionalArea::where('functional_area',$functional_area)->first();
        if(!empty($functionalArea->functional_area)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function job_type(Request $request)

    {

        $job_type = trim($request->job_type);
        $jobType = JobType::where('job_type',$job_type)->first();
        if(!empty($jobType->job_type)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function job_shift(Request $request)

    {

        $job_shift = trim($request->job_shift);
        $jobShift = JobShift::where('job_shift',$job_shift)->first();
        if(!empty($jobShift->job_shift)){
            echo 'false';
        }else{
            echo 'true';
        }

    }

    public function degree_level(Request $request)

    {

        $degree_level = trim($request->degree_level);
        $degreeLevel = DegreeLevel::where('degree_level',$degree_level)->first();
        if(!empty($degreeLevel->degree_level)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function job_experience(Request $request)

    {

        $job_experience = trim($request->job_experience);
        $jobExperience = JobExperience::where('job_experience',$job_experience)->first();
        if(!empty($jobExperience->job_experience)){
            echo 'false';
        }else{
            echo 'true';
        }

    }

    public function employer_name(Request $request)

    {

        $employer_name = trim($request->employer_name);
        $employerName = Company::where('name',$employer_name)->first();
        if(!empty($employerName->name)){
            echo 'false';
        }else{
            echo 'true';
        }

    }

    public function employer_email(Request $request)

    {

        $employer_email = trim($request->employer_email);
        $employerEmail = Company::where('email',$employer_email)->first();
        if(!empty($employerEmail->email)){
            echo 'false';
        }else{
            echo 'true';
        }

    }





    public function filterDefaultCompanies(Request $request)

    {

        $skills = $request->input('skills');
        $jobs = JobSkillManager::select('job_id')->whereIn('job_skill_id',$skills)->pluck('job_id')->toArray();

        $company_ids = array();
        if(null!==($jobs)){
            foreach ($jobs as $key => $value) {
                $job = DB::table('jobs')->where('id',$value)->where('is_active',1)->whereDate('expiry_date', '>', \Carbon\Carbon::now())->first();
                if(null!==($job)){
                   $company_ids[] = $job->company_id; 
                }
                 
            }
        }
        $company_id = $request->input('companies')?$request->input('companies'):null;
        $company_ids = array_unique($company_ids);
        $companies = Company::whereIn('id',$company_ids)->where('is_active',1)->pluck('name','id')->toArray();

        $dd = Form::select('company_id[]', $companies, $company_id, array('class'=>'form-control select2-multiple', 'id'=>'company_id', 'multiple'=>'multiple'));
        echo $dd;
        
        

    }


    public function filterDefaultJobs(Request $request)

    {

        $company_id = $request->input('company_id');

        //$job_id = $request->input('job_id');

        $company = Company::where('id',$company_id)->first();
        
        $openjobs = $company->getOpenJobs();

        $jobs = array();

        if(null!==($openjobs)){

            foreach ($openjobs as $key => $value) {

                $jobs[$value->id] = $value->title;

            }

        }

        $dd = Form::select('job_id', ['' => __('Select Job')] + $jobs, '', array('onChnage'=>'check_lock_unlock()','id' => 'job_id', 'class' => 'form-control job_id'));

        echo $dd;

    }



    public function filterRecruitmentJobs(Request $request)

    {

        $company_id = $request->input('company_id');
        if($request->input('job_id')){
            $job_id = $request->input('job_id');
        }else{
            $job_id = '';
        }
        

        $company = Company::where('id',$company_id)->first();
        
        $openjobs = $company->getRecruitmentJobs();

        $jobs = array();

        if(null!==($openjobs)){

            foreach ($openjobs as $key => $value) {

                $jobs[$value->id] = $value->title;

            }

        }

        $dd = Form::select('job_id', ['' => __('Select Job')] + $jobs, $job_id, array('onChnage'=>'get_info()','id' => 'job_id', 'class' => 'form-control job_id'));

        echo $dd;

    }

    public function filterDefaultStatus(Request $request)

    {
        $status = array(
            'called_for_interview' => 'Called for Interview',
            'interviewed' => 'Interviewed',
            'recruited' => 'Recruited',
            'resigned' => 'Resigned',
            'internally_filled_by_employer' => 'Internally filled by employer',
        );
        $dd = Form::select('status', ['' => __('Select Status')] + $status, '', array('onChnage'=>'filterStatus()','id' => 'status', 'class' => 'form-control status'));

        echo $dd;

    }




    public function filterDefaultJobsAll(Request $request)

    {
        //dd($request);
        $company_id = $request->input('company_id');
        //dd($company_id);
        $all_ids = $request->input('all_ids');
        $checked = trim($all_ids,',');
        $array = explode(',', $checked);
        $str = '';
        
        $jobs = array();
        if(null!==($company_id)){
            foreach ($company_id as $key => $val) {
                $company = Company::where('id',$val)->first();
        
                $openjobs = $company->getOpenJobs();

                $str .='<h4 style="font-weight: bold;">'.$company->name.'</h4>';
                $str .='<table class="table table-striped table-bordered table-hover"> <thead><tr><th style="text-align: center;">Job Title</th><th style="text-align: center;">Skills</th><th style="text-align: center;">Salary Range</th><th style="text-align: center;">Posted Date</th><th style="text-align: center;">Job Location</th><th style="text-align: center;">Action</th></tr></thead><tbody style="text-align: center;">';
                if(null!==($openjobs)){

                    foreach ($openjobs as $key => $value) {
                        $condition = Unlocked_users::where('job_id',$value->id)->where('unlocked_users_ids',$request->user_id)->first();

                        $check = '';
                        
                        if(in_array($value->id, $array)){
                           $check = 'checked'; 
                        }
                        
                        if($value->id==$request->job_id){
                            $check = 'checked'; 
                        }

                        

                                    if($value->salary_type == 'single_salary'){
                                        if(null!==($value->salary_from)){
                                            $salary = '<strong><span class="symbol">'.$value->salary_currency.'</span>'.number_format($value->salary_from).'</strong>';
                                        }else{
                                            $salary = '';
                                        }
                                        
                                    }else if($value->salary_type == 'salary_in_range'){
                                        //echo $job->salary_from;
                                        $salary_from = (null!==($value->salary_from))?'<strong><span class="symbol">'.$value->salary_currency.'</span>'.number_format($value->salary_from):null;
                                        $salary_to = (null!==($value->salary_from))?' - <span class="symbol">'.$value->salary_currency.'</span>'.number_format($value->salary_to).'</strong>':null;
                                        $salary = $salary_from.$salary_to;

                                    }else{
                                        if(null!==($value->salary_from)){
                                        $salary = '<strong><span class="symbol">'.$value->salary_currency.'</span>'.$value->salary_from.'</strong>';
                                        }else{
                                            $salary = '';
                                        }
                                    }  
                        $str .='<tr>';
                        $str .='<td>'.$value->title.'</td>';
                        $str .='<td>'.$value->getJobSkillsListt().'</td>';
                        $str .='<td>'.$salary.'</td>';
                        $str .='<td>'.date('d-M-Y',strtotime($value->created_at)).'</td>';
                        $str .='<td>'.$value->address.'</td>';
                        if(null==($condition)){
                            $str .='<td><input type="checkbox"  value="'.$value->id.'"  name="'.$company->id.'"  id="input_chcked_'.$value->id.'" '.$check.' onClick="myfunction('.$value->id.');"></td>';  
                        }else{
                            $str .='<td>Already shortlisted</td>';  
                        }
                        
                        $str .='</tr>';



                    }

                }
                $str .='</tbody></table>';
                $str .='<hr>';
            }
        }
        

        echo $str;

    }



    public function checkLockUnloc(Request $request)

    {

        $job_id = $request->input('job_id');

        $company_id = $request->input('company_id');

        $check = Unlocked_users::where('job_id',$job_id)->where('company_id',$company_id)->get();

        $ids = '';

        if(null!==($check)){

            $array = array();

            foreach ($check as $key => $value) {

                $array[] = $value->unlocked_users_ids; 

            }

            $ids = implode(',', $array);

        }

        echo $ids;

    }



    public function filterDefaultCities(Request $request)

    {

        $state_id = $request->input('state_id');

        $city_id = $request->input('city_id');

        $cities = DataArrayHelper::defaultCitiesArray($state_id);

        $dd = Form::select('city_id', ['' => 'Select Town / City'] + $cities, $city_id, array('id' => 'city_id', 'class' => 'form-control'));

        echo $dd;

    }



    /*     * ***************************************** */



    public function filterLangStates(Request $request)

    {

        $country_id = $request->input('country_id');

        $state_id = $request->input('state_id');

        $new_state_id = $request->input('new_state_id', 'state_id');

        $states = DataArrayHelper::langStatesArray($country_id);

        $dd = Form::select('state_id', ['' => __('Select County / State / Province / District')] + $states, $state_id, array('id' => $new_state_id, 'class' => 'form-control', 'required' => 'required'));

        echo $dd;

    }



    public function filterLangCities(Request $request)

    {

        $state_id = $request->input('state_id');

        $city_id = $request->input('city_id');

        $cities = DataArrayHelper::langCitiesArray($state_id);



        $dd = Form::select('city_id', ['' => 'Select Town / City'] + $cities, $city_id, array('id' => 'city_id', 'class' => 'form-control', 'required' => 'required'));

        echo $dd;

    }



    /*     * ***************************************** */



    public function filterStates(Request $request)

    {

        $country_id = $request->input('country_id');

        $state_id = $request->input('state_id');

        $new_state_id = $request->input('new_state_id', 'state_id');

        $states = DataArrayHelper::langStatesArray($country_id);

        $dd = Form::select('state_id[]', ['' => __('Select County / State / Province / District')] + $states, $state_id, array('id' => $new_state_id, 'class' => 'form-control'));

        echo $dd;

    }



    public function filterCities(Request $request)

    {

        $state_id = $request->input('state_id');

        $city_id = $request->input('city_id');

        $cities = DataArrayHelper::langCitiesArray($state_id);



        $dd = Form::select('city_id[]', ['' => 'Select Town / City'] + $cities, $city_id, array('id' => 'city_id', 'class' => 'form-control'));

        echo $dd;

    }



    /*     * ***************************************** */



    public function filterDegreeTypes(Request $request)

    {

        $degree_level_id = $request->input('degree_level_id');

        $degree_type_id = $request->input('degree_type_id');



        $degreeTypes = DataArrayHelper::langDegreeTypesArray($degree_level_id);



        $dd = Form::select('degree_type_id', ['' => 'Select degree type'] + $degreeTypes, $degree_type_id, array('id' => 'degree_type_id', 'class' => 'form-control'));

        echo $dd;

    }

    public function storeBonusForm(Request $request)
    {
        $bonuss = new Bonus();
        $bonuss->bonus = $request->input('bonus');
        $bonuss->is_active = 1;
        $bonuss->lang = $request->input('lang');
        $bonuss->is_default = 1;
        $bonuss->save();
        /*         * ************************************ */
        $bonuss->sort_order = $bonuss->id;
        $bonuss->bonus_id = $bonuss->id;
        
        $bonuss->update();
        $job_bonus = DataArrayHelper::langBonusArray();
        $bonus = $request->bonus_m.','.$bonuss->bonus_id;

        $exp_bonus = explode(',', $bonus);


        $dd = Form::select('bonus[]', $job_bonus,$exp_bonus , array('class'=>'form-control select2-multiple-bonus', 'id'=>'bonus', 'multiple'=>'multiple'));

        echo $dd;
    }

    public function checkBonus(Request $request)

    {

        $bonu_s = trim($request->bonus);
        $bonus = Bonus::where('bonus',$bonu_s)->first();
        if(!empty($bonus->bonus)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function checkAlert(Request $request)

    {

        $search_s = trim($request->search);
        $search = Alert::where('search_title',$search_s)->where('email',Auth::user()->email)->first();
        if(!empty($search->search_title)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function storeBenifitsForm(Request $request)
    {
        $benifits = new Benifits();
        $benifits->benifits = $request->input('benifits');
        $benifits->is_active = 1;
        $benifits->lang = $request->input('lang');
        $benifits->is_default = 1;
        $benifits->save();
        /*         * ************************************ */
        $benifits->sort_order = $benifits->id;
        $benifits->benifit_id = $benifits->id;
        $benifits->update();
        $benifitss = DataArrayHelper::langBenifitsArray();
        $benifitsss = $request->benifits.','.$benifits->benifit_id;

        $exp_benifits = explode(',', $benifitsss);


        $dd = Form::select('benifits[]', $benifitss,$exp_benifits , array('class'=>'form-control select2-multiple-benifits', 'id'=>'benifits', 'multiple'=>'multiple'));

        echo $dd;
    }

    public function checkBenifits(Request $request)

    {

        $benifit_s = trim($request->benifits);
        $benifits = Benifits::where('benifits',$benifit_s)->first();
        if(!empty($benifits->benifits)){
            echo 'false';
        }else{
            echo 'true';
        }

    }


    public function checkSalaryPeriod(Request $request)

    {

        $salary_period_s = trim($request->salary_period);
        $salary_period = SalaryPeriod::where('salary_period',$salary_period_s)->first();
        if(!empty($salary_period->salary_period)) {
            echo 'false';
        } else {
            echo 'true';
        }

    }


    public function viewPublicProfile($id)
    {
        if(Auth::guard('admin')->user()){
            $user = User::findOrFail($id);

        $profileCv = $user->getDefaultCv();

        $jobSkills = DataArrayHelper::defaultJobSkillsArray();

        $companies = Company::get();



        return view('admin.user.applicant_profile')

                        ->with('user', $user)

                        ->with('profileCv', $profileCv)

                        ->with('page_title', $user->getName())

                        ->with('jobSkills', $jobSkills)

                        ->with('companies', $companies)

                        ->with('form_title', 'Contact ' . $user->getName());
                    }else{
                        return redirect(url('admin/login'));
                    }
        



    }

    public function companyProfile($id)

    {
         if(Auth::guard('admin')->user()){
        $countries = DataArrayHelper::defaultCountriesArray();

        $industries = DataArrayHelper::defaultIndustriesArray();

        $ownershipTypes = DataArrayHelper::defaultOwnershipTypesArray();

        $company = Company::findOrFail($id);

        return view('admin.company.detail')

                        ->with('company', $company)

                        ->with('countries', $countries)

                        ->with('industries', $industries)

                        ->with('ownershipTypes', $ownershipTypes);
                    }else{
                        return redirect(url('admin/login'));
                    }

    }

    public function jobDetail(Request $request, $id)
    {
         if(Auth::guard('admin')->user()){
        $job = Job::findOrFail($id);
        
        return view('admin.job.detail')
                        ->with('job', $job);
                    }else{
                        return redirect(url('admin/login'));
                    }
    }



}

