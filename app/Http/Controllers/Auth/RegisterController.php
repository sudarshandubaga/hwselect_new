<?php



namespace App\Http\Controllers\Auth;



use App\User;
use App\Subscription;
use App\Verify_tokens;
use App\SiteSetting;
use App\CountryDetail;

use Redirect;

use App\Http\Requests;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\RegistersUsers;

use Jrean\UserVerification\Traits\VerifiesUsers;

use Jrean\UserVerification\Facades\UserVerification;

use App\Http\Requests\Front\UserFrontRegisterFormRequest;

use Illuminate\Auth\Events\Registered;

use App\Events\UserRegistered;

use App\Helpers\DataArrayHelper;

use App\Mail\Newsletter;

use Mail;



class RegisterController extends Controller

{

    /*

      |--------------------------------------------------------------------------

      | Register Controller

      |--------------------------------------------------------------------------

      |

      | This controller handles the registration of new users as well as their

      | validation and creation. By default this controller uses a trait to

      | provide this functionality without requiring any additional code.

      |

     */



    use RegistersUsers;

    use VerifiesUsers;



    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = '/home';



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {
        parent::__construct();
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError']]);

    }



    public function verify_phone(Request $request)

    {

        $token = $request->csrf_token;
        $tokens = explode('.', $token);

        $second = base64_decode($tokens[1]);
        $data = json_decode($second);
        $auth_time = trim($data->auth_time);
        $user_id = trim($data->user_id);
        $phone_number = trim($data->phone_number);

        $tok = new Verify_tokens();
        $tok->auth_time = $auth_time;
        $tok->user_id = $user_id;
        $tok->phone_number = $phone_number;
        $tok->save();

        echo $tok->auth_time;

    }

    public function showRegistrationForm()

    {
        $siteSetting = SiteSetting::findOrFail(1272);
        // dd($siteSetting);

        $countries_details = DataArrayHelper::langCountriesDetailsArray();

        return view('auth.register')->with('countries_details',$countries_details);

    }

    public function showJobseekerRegistrationForm(){
        $siteSetting = SiteSetting::findOrFail(1272);
        
        $countries_details = DataArrayHelper::langCountriesDetailsArray();

        return view('auth.registerJobseeker')->with('countries_details',$countries_details);
    }

    public function showCompanyRegistrationForm(){
        $siteSetting = SiteSetting::findOrFail(1272);

        $countries_details = DataArrayHelper::langCountriesDetailsArray();

        return view('auth.registerCompany')->with('countries_details',$countries_details);
    }

    public function saveForm(Request $request)

    {
        $request->session()->put('seeker_form_data', $request->all());
        echo 'done';

    }



    public function register(UserFrontRegisterFormRequest $request)

    {
        // dd('Hello');
    	//echo '<pre>';
    	$secret_key = $this->_setting->nocaptcha_secret; // '6Lc5oYAcAAAAAKJStGu5L6lnlezlCpwGFhCBrz8H';

        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);

        // dd($responseData);
        
        if($responseData->success == 1)
        {
            $verif = Verify_tokens::where('auth_time','like',trim($request->auth_time))->first();
            $siteSetting = SiteSetting::findOrFail(1272);
            if(null!==($siteSetting->verify_registration_phone_number)){
                if(null==($verif)){
                  return redirect()->back()
                            ->withInput($request->input())
                            ->with('message.not_verified_seeker','success');
                }
            }

            $country = CountryDetail::where('phone_code', $request->phone_num_code)->first();
            
            //dd($verif);
            $user = new User();

            $user->first_name = $request->input('first_name');

            $user->middle_name = $request->input('middle_name');

            $user->last_name = $request->input('last_name');

            $user->phone = $request->input('phone');

            $user->phone_code = $request->input('phone_code');
            $user->phone_num_code = $request->input('phone_num_code');

            $user->country_id = $country->country_id;

            $user->email = $request->input('email');

            $user->password = bcrypt($request->input('password'));

            if(null==($siteSetting->verify_registration_phone_number)){
                $user->phone_verify = 'yes';
            }

            $user->is_active = 0;

             if($request->input('is_subscribed')){
                $user->is_subscribed = 1;
            }

            $user->verified = 0;

            $user->two_step_verification = $this->_setting->verify_registration_phone_number;

            $user->save();

            /*         * *********************** */

            $user->name = $user->getName();

            $user->update();

            /*         * *********************** */
            $request->session()->put('seeker_form_data', '');
            event(new Registered($user));

            event(new UserRegistered($user));

            //$this->guard()->login($user);

            UserVerification::generate($user);

            UserVerification::send($user, 'User Verification', config('mail.recieve_to.address'), config('mail.recieve_to.name'));

            if($request->input('is_subscribed')){
                $subscription = new Subscription();
                $subscription->email = $request->input('email');
                $subscription->name = $request->input('name');
                $subscription->type = 'seeker';
                $subscription->save();
                $data['name'] = $request->input('name');
                $data['email'] = $request->input('email');
                $data['type'] = 'seeker';
                //Mail::send(new Newsletter($data));
            }
            return Redirect::route('login')->with('message.added','success')->with('email',$request->input('email'));
            //return $this->registered($request, $user) ?: redirect($this->redirectPath());
        }
    }



}

