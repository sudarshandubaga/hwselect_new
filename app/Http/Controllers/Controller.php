<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\Job;
use App\SiteSetting;

class Controller extends BaseController
{

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public $_setting = null;

    public function __construct()
    {
        $data['setting'] = $this->_setting = SiteSetting::first();

        $top_category_ids = [];
        if($data['setting']->country_1) $top_category_ids[] = $data['setting']->country_1;
        if($data['setting']->country_2) $top_category_ids[] = $data['setting']->country_2;
        if($data['setting']->country_3) $top_category_ids[] = $data['setting']->country_3;

        $top_countries = $data['setting']->getPriorCountriesFlags(); 
        $all_countries = \App\Country::isDefault()->active()->whereNotIn('id', $top_category_ids)->get()->sortBy('country_short_name')->pluck('flag', 'id')->toArray();
        
        $data['country'] = $top_countries + $all_countries;

        \View::share($data);
    }

    public static function JobCount() {
    	if(null !== (Auth::user())) {
            // $myFavouriteJobSlugs = Auth::user()->getFavouriteJobSlugsArray();
            $myFavouriteJobIds = Auth::user()->getFavouriteJobIdArray();
            
            $jobs = Job::whereIn('id', $myFavouriteJobIds)->count();  
            //dd($jobs);
            return $jobs;
        }
    }
}
