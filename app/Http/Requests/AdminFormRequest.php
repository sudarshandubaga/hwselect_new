<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int) $this->input('id', 0);
        $pass_required = 'required|min:3|max:16';
        $id_str = '';
        if ($id > 0) {
            $id_str = ',' . $id;
            $pass_required = '';
        }
        //echo $id_str;exit;
        return [
            'name' => 'required|unique:admins,name' . $id_str . '|max:40',
            'email' => 'required|unique:admins,email' . $id_str . '|email|max:40',
            'password' => $pass_required,
            'role_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name Required',
            'name.unique' => 'The name has already been taken.',
            'name.max' => 'Name may not be greater than 30 characters.',
            'email.required' => 'Email Required',
            'email.email' => 'The email must be a valid email address.',
            'email.unique' => 'This Email has already been taken.',
            'name.max' => 'The email may not be greater than 40 characters.',
            'password.required' => 'Password Required',
            'password.min' => 'Password must be a minimum of 3 characters long',
            'password.max' => 'Password may not be greater than 16 characters.',
            'role_id.required' => 'Please Select Role',
        ];
    }

}
