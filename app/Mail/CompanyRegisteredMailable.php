<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\SiteSetting;

class CompanyRegisteredMailable extends Mailable
{

    use SerializesModels;

    public $company;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company)
    {
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        return $this->to(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
                        ->bcc('richa.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject('Company "' . $this->company->name . '" has registered on "' . config('app.name'))
                        ->view('emails.company_registered_message')
                        ->with(
                                [
                                    'name' => $this->company->name,
                                    'email' => $this->company->email,
                                    'link' => route('company.detail', $this->company->slug),
                                    'link_admin' => route('edit.company', ['id' => $this->company->id])
                                ]
        );
    }

}
