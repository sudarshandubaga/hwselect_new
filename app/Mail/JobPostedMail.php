<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SiteSetting;

class JobPostedMail extends Mailable
{

    use Queueable,
        SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $siteSetting = SiteSetting::findOrFail(1272);
        if (strpos($siteSetting->mail_cc_address, ',') !== false) {
            $cc_email = explode(",",$siteSetting->mail_cc_address);
        }
        else{
            $cc_email = $siteSetting->mail_cc_address;
        }
        
        $to = [['email' => config('mail.recieve_to.address'), 'name' => config('mail.recieve_to.name')], 
       ['email' => $this->data['email'], 'name' => $this->data['full_name']]];
        return $this->from($this->data['email'], $this->data['full_name'])
                        ->replyTo($this->data['email'], $this->data['full_name'])
                        ->to($to)
                        // ->bcc('aesha.isync@gmail.com')
                        ->cc($cc_email, $siteSetting->mail_cc_name)
                        ->subject($this->data['subject'])
                        ->view('emails.send_posted_job_mail')
                        ->with($this->data);
    }

}
