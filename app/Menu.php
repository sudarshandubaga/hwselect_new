<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
   protected $table = 'menus';
   public function buildMenu($menu, $parentid = 0) 
	{ 
	  $result = null;
         
	  foreach ($menu as $item) 
	    if ($item->parent_id == $parentid) { 
	      $result .= "<li class='dd-item nested-list-item' id='item_{$item->id}' data-order='{$item->order}' data-id='{$item->id}'>
	      <div class='dd-handle nested-list-handle'>
	        <span class='glyphicon glyphicon-move'></span>
	      </div>
	      <div class='nested-list-content'>{$item->label}
	        <div class='pull-right'>
                <a href='#' onClick='load_menu_edit_form({$item->id});'> Edit</a>|
	          <a href='#' onClick='delete_menu({$item->id});' rel='{$item->id}'>Delete</a>
	        </div>
	      </div>".$this->buildMenu($menu, $item->id) . "</li>"; 
	    } 
	  return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null; 
	} 

	// Getter for the HTML menu builder
	public function getHTML($items)
	{
		return $this->buildMenu($items);
	}

	public function submenus() {
	    return $this->hasMany(Menu::class, 'parent_id', 'id');
	   }
 
}
