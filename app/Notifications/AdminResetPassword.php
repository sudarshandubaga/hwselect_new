<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\HtmlString;

use App\SiteSetting;

class AdminResetPassword extends Notification
{

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $siteSetting = SiteSetting::findOrFail(1272);

        return (new MailMessage)
                        ->subject(Lang::get('Request to Reset Password – Notification'))
                        ->greeting(new HtmlString('<strong>Dear</strong> '.$notifiable->name.'!'))
                        // ->line(new HtmlString('DEMO Select Ltd "ADMINISTRATOR" has created an admin(ROAL) account for you  Please setup a new password on the link below to login to your ADMIN PANEL'))
                        ->line(new HtmlString('<strong>Time of Request:</strong> '.date('l, d F Y')))
                        ->line(new HtmlString('<strong>Ip Address:</strong> '.\Request::ip()))
                        ->line(Lang::get('You are receiving this email because we received a password reset request for your account.'))
                        ->action('Reset Password', url('admin/password/reset', $this->token).'?email='.$notifiable->email)
                        ->line(Lang::get('Password reset link will expire in 30 minutes. If you did not request a password reset, no further action is required.'));
    }

}
