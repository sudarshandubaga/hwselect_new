<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportAbuseMessage extends Model
{

    protected $table = 'report_abuse_messages';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    protected $with = ['user', 'company', 'job'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
