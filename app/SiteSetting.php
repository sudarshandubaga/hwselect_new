<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{

    protected $table = 'site_settings';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function getPriorCountriesFlags()
    {
        $country = [];
        if(!empty($this->country_1))
        {
            $countryRow = Country::where('id', $this->country_1)->active()->first();
            if(!empty($countryRow->id))
                $country[$this->country_1] = $countryRow->flag;
        }
        if(!empty($this->country_2))
        {
            $countryRow = Country::where('id', $this->country_2)->active()->first();
            if(!empty($countryRow->id))
                $country[$this->country_2] = $countryRow->flag;
            // $country[$this->country_2] = Country::where('id', $this->country_2)->first()->flag;
        }
        if(!empty($this->country_3))
        {
            $countryRow = Country::where('id', $this->country_3)->active()->first();
            if(!empty($countryRow->id))
                $country[$this->country_3] = $countryRow->flag;
            // $country[$this->country_3] = Country::where('id', $this->country_3)->first()->flag;
        }
        
        return $country;
    }

    public function getPriorCountries()
    {
        $country = [];
        if(!empty($this->country_1))
        {
            // $countryRow = Country::where('id', $this->country_1)->active()->first();
            $country[$this->country_1] = CountryDetail::where('country_id', $this->country_1)->first()->country_name;
        }
        if(!empty($this->country_2))
        {
            // $arr[] = $this->country_2;
            $country[$this->country_2] = CountryDetail::where('country_id', $this->country_2)->first()->country_name;
        }
        if(!empty($this->country_3))
        {
            // $arr[] = $this->country_3;
            $country[$this->country_3] = CountryDetail::where('country_id', $this->country_3)->first()->country_name;
        }
        // dd($country);
        // $country = CountryDetail::whereIn('country_id', $arr)->get()->pluck('country_name', 'country_id');
        return $country;
    }

}
