<?php

namespace App\Traits;

use File;
use ImgUploader;
use Auth;
use DB;
use Input;
use Mail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Redirect;
use App\User;
use App\ProfileCv;
use App\Http\Requests;
use Illuminate\Http\Redelete_profile_cvquest;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileCvFormRequest;
use App\Http\Requests\ProfileCvFileFormRequest;
use App\Mail\NotifyToAdmin;

trait ProfileCvsTrait
{

    public function showProfileCvs(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $html = '<div class="table-responsive"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileCvs)):
            $cvCounter = 0;
            
            $html .= '<input type="hidden" id="totalCvCount" value="'. $user->profileCvs->count() .'">';

            foreach ($user->profileCvs as $cv):
                $default = 'Not Default';
                if ($cv->is_default == 1)
                    $default = 'Default CV';
                $dd = "'".$cv->title."'";

                $deleteHtml  = '';
                if($cv->is_default != 1) {
                    $deleteHtml = '&nbsp;|&nbsp;<a href="javascript:;" onclick="delete_profile_cv(' . $cv->id . ','.$dd.');" class="text text-danger">' . __('Delete') . '</a>';
                }
                
                $html .= '<tr id="cv_' . $cv->id . '">
                            <td width="45%" class="black-text">' . ImgUploader::get_doc("cvs/$cv->cv_file", $cv->title, $cv->title) . '</td>
                            <td width="15%" align="center"><span class="text text-red">' . $default . '</span></td>

                            <td width="25%"><span class="text text-blue">Added ' . date('l jS F Y',strtotime($cv->created_at)) . '</span></td>
                            <td width="10%"><a href="javascript:;" onclick="showProfileCvEditModal(' . $cv->id . ');" class="text text-warning">' . __('Update') . $deleteHtml . '</a></td>
                        </tr>';
            endforeach;
        endif;

        echo $html . '</table></div>';
    }

    public function uploadCvFile($request)
    {
        $fileName = '';
        if ($request->hasFile('cv_file')) {
            $cv_file = $request->file('cv_file');
            $fileName = ImgUploader::UploadDoc('cvs', $cv_file, $request->input('title'));
        }
        //dd($fileName);
        return $fileName;
    }

    public function getFrontProfileCvForm(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $returnHTML = view('user.forms.cv.cv_modal')->with('user', $user)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getProfileCvForm(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $returnHTML = view('admin.user.forms.cv.cv_modal')->with('user', $user)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function storeProfileCv(ProfileCvFormRequest $request, $user_id)
    {

        $profileCv = new ProfileCv();
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->save();
        $user = User::findOrFail($user_id);
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['cv'] = $profileCv->cv_file;

        Mail::send(new NotifyToAdmin($data));

        $returnHTML = view('admin.user.forms.cv.cv_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function storeFrontProfileCv(ProfileCvFormRequest $request, $user_id)
    {
        $user = User::findOrFail($user_id);

        $profileCv = new ProfileCv();
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->save();
        if(count($user->profileCvs)==0){
            $profileCv->is_default = 1;
            $profileCv->update();
        }
        $user = User::findOrFail($user_id);
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['cv'] = $profileCv->cv_file;

        Mail::send(new NotifyToAdmin($data));

        $returnHTML = view('user.forms.cv.cv_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    private function assignValues($profileCv, $request, $user_id)
    {
        $profileCv->user_id = $user_id;
        $profileCv->title = $request->input('title');
        $profileCv->is_default = $request->input('is_default');
        //dd($profileCv);
        /*         * ************************************ */
        if ((int) $request->input('is_default') == 1) {
            $this->updateDefaultCv($profileCv->id);
        }
        /*         * ************************************ */

        if ($request->hasFile('cv_file')) {
            //$this->deleteCv($profileCv->id);

            $profileCv->cv_file = $this->uploadCvFile($request);
        }
        

        return $profileCv;
    }

    public function getProfileCvEditForm(Request $request, $user_id)
    {
        $cv_id = $request->input('cv_id');
        $profileCv = ProfileCv::find($cv_id);
        $user = User::find($user_id);
        $returnHTML = view('admin.user.forms.cv.cv_edit_modal')
                ->with('user', $user)
                ->with('profileCv', $profileCv)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileCvEditForm(Request $request, $user_id)
    {
        $cv_id = $request->input('cv_id');
        $profileCv = ProfileCv::find($cv_id);
        $user = User::find($user_id);
        $returnHTML = view('user.forms.cv.cv_edit_modal')
                ->with('user', $user)
                ->with('profileCv', $profileCv)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function updateProfileCv(ProfileCvFormRequest $request, $cv_id, $user_id)
    {

        $profileCv = ProfileCv::find($cv_id);
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->update();

        $returnHTML = view('admin.user.forms.cv.cv_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function updateFrontProfileCv(ProfileCvFormRequest $request, $cv_id, $user_id)
    {

        $profileCv = ProfileCv::find($cv_id);
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->update();

        $returnHTML = view('user.forms.cv.cv_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function makeDefaultCv(Request $request)
    {
        $id = $request->input('id');
        try {
            $profileCv = ProfileCv::findOrFail($id);
            $profileCv->is_default = 1;
            $profileCv->update();
            $this->updateDefaultCv($id);
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    private function updateDefaultCv($cv_id)
    {
        ProfileCv::where('is_default', '=', 1)->where('id', '<>', $cv_id)->update(['is_default' => 0]);
    }

    public function deleteAllProfileCvs($user_id)
    {
        $profileCvs = ProfileCv::where('user_id', '=', $user_id)->get();
        foreach ($profileCvs as $profileCv) {
            echo $this->removeProfileCv($profileCv->id);
        }
    }

    public function deleteProfileCv(Request $request)
    {
        $id = $request->input('id');
        echo $this->removeProfileCv($id);
    }

    private function removeProfileCv($id)
    {
        try {
            $this->deleteCv($id);
            $profileCv = ProfileCv::findOrFail($id);
            $profileCv->delete();
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function deleteCv($id)
    {
        try {
            $profileCv = ProfileCv::findOrFail($id);
            $file = $profileCv->cv_file;
            if (!empty($file)) {
                File::delete(ImgUploader::real_public_path() . 'cvs/' . $file);
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

}
