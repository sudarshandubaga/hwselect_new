<?php

namespace App\Traits;

use Auth;
use DB;
use Input;
use Carbon\Carbon;
use Redirect;
use App\User;
use App\Job;
use App\ProfileSkill;
use App\JobSkill;
use App\JobExperience;
use App\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileSkillFormRequest;
use App\Helpers\DataArrayHelper;

trait ProfileSkillTrait
{

    public function showProfileSkills(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $html = '<table class="table table-bordered table-condensed"><tr>
      <th bgcolor="#eeeeee" scope="col">Skill Sets</th>
      <th bgcolor="#eeeeee" scope="col">Experience</th>
      <th bgcolor="#eeeeee" scope="col">Action</th>
    </tr>';
        if (isset($user) && count($user->profileSkills)):
            foreach ($user->profileSkills as $skill):

                $title = "'".$skill->getJobSkill('job_skill')."'";

                $html .= '<tr id="skill_' . $skill->id . '">
						<td><span class="text text-success">' . $skill->getJobSkill('job_skill') . '</span></td>
						<td><span class="text text-success" style="white-space: nowrap">' . $skill->getJobExperience('job_experience') . '</span></td>
						<td><a href="javascript:;" onclick="showProfileSkillEditModal(' . $skill->id . ');" class="text text-warning">' . __('Edit') . '</a>&nbsp;|&nbsp;<a href="javascript:;" onclick="delete_profile_skill(' . $skill->id . ','.$title.');" class="text text-danger">' . __('Delete') . '</a></td>
								</tr>';
            endforeach;
        endif;

        echo $html . '</table>';
    }

    public function showApplicantProfileSkills(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $html = '<div class="col-md-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileSkills)):
            foreach ($user->profileSkills as $skill):

                $html .= '<tr id="skill_' . $skill->id . '">
						<td><span class="text text-success">' . $skill->getJobSkill('job_skill') . '</span></td>
						<td><span class="text text-success" style="white-space: nowrap">' . $skill->getJobExperience('job_experience') . '</span></td></tr>';
            endforeach;
        endif;

        echo $html . '</table></div>';
    }

    public function getProfileSkillForm(Request $request, $user_id)
    {

        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();

        $user = User::find($user_id);
        $returnHTML = view('admin.user.forms.skill.skill_modal')
                ->with('user', $user)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileSkillForm(Request $request, $user_id)
    {

        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        $user = User::find($user_id);
        $returnHTML = view('user.forms.skill.skill_modal')
                ->with('user', $user)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function storeProfileSkill(ProfileSkillFormRequest $request, $user_id)
    {
        $profileSkill = new ProfileSkill();
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->save();
        /*         * ************************************ */
        $returnHTML = view('admin.user.forms.skill.skill_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function storeFrontProfileSkill(Request $request, $user_id)
    {
        if($request->input('add_new_skill')){
            $this->validate($request,[
                'add_new_skill' => 'required|unique:job_skills,job_skill',
                'job_experience_id' => 'required',
            ],[
                'add_new_skill.required' => 'Please select or add acquired skills',
                'add_new_skill.unique' => 'Skill already exists',
                'job_experience_id.required' => 'Please Select Length of Experience',
            ]);

            $jobSkill = new JobSkill();
            $jobSkill->job_skill = $request->input('add_new_skill');
            $jobSkill->is_active = 1;
            $jobSkill->lang = 'en';
            $jobSkill->is_default = 1;
            $jobSkill->save();
            /*         * ************************************ */
            $jobSkill->sort_order = $jobSkill->id;
            $jobSkill->job_skill_id = $jobSkill->id;
            
            $jobSkill->update();

            $profileSkill = new ProfileSkill();
            $profileSkill->user_id = $user_id;
            $profileSkill->job_skill_id = $jobSkill->id;
            $profileSkill->job_experience_id = $request->input('job_experience_id');
            $profileSkill->save();
            /*         * ************************************ */
            $returnHTML = view('user.forms.skill.skill_thanks')->render();





        }else{
            $this->validate($request,[
                'job_skill_id' => 'required',
                'job_experience_id' => 'required',
            ],[
                'job_skill_id.required' => 'Please select or add acquired skills',
                'job_experience_id.required' => 'Please Select Length of Experience',
            ]);

            $check = ProfileSkill::where('user_id',$user_id)->where('job_skill_id',$request->input('job_skill_id'))->first();
            if (!$check) {
                $profileSkill = new ProfileSkill();
                $profileSkill->user_id = $user_id;
                $profileSkill->job_skill_id = $request->input('job_skill_id');
                $profileSkill->job_experience_id = $request->input('job_experience_id');
                $profileSkill->save();
                /*         * ************************************ */
                $returnHTML = view('user.forms.skill.skill_thanks')->render();
            }else{
                 $returnHTML = view('user.forms.skill.skill_already_exist')->render();
            }
            
        }
        
        
        
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function getProfileSkillEditForm(Request $request, $user_id)
    {
        $skill_id = $request->input('skill_id');
        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();

        $profileSkill = ProfileSkill::find($skill_id);
        $user = User::find($user_id);

        $returnHTML = view('admin.user.forms.skill.skill_edit_modal')
                ->with('user', $user)
                ->with('profileSkill', $profileSkill)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileSkillEditForm(Request $request, $user_id)
    {
        $skill_id = $request->input('skill_id');

        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        $profileSkill = ProfileSkill::find($skill_id);
        $user = User::find($user_id);

        $returnHTML = view('user.forms.skill.skill_edit_modal')
                ->with('user', $user)
                ->with('profileSkill', $profileSkill)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function updateProfileSkill(ProfileSkillFormRequest $request, $skill_id, $user_id)
    {

        $profileSkill = ProfileSkill::find($skill_id);
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->update();
        /*         * ************************************ */

        $returnHTML = view('admin.user.forms.skill.skill_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function updateFrontProfileSkill(ProfileSkillFormRequest $request, $skill_id, $user_id)
    {

        $profileSkill = ProfileSkill::find($skill_id);
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->update();
        /*         * ************************************ */

        $returnHTML = view('user.forms.skill.skill_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function deleteProfileSkill(Request $request)
    {
        $id = $request->input('id');
        try {
            $profileSkill = ProfileSkill::findOrFail($id);
            $profileSkill->delete();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function onlyskills(){
        $array = array();
        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        foreach ($jobSkills as $key => $value) {
            $array[] = array(
                "id" => $key,
                "tag_name" => $value,
            );
        }
        return json_encode($array);

    }
    public function matchedskills($id){
        $job = Job::findOrFail($id);
        $jobSkillIds = $job->getJobSkillsArray();
        $array = array();
       if(null!==($jobSkillIds)){
                $skills_names = JobSkill::select('job_skill')->whereIn('id',$jobSkillIds)->pluck('job_skill')->toArray();
        }
        foreach ($skills_names as $key => $value) {
            $array[] =  $value;
        }
        return json_encode($array);

    }

}
