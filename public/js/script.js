"use strict";
/* ==== Jquery Functions ==== */
(function ($) {
	$('[type=password]').parent().addClass('password-field');
	$('.password-field input').after(
		`<i class="fa fa-eye eye-icon"></i>`
	);

	$('[data-toggle="offcanvas"]').on('click', function () {
		$('.sidebarmob').toggleClass('show');
	});



	$(document).on('click', '.eye-icon', function () {
		var sel = $(this).closest('.password-field').find('input');

		$(this).toggleClass('fa-eye fa-eye-slash');

		if (sel.attr('type') == 'password') {
			sel.attr('type', 'text');
		} else {
			sel.attr('type', 'password');
		}
	});

	/* ==== Testimonials Slider ==== */
	$(".hmtesti").owlCarousel({
		loop: true,
		margin: 30,
		dots: true,
		nav: false,
		autoplay: true,
		smartSpeed: 500,
		autoplayTimeout: 3000,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			990: {
				items: 2
			},
			1250: {
				items: 3
			}
		}
	});

	window.onscroll = function () { myFunction() };

	var header = document.getElementById("stickyheader");
	var sticky = header.offsetTop;

	function myFunction() {
		if (window.pageYOffset > sticky) {
			header.classList.add("sticky");
		} else {
			header.classList.remove("sticky");
		}
	}


})(jQuery);



