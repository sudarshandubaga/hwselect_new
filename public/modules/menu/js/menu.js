$(function() {
  $('.dd').nestable({ 
    dropCallback: function(details) {
        console.log(details);
       var order = new Array();
       $("li[data-id='"+details.destId +"']").find('ol:first').children().each(function(index,elem) {
         order[index] = $(elem).attr('data-id');
       });

       if (order.length === 0){
        var rootOrder = new Array();
        $("#nestable > ol > li").each(function(index,elem) {
          rootOrder[index] = $(elem).attr('data-id');
        });
       }

       $.post(APP_URL + '/admin/menu/post_index', 
        { '_token': $('input[name=_token]').val(),
            source : details.sourceId, 
          destination: details.destId, 
          order:JSON.stringify(order),
          rootOrder:JSON.stringify(rootOrder) 
        }, 
        function(data) {
          console.log('data '+data); 
        })
       .done(function() { 
          $( "#success-indicator" ).fadeIn(100).delay(1000).fadeOut();
       })
       .fail(function() {  })
       .always(function() {  });
     }
   });

  $('.delete_toggle').each(function(index,elem) {
      $(elem).click(function(e){
        e.preventDefault();
        $('#postvalue').attr('value',$(elem).attr('rel'));
        $('#deleteModal').modal('toggle');
      });
  });
});
function load_menu_edit_form(id) {
    $.getJSON(APP_URL + '/admin/menu/edit/' + id, function (data) {
        $('#id').val(data.id);
        $('#title_edit').val(data.title);
        $('#label_edit').val(data.label);
        $('#url_edit').val(data.url);
        $('#editModal').modal('show');
    });
}
function delete_menu(id) {
    var is_confirm = confirm("Are you sure you want to delete this Menu?");
    if (is_confirm) {
        $.ajax({
            type: 'DELETE',
            url: 'menu/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                toastr.success('Successfully deleted Menu!', 'Success Alert', {timeOut: 5000});
                $('#item_' + id).remove();
                $('.col1').each(function (index) {
                    $(this).html(index + 1);
                });
            }
        });
    }
}