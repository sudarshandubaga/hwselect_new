@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }
</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Contact Enquiries List</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title"> Manage Contact Enquiries</h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">{{ request('type') }} Enquiries</span> </div>

                    </div>

                    <div class="portlet-body">

                        <div>
                            {{ $enquiries->firstItem() }} - {{ $enquiries->lastItem() }} of {{ $enquiries->total() }} are showing.
                        </div>

                        <div class="table-container">
                            {{ Form::open(['url' => route('list.admin.contact'), 'method' => 'DELETE']) }}
                            <div style="margin: 10px 0;" class="text-right">
                                <button type="button" class="btn btn-danger delete-btn"> <i class="fa fa-trash"></i> Remove</button>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th>Message</th>
                                            <th style="text-align: center;">
                                                <div>
                                                    <label>
                                                        <input type="checkbox" class="group-checkable" id="selectall">
                                                        All
                                                    </label>
                                                </div>
                                            </th>
                                            <!-- <th>Message</th> -->
                                        </tr>
                                    </thead>
    
                                    <tbody>
                                        @foreach($enquiries as $key => $ce)
                                        <tr>
                                            <td>{{ $ce->full_name }}</td>
                                            <td>{{ $ce->email }}</td>
                                            <td>{{ $ce->phone }}</td>
                                            <td>{{ date("F d, Y h:i:s A", strtotime($ce->created_at)) }}</td>
                                            <td>
                                                <a href="#message_modal_{{ $ce->id }}" data-toggle="modal">
                                                    <i class="fa fa-plus"></i> View Message
                                                </a>

                                                <div id="message_modal_{{ $ce->id }}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Message</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{ $ce->message_txt }}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                        </div>

                                                    </div>
                                                    </div>
                                            </td>
                                            <!-- <td>{{ $ce->message_txt }}</td> -->
                                            <td>
                                                <div>
                                                    <label>
                                                        <input type="checkbox" class="checkboxes" name="check[]" value="{{ $ce->id }}" id="check_{{ $ce->id }}">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
    
                                </table>
                            </div>

                            {{ Form::close() }}

                            
                        </div>
                        
                    </div>
                    {{ $enquiries->appends(['type' => request()->get('type')])->links() }}

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts') 

<script>

    $(function () {

        $(document).on('click', '#selectall', function () {
            // alert();
            $('.checkboxes').prop('checked', $(this).prop('checked'));

            if ($(this).prop('checked')) {
                $('.checkboxes').parent().addClass('checked');
            } else {
                $('.checkboxes').parent().removeClass('checked');

            }
        });

        $(document).on('click', '.delete-btn', function () {
            let form = $(this).closest('form');

            if(form.find('.checkboxes:checked').length) {
                swal({
                    title: "Are you sure?",
                    content: "Once deleted, you can not be able to recover these record(s)",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.trigger('submit');
                    }
                });
            } else {
                swal('Warning!', 'Please select at least one record to delete.', 'warning');
            }
        });

        $('#admin_user_datatable_ajax').DataTable({

            "order": [[0, "asc"]],

            processing: true,

            serverSide: true,

            stateSave: false,
            
            pagingType: 'full_numbers',

            /*

             searching: false,

             paging: true,

             info: true,

             */

            ajax: '{!! route('fetch.data.admin.users') !!}',

            columns: [

                /*{data: 'id_checkbox', name: 'id_checkbox', orderable: false, searchable: false},*/

                {data: 'name', name: 'admins.name'},

                {data: 'email', name: 'admins.email'},

                {data: 'role_name', name: 'roles.role_name'},

                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]

        });

    });

    function delete_user(id,name,role) {
        const alertMessage = `Are you sure! you want to delete admin user?
        Name: ${name}
        Role: ${role}`;
        if (confirm(alertMessage)) {

            $.post("{{ route('delete.admin.user') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            var table = $('#admin_user_datatable_ajax').DataTable();

                            table.row('admin_user_dt_row_' + id).remove().draw(false);

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

</script> 

@endpush