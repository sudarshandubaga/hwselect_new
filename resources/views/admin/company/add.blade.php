@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('list.companies') }}">Companies</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Add Company</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->        
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Company Form</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                        </ul>
                        {!! Form::open(array('method' => 'post', 'route' => 'store.company', 'class' => 'form', 'files'=>true)) !!}
                        <div class="tab-content">              
                            <div class="tab-pane fade active in" id="Details"> @include('admin.company.forms.form') </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection

@push('scripts')
<script type="text/javascript">
        if ($("form").length > 0) {
    var msg = 'Enter a valid Mobile Number for (2 Step Verification) secure Login';
    @if(!$siteSetting->verify_registration_phone_number)
        var msg = 'Please Enter a Valid Contact Number';
    @endif
     $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                        remote: {
                            type: 'get',
                            url: "{{url('check-employer_name')}}",
                            data: {
                              employer_name: function() {
                                return $( ".employer_name" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                    }, 
                   
                    phone: {
                        required: true,
                        minlength: 11,

                        remote: {
                            type: 'get',
                            url: "{{url('check-phone?type=emp')}}",
                            data: {
                                phone: function() {
                                    return $( "#phone" ).val();
                                }
                            },  
                            dataType: 'json'
                        },
                    },

                    email: {
                        required: true,
                        email:true,
                        remote: {
                            type: 'get',
                            url: "{{ url('check-email') }}?type=emp",
                            data: {
                              email: function() {
                                return $( "#email" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
                        
                    },
                    
                },
                messages: {

                    name: {
                        required: "Company Name required",
                        remote: "A company with this name already exists and cannot be duplicated, please modify the name Example: IBM LTD, IBM London, IBM Leeds ETC",
                    },


                    email: {
                        required: "Email required",
                        remote: "This Email is already taken."
                    }, 

                    phone: {
                        required: "Phone Number required",
                        minlength: msg,
                        remote: "This Phone Number is already taken."
                    },

                },
            
            })
        }
</script>
@endpush