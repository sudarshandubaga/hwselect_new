@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }

</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Companies</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage Companies</h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Companies</span> </div>

                        <div class="actions"> <a href="{{ route('create.company') }}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New Company</a> </div>

                    </div>
					
					<div class="row text-center">
						<div class="col-md-4"><strong>Is Active</strong> = Registered Only</div>
						<div class="col-md-4"><strong>Active</strong> = Registered & Email Verified</div>
						<div class="col-md-4"><strong>Inactive</strong> =  Registered but Email not verified</div>
						 
					</div>
					<hr>
                    <div class="portlet-body">

                        <div class="table-container">

                            <form method="post" role="form" id="datatable-search-form">

                                <table class="table table-striped table-bordered table-hover"  id="companyDatatableAjax">

                                    <thead>

                                        <tr role="row" class="filter">

                                            <td>{!! Form::select('name', ['' => 'Select Company']+$companies, null, array('id'=>'name', 'class'=>'form-control')) !!}</td>

                                            <td><input type="text" class="form-control" name="email" id="email" autocomplete="off" placeholder="Company Email"></td>


                                            <td><input type="text" class="form-control" name="phone" id="phone" autocomplete="off" placeholder="Phone Number"></td>

                                             <td>
                                                <input type="date" class="form-control" name="date_from" id="date_from" autocomplete="off" placeholder="Date From">

                                                <input type="date" class="form-control" name="date_to" id="date_to" autocomplete="off" placeholder="Date To">
                                            </td>

                                            <td><select name="is_active" id="is_active" class="form-control">

                                                    <option value="-1">Is Active?</option>

                                                    <option value="1" selected="selected">Active</option>

                                                    <option value="0">In Active</option>

                                                </select></td>

                                            <td style="display: none;"><select name="is_featured" id="is_featured" class="form-control">

                                                    <option value="-1">Is Featured?</option>

                                                    <option value="1">Featured</option>

                                                    <option value="0">Not Featured</option>

                                                </select></td>

                                            <td colspan="2"></td>

                                        </tr>

                                        <tr role="row" class="heading">

                                            <th>Name</th>

                                            <th>Email</th>

                                            <th>Phone Number</th>

                                            <th>Registration Date</th>

                                            <th>Is Active?</th>

                                            <th style="display: none;">Is Featured?</th>

                                            <th>Actions</th>

                                            <th class="text-center">
                                                <button type="button" class="btn btn-danger delete">Delete</button>
                                                <br>
                                                Select All<br><input type="checkbox" id="selectall" name="selectall" onclick="toggle(this)" autocomplete="off"></th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    </tbody>

                                </table>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts') 

<script>

    $(function () {

        var oTable = $('#companyDatatableAjax').DataTable({

            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,
            
            
            pagingType: 'full_numbers',

            /*		

             "order": [[1, "asc"]],            

             paging: true,

             info: true,

             */

            ajax: {

                url: '{!! route('fetch.data.companies') !!}',

                data: function (d) {

                    d.name = $('#name').val();

                    d.email = $('#email').val();

                    d.phone = $('#phone').val();

                    d.date_from = $('#date_from').val();

                    d.date_to = $('#date_to').val();

                    d.is_active = $('#is_active').val();

                }

            }, columns: [

                {data: 'name', name: 'name'},

                {data: 'email', name: 'email'},

                {data: 'phone', name: 'phone'},

                 {data: 'created_at', name: 'created_at'},

                {data: 'is_active', name: 'is_active'},

                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox'},

            ]

        });

        $('#datatable-search-form').on('submit', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#name').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#email, #phone').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        
        $('#date_from').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#date_to').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#is_active').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        
    });

    function deleteCompany(id,name) {

        var msg = 'Confirm you want to delete ('+name+')';

        if (confirm(msg)) {

            $.post("{{ route('delete.company') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            var table = $('#companyDatatableAjax').DataTable();

                            table.row('companyDtRow' + id).remove().draw(false);

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

    function makeActive(id) {

        $.post("{{ route('make.active.company') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#companyDatatableAjax').DataTable();

                        table.row('companyDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeNotActive(id) {

        $.post("{{ route('make.not.active.company') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#companyDatatableAjax').DataTable();

                        table.row('companyDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeFeatured(id) {

        $.post("{{ route('make.featured.company') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#companyDatatableAjax').DataTable();

                        table.row('companyDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeNotFeatured(id) {

        $.post("{{ route('make.not.featured.company') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#companyDatatableAjax').DataTable();

                        table.row('companyDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

     toggle = function(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i] != source)
    checkboxes[i].checked = source.checked;
    }
    }
    $('.delete').on('click',function(){

        if(!confirm('Are you sure you want to delete?')){
            e.preventDefault();
            return false;
        } else {
            var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
                return this.value;
            }).get();
            var ids = checkedVals.join(",");
            $.ajax({
            method: "GET",
            url: "{{route('delete.companies')}}",
            data: { ids: ids}
            })
            .done(function( msg ) {
                location.reload();
            });
        }

    })
    @if(isset(request()->is_verified))
        $('#is_verified').val({{request()->is_verified}});
    @endif
</script> 

@endpush