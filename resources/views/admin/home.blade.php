@extends('admin.layouts.admin_layout')

@section('content')

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content" style="background-color:#eef1f5;"> 

        <!-- BEGIN PAGE HEADER-->     

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="index.html">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>{{ $siteSetting->site_name }} Admin Panel</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title"> {{ $siteSetting->site_name }} Admin Panel </h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12 col-sm-12">
				<div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 green" href="{{ route('list.users') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalTodaysUsers }}</span> </div>

                            <div class="desc"> Total Users </div>

                        </div>

                    </a> </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('list.users','is_active=1') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalActiveUsers }}</span> </div>

                            <div class="desc"> Active Users </div>

                        </div>

                    </a> </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('list.users','is_verified=1') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalVerifiedUsers }}</span> </div>

                            <div class="desc"> Verified Users </div>

                        </div>

                    </a> </div>
					
					 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('list.applicants') }}">
                        <div class="visual"> <i class="fa fa-user"></i> </div>
                        <div class="details">
                            <div class="number"> <span data-counter="counterup" data-value="{{$totalAppliedUsers}}">{{ $totalAppliedUsers}}</span> </div>
                            <div class="desc"> Review Job Applications </div>
                        </div>
                    </a> </div>
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('list.onhold.jobs') }}">
                        <div class="visual"> <i class="fa fa-user"></i> </div>
                        <div class="details">
                            <div class="number"> <span data-counter="counterup" data-value="{{$totalRejectedJobs}}">{{$totalRejectedJobs}}</span> </div>
                            <div class="desc"> On-Hold (Pending) </div>
                        </div>
                    </a> </div>
					
					
				<!-- </div>
            </div>

            <div class="col-md-6 col-sm-6">
				<div class="row"> -->
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('list.jobs') }}">

                        <div class="visual"> <i class="fa fa-list"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalJobs }}</span> </div>

                            <div class="desc"> Total Jobs </div>

                        </div>

                    </a> </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('list.jobs','is_active=1') }}">

                        <div class="visual"> <i class="fa fa-list"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalActiveJobs }}</span> </div>

                            <div class="desc"> Active Jobs </div>

                        </div>

                    </a> </div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('list.jobs',['is_active=0','admin_reviewed=0']) }}">

                        <div class="visual"> <i class="fa fa-list"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{$totalPendingJobs}}</span> </div>

                            <div class="desc">Jobs Awaiting Approval </div>

                        </div>

                    </a>
				</div>
                
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('list.companies','is_verified=1') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalVerifiedCompanies}}</span> </div>

                            <div class="desc"> Verified Companies </div>

                        </div>

                    </a> </div>


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 green" href="{{ route('list.companies') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalCompanies}}</span> </div>

                            <div class="desc"> Total Companies </div>

                        </div>

                    </a> </div>

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('list.request.jobs') }}">

                        <div class="visual"> <i class="fa fa-user"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="{{$totalDeleteJobs}}">{{ $totalDeleteJobs}}</span> </div>

                            <div class="desc"> Jobs Awaiting Deletion </div>

                        </div>

                    </a> </div>

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <div>
                            <a class="dashboard-stat dashboard-stat-v2 green" href="javascript: return void()">
                                <div class="visual"> <i class="fa fa-user"></i> </div>
                                <div class="details">
                                    <div class="number"> <span data-counter="counterup" data-value="{{$totalNewUsers}}">{{$totalNewUsers}}</span> </div>
                                    <div class="desc">New Jobseekers</div>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 red" href="javascript: return void()">
                            <div class="visual"> <i class="fa fa-user"></i> </div>
                            <div class="details">
                                <div class="number"> <span data-counter="counterup" data-value="{{$totalNewCompanies}}">{{$totalNewCompanies}}</span> </div>
                                <div class="desc">New Company Registration</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('list.admin.contact', ['type' => 'Company']) }}">
                            <div class="visual"> <i class="fa fa-user"></i> </div>
                            <div class="details">
                                <div class="number"> <span data-counter="counterup" data-value="{{$totalCompanyEnquiries}}">{{$totalCompanyEnquiries}}</span> </div>
                                <div class="desc">Company Enquiries</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('list.admin.contact', ['type' => 'Job Seeker']) }}">
                            <div class="visual"> <i class="fa fa-user"></i> </div>
                            <div class="details">
                                <div class="number"> <span data-counter="counterup" data-value="{{$totalJobseekerEnquiries}}">{{$totalJobseekerEnquiries}}</span> </div>
                                <div class="desc">Jobseeker Enquiries</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green" href="{{ route('list.abused.jobs') }}">
                            <div class="visual"> <i class="fa fa-user"></i> </div>
                            <div class="details">
                                <div class="number"> <span data-counter="counterup" data-value="{{$totalAbusedJobs}}">{{$totalAbusedJobs}}</span> </div>
                                <div class="desc">Abused Reported Jobs</div>
                            </div>
                        </a>
                    </div>
				</div>

                   
				
                <?php /*?><div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('list.jobs','is_featured=1') }}">

                        <div class="visual"> <i class="fa fa-list"></i> </div>

                        <div class="details">

                            <div class="number"> <span data-counter="counterup" data-value="1349">{{ $totalFeaturedJobs }}</span> </div>

                            <div class="desc"> Featured Jobs </div>

                        </div>

                    </a>
				</div><?php */?>

            </div>

        </div>
		
		<?php $expired_jobs = App\Job::where('expiry_date' ,'<', \Carbon\Carbon::now())->count(); ?>
		<div class="expiredjobsalert">
			<div class="row">
				<div class="col-md-8"><p>{{$expired_jobs}} Expired Jobs – Please review to edit /modify or confirm to archive</p></div>
				<div class="col-md-4"><a href="{{ route('list.expired.jobs') }}">View Expired Jobs</a></div>
			</div>
		</div>
		
		
		
        <div class="row">

            <div class="col-md-6 col-sm-6">

                <div class="portlet light bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-share font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">Recent Registered Users</span> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="slimScrol">

                            <ul class="feeds">

                                @foreach($recentUsers as $recentUser)

                                <li>
                                    <div class="col1">

                                        <div class="cont">

                                            <div class="cont-col1">

                                                <div class="label label-sm label-info"> <i class="fa fa-check"></i> </div>

                                            </div>

                                            <div class="cont-col2">
                                                
                                                <div class="desc">
                                                    <a href="{{ route('edit.user', $recentUser->id) }}"> {{ $recentUser->name }} ({{ $recentUser->email }}) </a>  - <i class="fa fa-home" aria-hidden="true"></i> {{ $recentUser->getLocation()}}

                                                    -
                                                    <span class="fa fa-calendar"></span> {{ date('d M Y', strtotime($recentUser->created_at)) }}
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                                @endforeach

                            </ul>

                        </div>

                        <div class="scroller-footer">

                            <div class="btn-arrow-link pull-right"> <a href="{{ route('list.users') }}">See All Users</a> <i class="icon-arrow-right"></i> </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-6 col-sm-6">

                <div class="portlet light bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-share font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">Recent Registered Companies</span> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="slimScrol">

                            <ul class="feeds">

                                @foreach($recentCompanies as $recentCompany)

                                <li>

                                    <div class="col1">

                                        <div class="cont">

                                            <div class="cont-col1">

                                                <div class="label label-sm label-info"> <i class="fa fa-check"></i> </div>

                                            </div>

                                            <div class="cont-col2">

                                                <div class="desc">
                                                    <a href="{{ route('edit.company', $recentCompany->id) }}"> {{ $recentCompany->name }} ({{ $recentCompany->email }}) </a>  - <i class="fa fa-home" aria-hidden="true"></i> {{ $recentCompany->getLocation()}}
                                                    -
                                                    <span class="fa fa-calendar"></span> {{ date('d M Y', strtotime($recentCompany->created_at)) }}
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                                @endforeach

                            </ul>

                        </div>

                        <div class="scroller-footer">

                            <div class="btn-arrow-link pull-right"> <a href="{{ route('list.companies') }}">See All Companies</a> <i class="icon-arrow-right"></i> </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-12 col-sm-12">

                <div class="portlet light bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-share font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">Recent Jobs</span> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="slimScrol">

                            <ul class="feeds">

                                @foreach($recentJobs as $recentJob)

                                <li>

                                    <div class="col1">

                                        <div class="cont">

                                            <div class="cont-col1">

                                                <div class="label label-sm label-info"> <i class="fa fa-check"></i> </div>

                                            </div>

                                            <div class="cont-col2">

                                                <div class="desc">
                                                    <a href="{{ route('edit.job', $recentJob->id) }}"> {{ \Illuminate\Support\Str::limit($recentJob->title, 50) }} </a>  - <i class="fa fa-list" aria-hidden="true"></i> {{ $recentJob->getCompany('name') }} - <i class="fa fa-home" aria-hidden="true"></i> {{ $recentJob->getLocation() }}
                                                    -
                                                    <span class="fa fa-calendar"></span> {{ date('d M Y', strtotime($recentJob->created_at)) }}
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                                @endforeach                  

                            </ul>

                        </div>

                        <div class="scroller-footer">

                            <div class="btn-arrow-link pull-right"> <a href="{{ route('list.jobs') }}">See All Jobs</a> <i class="icon-arrow-right"></i> </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts')

<script type="text/javascript">

    $(function () {

        $('.slimScrol').slimScroll({

            height: '250px',

            railVisible: true,

            alwaysVisible: true

        });

    });

</script>

@endpush