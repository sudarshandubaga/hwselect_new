
<div class="form-body" style="width: 100%">
{!! Form::hidden('id', null) !!}        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'meta_title') !!}">
        {!! Form::label('meta_title', 'Meta title', ['class' => 'bold']) !!}
        {!! Form::text('meta_title', null, array('class'=>'form-control', 'id'=>'meta_title', 'maxlength'=>'65', 'placeholder'=>'Meta title')) !!}
        
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'meta_description') !!}">
        {!! Form::label('meta_description', 'Meta description', ['class' => 'bold']) !!}
        {!! Form::textarea('meta_description', null, array('class'=>'form-control', 'id'=>'meta_description', 'placeholder'=>'Meta description', 'maxlength'=>'165')) !!}
        
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'meta_keywords') !!}">
        {!! Form::label('meta_keywords', 'Meta Keyword', ['class' => 'bold']) !!}
        {!! Form::text('meta_keywords', null, array('class'=>'form-control', 'id'=>'meta_keywords', 'placeholder'=>'Meta Keyword')) !!}
       
    </div>
    <div class="form-actions">
        {!! Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary btnmeta', 'type'=>'submit')) !!}
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
      $(".btnmeta").click(function(){
        $(".forms").submit();
    });
</script>
@endpush