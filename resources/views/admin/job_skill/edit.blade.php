@extends('admin.layouts.admin_layout')

@section('content')

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <a href="{{ route('list.job.skills') }}">Skill Acquired</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Edit Skill Acquired</span> </li>

            </ul>

        </div>

        <!-- END PAGE BAR -->        

        <!-- END PAGE HEADER-->

        <br />

        @include('flash::message')

        <div class="row">

            <div class="col-md-12">

                <div class="portlet light bordered">

                    <div class="portlet-title">

                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Skill Acquired Form</span> </div>

                    </div>

                    <div class="portlet-body form">          

                        <ul class="nav nav-tabs">              

                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                            <li> <a href="#SEODetails" data-toggle="tab" aria-expanded="false"> S.E.O. </a> </li>

                        </ul>

                        {!! Form::model($jobSkill, array('method' => 'put', 'route' => array('update.job.skill', $jobSkill->id), 'class' => 'form', 'files'=>true)) !!}

                        {!! Form::hidden('id', $jobSkill->id) !!}            

                        <div class="tab-content">              

                            <div class="tab-pane fade active in" id="Details"> @include('admin.job_skill.forms.form') </div>
                            <div class="tab-pane fade active in" id="SEODetails"> @include('admin.job_skill.forms.seo_form') </div>

                        </div>

                        <div class="form-actions">

                            {!! Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}

                        </div>

                        {!! Form::close() !!}

                    </div>

                </div>

            </div>

        </div>

        <!-- END CONTENT BODY --> 

    </div>

    @endsection