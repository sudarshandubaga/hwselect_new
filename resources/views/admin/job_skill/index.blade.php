@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }	
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }
</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>Skill Acquired</span> </li>

            </ul>

        </div>
        <br />

        @include('flash::message')

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage Skill Acquired </h3>

        <!-- END PAGE TITLE--> 

        <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12"> 

                <!-- Begin: life time stats -->

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">

                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Skill Acquired</span> </div>

                        <div class="actions"> <a href="{{ route('create.job.skill') }}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New Skill Acquired</a> </div>

                    </div>

                    <div class="portlet-body">

                        <div class="table-container">

                            <form method="post" role="form" id="jobSkill-search-form">

                                <table class="table table-striped table-bordered table-hover"  id="jobSkillDatatableAjax">

                                    <thead>

                                        <tr role="row" class="filter"> 

                                            <td>{!! Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('id'=>'lang', 'class'=>'form-control')) !!}</td>

                                            <td><input type="text" class="form-control" name="job_skill" id="job_skill" autocomplete="off" placeholder="Skill Acquired"></td>                      

                                            <td><select name="is_active" id="is_active"  class="form-control">

                                                    <option value="-1">Is Active?</option>

                                                    <option value="1" selected="selected">Active</option>

                                                    <option value="0">In Active</option>

                                                </select></td></tr>

                                        <tr role="row" class="heading">                                            

                                            <th>Language</th>

                                            <th>Skill Acquired</th>

                                            <th>Actions</th>
                                             <th class="text-center">
                                                <button class="btn btn-danger delete">Delete</button>
                                                <br>
                                                Select All: <br>
                                                <input type="checkbox" id="selectall" name="selectall" onclick="toggle(this)" autocomplete="off"></th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    </tbody>

                                </table>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END CONTENT BODY --> 

</div>

@endsection

@push('scripts') 

<script>

    $(function () {

        var oTable = $('#jobSkillDatatableAjax').DataTable({

            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,

            pagingType: 'full_numbers',


            /*		

             "order": [[1, "asc"]],            

             paging: true,

             info: true,

             */

            ajax: {

                url: '{!! route('fetch.data.job.skills') !!}',

                data: function (d) {

                    d.lang = $('#lang').val();

                    d.job_skill = $('input[name=job_skill]').val();

                    d.is_active = $('#is_active').val();

                }

            }, columns: [

                {data: 'lang', name: 'lang'},

                {data: 'job_skill', name: 'job_skill'},

                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},

            ]

        });

        $('#jobSkill-search-form').on('submit', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#lang').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#job_skill').on('keyup', function (e) {

            oTable.draw();

            e.preventDefault();

        });

        $('#is_active').on('change', function (e) {

            oTable.draw();

            e.preventDefault();

        });

    });

    function deleteJobSkill(id, is_default, title) {

        var msg = 'Are you sure you would you would like to delete, Skill Acquired ('+title+')';

        if (is_default == 1) {

            msg = 'Are you sure you would you would like to delete, Skill Acquired ('+title+')';

        }

        if (confirm(msg)) {

            $.post("{{ route('delete.job.skill') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})

                    .done(function (response) {

                        if (response == 'ok')

                        {

                            var table = $('#jobSkillDatatableAjax').DataTable();

                            table.row('jobSkillDtRow' + id).remove().draw(false);

                        } else

                        {

                            alert('Request Failed!');

                        }

                    });

        }

    }

    function makeActive(id) {

        $.post("{{ route('make.active.job.skill') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobSkillDatatableAjax').DataTable();

                        table.row('jobSkillDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

    function makeNotActive(id) {

        $.post("{{ route('make.not.active.job.skill') }}", {id: id, _method: 'PUT', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (response == 'ok')

                    {

                        var table = $('#jobSkillDatatableAjax').DataTable();

                        table.row('jobSkillDtRow' + id).remove().draw(false);

                    } else

                    {

                        alert('Request Failed!');

                    }

                });

    }

     toggle = function(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i] != source)
    checkboxes[i].checked = source.checked;
    }
    }
    $('.delete').on('click',function(){
        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();
        var ids = checkedVals.join(",");
        $.ajax({
          method: "GET",
          url: "{{route('delete.skills')}}",
          data: { ids: ids}
        })
        .done(function( msg ) {
            location.reload();
        });
    })

</script> 

@endpush