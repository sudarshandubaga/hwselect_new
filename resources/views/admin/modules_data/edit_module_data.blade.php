@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }  
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    } 

</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>All {{$module->module_term}}</span> </li>
               

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Update {{$module->module_term}} <small>{{$module->module_term}}</small> </h3>


<!-- end page title end breadcrumb -->
<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="card m-b-30">
            <div class="card-body">
                <div class="">

                    @if(session()->has('message.added'))
                    <div class="alert alert-success alert-dismissible fade show d-flex align-items-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! session('message.content') !!}.
                    </div>
                    @endif
                   <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">{{$module->module_term}} Form</span> </div>
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                        </ul>

                                        <form class="form-horizontal form-material" action="{{ route('update-module-data') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" value="{{$module->id}}" name="module_id">
                                            <input type="hidden" value="{{$module->thumbnail_height}}" name="thumbnail_height">
                                            <input type="hidden" value="{{$module->thumbnail_width}}" name="thumbnail_width">
                                            <input type="hidden" value="{{$data->id}}" name="id">
                                            <div class="row">
                                                <div class="col-md-12">
                                                     @if($module->id==1)
                                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                                        <label>Select Country</label>
                                                        {!! Form::select('title', ['' => 'Select Country']+$countries, null!==(old('title'))?old('title'):$data->title, array('class'=>'form-control', 'id'=>'title')) !!}
                                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                                    </div>
                                                    @else
                                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                                        <label>Title</label>
                                                        <input type="text" name="title" id="title" value="{{ null!==(old('title'))?old('title'):$data->title }}" placeholder="Enter Title" maxlength="35" required class="form-control">
                                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                                    </div>
                                                    @endif
                                                    
                                                </div>
                                                @if($module->page_description == 'on')
                                                <div class="col-md-12">
                                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                                        @if($module->id!=13)
                                                        <label>Menu Link</label>
                                                        <input type="text" name="description" class="form-control" value="{{ null!==(old('description'))?old('description'):$data->description }}">
                                                        @else
                                                        <label>Description</label>
                                                        <textarea name="description" id="description">{{ null!==(old('description'))?old('description'):$data->description }}</textarea>
                                                        @endif
                                                       
                                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                                    </div>
                                                </div>
                                                @endif
                                                @if($module->featured_image == 'on')
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                                        <label>Upload Image</label>
                                                        <input type="file" name="image" id="image" value="{{ old('image') }}" class="form-control">

                                                        <img style="width: 46%; padding-top: 14px;" src="{{asset('images/thumbnail/'.$data->image)}}" alt="">

                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-md-12">
                                                    <div class="text-right m-t-15">
                                                        <a href="{{route('modules-data',$module->slug)}}" class="btn btn-warning waves-effect waves-light btn-form"><i class="fas fa-arrow-alt-circle-left"></i>Back</a>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light btn-form">Update<i class="fas fa-arrow-alt-circle-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>


@endsection
@push('scripts')
@include('admin.shared.tinyMCEFront')
<script>
       @if($module->id==1)
$('#title').select2();

@endif
</script>
@endpush