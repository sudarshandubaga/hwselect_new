
@extends('admin.layouts.admin_layout')

@section('content')

<style type="text/css">

    .table td, .table th {

        font-size: 12px;

        line-height: 2.42857 !important;

    }   
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button{
        padding: 0px;
    }
</style>

<div class="page-content-wrapper"> 

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content"> 

        <!-- BEGIN PAGE HEADER--> 

        <!-- BEGIN PAGE BAR -->

        <div class="page-bar">

            <ul class="page-breadcrumb">

                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>

                <li> <span>All {{$module->module_name}}</span> </li>
                

            </ul>

        </div>

        <!-- END PAGE BAR --> 

        <!-- BEGIN PAGE TITLE-->

        <h3 class="page-title">Manage {{$module->module_name}} 
		
		<a href="{{ route('add-module-data',$module->slug) }}" class="btn btn-success" style="float: right;">
        <i class="glyphicon glyphicon-plus"></i>  Add New {{$module->module_term}}
                    </a>
			<div class="clearfix"></div>
		</h3>

<!-- end page title end breadcrumb -->
<div class="row">
    <div class="col-md-12">
        <div class="card m-b-30">
            <div class="card-body">
                @include('flash::message')
                @if(session()->has('message.added'))
                <div class="alert alert-success alert-dismissible fade show d-flex align-items-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! session('message.content') !!}.
                </div>
                @endif
                @if ($message = Session::get('warning'))
                <div class="alert alert-danger alert-dismissible fade show d-flex align-items-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! $message !!}.
                </div>
                @endif
                <table id="datatable" style="text-align: center;" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="search" id="country" class="form-control" placeholder="Search">
                            </th>
                            <th>
                                <select name="status" id="status" class="form-control">
                                    <option value>All</option>
                                    <option value="blocked">Blocked</option>
                                    <option value="active">Active</option>
                                </select>
                            </th>
                            <th colspan="2"></th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">Ip</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;">Created Date</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div>
</div>
</div>


@endsection
@push('scripts')
<script>
    $('.delete').on('click',function(){
        var title = $(this).data('title');
        if (confirm('Are you sure you want to delete {{$module->module_term}} ('+title+')')) {
            return true;
        } else {
            return false;
        }
    });

    function update_status(id) {
        var current_status = $("#sts_" + id + " span").html();
        $.ajax({
            type: 'GET',
            url: '{{url("/admin")}}/data-status/' + id + '/' + current_status,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(sts) {
                var class_label = 'success';
                if (sts != 'active')
                    var class_label = 'warning';
                $("#sts_" + id).html('<span class="btn btn-' + class_label + '">' + sts + '</span>');
            }
        });

    }
    
    $(document).ready(function() {

        var oTable = $('#datatable').DataTable({

            "autoWidth": false,

            "info": false,

            "JQueryUI": true,

            "ordering": true,

            "pageLength": 10,

            "pagingType": 'full_numbers',
             
            processing: true,

            serverSide: true,

            stateSave: false,

            searching: false,

            ajax: {

                url: '{!! route('fetch.data.ips') !!}',

                data: function (d) {

                    // d.lang = $('#lang').val();

                    d.country = $('#country').val();

                    d.status = $('#status').val();

                }

            },
            columns: [

                {data: 'title', name: 'title'},

                {data: 'status', name: 'status', orderable: false},

                {data: 'created_date', name: 'created_date'},

                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]

            

        });

        $('#country, #status').on('keyup change', function(e) {
            e.preventDefault();
            oTable.draw();
        });

        $(window).scrollTop(0);

    });
</script>
<style>
    .status {
        text-transform: capitalize;
    }
</style>
@endpush