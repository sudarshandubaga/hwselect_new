{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">        
    {!! Form::hidden('id', $letter->id) !!}
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'title') !!}">
        {!! Form::label('title', 'Title', ['class' => 'bold']) !!}
        {!! Form::text('title', $letter->title, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'title') !!}
    </div>
     <div class="form-group {!! APFrmErrHelp::hasError($errors, 'send_to') !!}" >
        {!! Form::label('send_to', 'Send to?', ['class' => 'bold']) !!}
        <div class="radio-list">
            
            <label class="radio-inline">
                <input id="default" name="send_to" type="radio" value="company">
                Companies </label>
            <label class="radio-inline">
                <input id="not_default" name="send_to" type="radio" value="seeker">
                Seekers </label>
        </div>          
        {!! APFrmErrHelp::showErrors($errors, 'send_to') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">
        {!! Form::label('description', 'Description', ['class' => 'bold']) !!}
        {!! Form::textarea('description', $letter->description, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'description') !!}
    </div>
    
   
    
    	
    <div class="form-actions">
        {!! Form::button('Send <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}
    </div>
</div>
@push('scripts')
@include('admin.shared.tinyMCEFront') 
@endpush