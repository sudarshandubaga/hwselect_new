<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="title">Sites Pages</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('list.cms') }}" class="nav-link "> <span class="title">List Site Pages</span> </a> </li>

        <li class="nav-item  "> <a href="{{ route('create.cms') }}" class="nav-link "> <span class="title">Add new Site Page</span> </a> </li>

      

    </ul>

</li>


<li class="nav-item "><a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-level-up" aria-hidden="true"></i> <span class="title">Manage Newsletters</span> <span class="arrow"></span> </a>

    <ul class="sub-menu">

        <li class="nav-item  "> <a href="{{ route('modules-data','newsletter') }}" class="nav-link "> <span class="title">List Newsletters</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('newsletter.subscribers') }}" class="nav-link "> <span class="title">List Subscribers</span> </a> </li>

    </ul>

</li>



