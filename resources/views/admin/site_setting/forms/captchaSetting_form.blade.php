{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'nocaptcha_sitekey') !!}">
        <a href="#siteKeyModal" data-toggle="modal" class="pull-right">
            <i class="icon-question"> HELP</i>
        </a>
        {!! Form::label('nocaptcha_sitekey', 'Sitekey', ['class' => 'bold']) !!}                    
        {!! Form::text('nocaptcha_sitekey', null, array('class'=>'form-control', 'id'=>'nocaptcha_sitekey', 'placeholder'=>'Sitekey')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'nocaptcha_sitekey') !!}                                       
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'nocaptcha_secret') !!}">
        {!! Form::label('nocaptcha_secret', 'Secret', ['class' => 'bold']) !!}                    
        {!! Form::text('nocaptcha_secret', null, array('class'=>'form-control', 'id'=>'nocaptcha_secret', 'placeholder'=>'Secret')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'nocaptcha_secret') !!}                                       
    </div>    
</div>

<div class="modal fade" id="siteKeyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Captcha Guide</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>Step by Step guide that how you can set register your domain for google recaptcha :-</b></p>
		<p>Step 1: Go on website <a href="https://www.google.com/recaptcha/about/" target="_blank">https://www.google.com/recaptcha/about/</a></p>
		<p>Step 2: Click on "v3 Admin Console" text in header.</p>
		<p>Step 3: Click on "+" icon in header.</p>
		<p>Step 4: Register a new site window will open after completion of step 3. </p>
		<p>
			<ul>
			<li>Type "Label" for your website</li>
			<li>select "reCAPTCHA type". It may be "reCAPTCHA v3"(automated) or "reCAPTCHA v2"(you have to select the boxes as google will tell you for submitting the form) depends on your selection.</li>		
			<li>Enter domain name without http or https and without / after domain name.</li>
			<li>Tick on "Accept the reCAPTCHA Terms of Service"</li>
			<li>Click on submit button.</li>
			</ul>
		</p>
		<p>Step 5: After click on submit button, a new window "Adding reCAPTCHA to your site" will be open. 
			<ul>
			<li>Click on "Copy site key" and paste it into field site key where you want to apply the reCAPTCHA</li>
			<li>Click on "Copy secret key" and paste it into field site key where you want to apply the reCAPTCHA</li>
			</ul>
		</p>
		<p>Step 6: That's it..!! </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--div class="modal fade" id="siteSecretModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div-->
