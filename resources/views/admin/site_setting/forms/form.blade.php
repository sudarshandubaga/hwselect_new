{!! APFrmErrHelp::showErrorsNotice($errors) !!}

@include('flash::message')

<div class="form-body">

    <div class="row">

        <div class="col-md-6">

            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'image') !!}">
			<a href="#changeSiteLogo" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>	
                <div class="fileinput fileinput-new" data-provides="fileinput">

                    <div class="fileinput-new thumbnail" style="width: 200px;">
						
						 @if(isset($siteSetting))

            {{ ImgUploader::print_image("sitesetting_images/thumb/$siteSetting->site_logo") }}  
			
				@else
						<img src="{{ asset('/') }}admin_assets/no-image.png" alt="" />
						
        @endif 
						
						
					
					</div>

                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>

                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Add or change site Logo </span> <span class="fileinput-exists"> Change </span> {!! Form::file('image', null, array('id'=>'image')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>

                </div>

                {!! APFrmErrHelp::showErrors($errors, 'image') !!} </div>

        </div>

        

    </div>

    

    

    <div class="row">

        <div class="col-md-6">

            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'favicon') !!}">

                <div class="fileinput fileinput-new" data-provides="fileinput">

                    <div class="fileinput-new thumbnail" style="width: 32px; height: 32px;">
						
						
						
						@if(isset($siteSetting))
            {{ ImgUploader::print_image("favicon.ico") }}   
			
        @else
						<img src="{{ asset('/') }}admin_assets/no-image.png" alt="" />
						
        @endif 
						
					</div>

                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 16px; max-height: 16px;"> </div>

                    <div> <span class="btn default btn-file"> <span class="fileinput-new">Add or change Favicon </span> <span class="fileinput-exists"> Change </span> {!! Form::file('favicon', null, array('id'=>'favicon')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>

                </div>

                <span id="name-error" class="help-block help-block-error">The favicon must be a file of type/extension ".ico"</span>

            </div>

        </div>

        

    </div>

    

    

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_name') !!}">

        {!! Form::label('site_name', 'Site Name', ['class' => 'bold']) !!}                    

        {!! Form::text('site_name', null, array('class'=>'form-control', 'id'=>'site_name', 'placeholder'=>'Site Name', 'maxlength' => 50)) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_name') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_slogan') !!}" style="display: none;">

        {!! Form::label('site_slogan', 'Site Slogan', ['class' => 'bold']) !!}                    

        {!! Form::text('site_slogan', null, array('class'=>'form-control', 'id'=>'site_slogan', 'placeholder'=>'Site Slogan')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_slogan') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_phone_primary') !!}">

        {!! Form::label('site_phone_primary', 'Primary Phone#', ['class' => 'bold']) !!}                    

        {!! Form::text('site_phone_primary', null, array('class'=>'form-control', 'id'=>'site_phone_primary', 'placeholder'=>'Primary Phone#')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_phone_primary') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_phone_secondary') !!}">

        {!! Form::label('site_phone_secondary', 'Secondary Phone#', ['class' => 'bold']) !!}                    

        {!! Form::text('site_phone_secondary', null, array('class'=>'form-control', 'id'=>'site_phone_secondary', 'placeholder'=>'Secondary Phone#')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_phone_secondary') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_from_address') !!}">
			<a href="#fromEmailAddress" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_from_address', 'From Email Address', ['class' => 'bold']) !!}                    

        {!! Form::text('mail_from_address', null, array('class'=>'form-control', 'id'=>'mail_from_address', 'placeholder'=>'From Email Address')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_from_address') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_from_name') !!}">
			<a href="#fromEmailName" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_from_name', 'From Email Name', ['class' => 'bold']) !!}                    

        {!! Form::text('mail_from_name', null, array('class'=>'form-control', 'id'=>'mail_from_name', 'placeholder'=>'From Email Name')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_from_name') !!}                                       

    </div>    

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_to_address') !!}">
			<a href="#toEmailAddress" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_to_address', 'To Email Address', ['class' => 'bold']) !!}                    

        {!! Form::text('mail_to_address', null, array('class'=>'form-control', 'id'=>'mail_to_address', 'placeholder'=>'To Email Address')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_to_address') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_to_name') !!}">
			<a href="#toEmailName" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_to_name', 'To Email Name', ['class' => 'bold']) !!}                    

        {!! Form::text('mail_to_name', null, array('class'=>'form-control', 'id'=>'mail_to_name', 'placeholder'=>'To Email Name')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_to_name') !!}                                       

    </div> 


    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_to_address') !!}">
			<a href="#ccEmailAddress" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_cc_address', 'CC Email Address', ['class' => 'bold']) !!} <span>please add (,) between two emails (Example: abc@123.com,xyz@123.com)</span>                

        {!! Form::text('mail_cc_address', null, array('class'=>'form-control', 'id'=>'mail_cc_address', 'placeholder'=>'CC Email Address')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_cc_address') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_cc_name') !!}">
			<a href="#ccEmailName" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>
        {!! Form::label('mail_cc_name', 'CC Email Name', ['class' => 'bold']) !!}                    

        {!! Form::text('mail_cc_name', null, array('class'=>'form-control', 'id'=>'mail_cc_name', 'placeholder'=>'CC Email Name')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'mail_cc_name') !!}                                       

    </div>
    @php
        $arra = \App\SiteSetting::first()->getPriorCountries(); // array();
        
    @endphp
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'default_country_id') !!}">

        {!! Form::label('default_country_id', 'Default Country', ['class' => 'bold']) !!}                    

        {!! Form::select('default_country_id',$arra+$countries, null, array('class'=>'form-control', 'id'=>'default_country_id')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'default_country_id') !!}                                       

    </div>

    <div style="display: none;" class="form-group {!! APFrmErrHelp::hasError($errors, 'country_specific_site') !!}">

        {!! Form::label('country_specific_site', 'Make site specific to this Country?', ['class' => 'bold']) !!}        <div class="radio-list">

            <label class="radio-inline">{!! Form::radio('country_specific_site', 1, true, ['id' => 'country_specific_site_yes']) !!} Yes </label>

            <label class="radio-inline">{!! Form::radio('country_specific_site', 0, null, ['id' => 'country_specific_site_no']) !!} No </label>

        </div>

        {!! APFrmErrHelp::showErrors($errors, 'country_specific_site') !!}                                       

    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'default_currency_code') !!}">

        {!! Form::label('default_currency_code', 'Default Currency Code', ['class' => 'bold']) !!}                    

        {!! Form::select('default_currency_code',$currency_codes, null, array('class'=>'form-control', 'id'=>'default_currency_code')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'default_currency_code') !!}                                       

    </div>
	
	<div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_user') !!}">
        {{ Form::checkbox('verify_registration_phone_number',null, null, array('id'=>'verify_registration_phone_number')) }}
        
        {!! Form::label('verify_registration_phone_number', 'Enable two-factor Mobile authentication and verification on all registration forms', ['class' => 'bold']) !!} 
        
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'show_social_links') !!}">
        {{ Form::checkbox('show_social_links',null, null, array('id'=>'show_social_links')) }}
        {!! Form::label('show_social_links', 'Enable Social Links In Profile', ['class' => 'bold']) !!}
    </div>
	
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_street_address') !!}">

        {!! Form::label('site_street_address', 'Street Address', ['class' => 'bold']) !!}                    

        {!! Form::textarea('site_street_address', null, array('class'=>'form-control', 'id'=>'site_street_address', 'maxlength'=>200, 'placeholder'=>'Street Address')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_street_address') !!}                                       

    </div>




       
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_newsletter_active') !!}">
			<a href="#newsletterModal" data-toggle="modal" class="pull-right">
				<i class="icon-question"> HELP</i>
			</a>            
            {!! Form::label('is_newsletter_active', 'Is Newsletter active?', ['class' => 'bold']) !!}
            <div class="radio-list">
                <label class="radio-inline">{!! Form::radio('is_newsletter_active', 1, null, ['id' => 'is_newsletter_active_yes']) !!} Yes </label>
                <label class="radio-inline">{!! Form::radio('is_newsletter_active', 0, true, ['id' => 'is_newsletter_active_no']) !!} No </label>
            </div>
            {!! APFrmErrHelp::showErrors($errors, 'is_newsletter_active') !!}
        </div>

       
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_testimonial_active') !!}">
            {!! Form::label('is_testimonial_active', 'Show Testimonial On Home Page', ['class' => 'bold']) !!}
            <div class="radio-list">
                <label class="radio-inline">{!! Form::radio('is_testimonial_active', 1, null, ['id' => 'is_testimonial_active_yes']) !!} Yes </label>
                <label class="radio-inline">{!! Form::radio('is_testimonial_active', 0, true, ['id' => 'is_testimonial_active_no']) !!} No </label>
            </div>
            {!! APFrmErrHelp::showErrors($errors, 'is_testimonial_active') !!}
        </div>  


  
        <!--div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_blog_active') !!}">
            {!! Form::label('is_blog_active', 'Show Blog On Home Page', ['class' => 'bold']) !!}
            <div class="radio-list">
                <label class="radio-inline">{!! Form::radio('is_blog_active', 1, null, ['id' => 'is_blog_active_yes']) !!} Yes </label>
                <label class="radio-inline">{!! Form::radio('is_blog_active', 0, true, ['id' => 'is_blog_active_no']) !!} No </label>
            </div>
            {!! APFrmErrHelp::showErrors($errors, 'is_blog_active') !!}
        </div-->  
 <?php 
    $expiry_dates  = array(
                    '1 week' => '1 week',
                    '2 week' => '2 weeks',
                    '1 month' => '1 month',
                    '2 month' => '2 months',
                    '3 month' => '3 months',
                    '6 month' => '6 months',
                );
    ?>
         <div class="form-group {!! APFrmErrHelp::hasError($errors, 'default_months') !!}">

        {!! Form::label('default_months', 'Set Default Job Expiry Date', ['class' => 'bold']) !!}    

        {!! Form::select('default_months', ['' => __('Choose Default Job Expiry Date')]+$expiry_dates, null, array('class'=>'form-control', 'id'=>'default_months')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'default_months') !!}                                       

    </div>

<div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_testimonial_active') !!}">
    {!! Form::label('company_details', 'Company Registration Details', ['class' => 'bold']) !!}
    <div class="radio-list">
        {!! Form::text('company_details', null, ['class' => 'form-control', 'placeholder' => 'Company Registration Details']) !!}
    </div>
    {!! APFrmErrHelp::showErrors($errors, 'company_details') !!}
</div>
<div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_testimonial_active') !!}">
    {!! Form::label('job_id_initials', 'Job ID Initials', ['class' => 'bold']) !!}
    <div class="radio-list">
        {!! Form::text('job_id_initials', null, ['class' => 'form-control', 'placeholder' => 'Job ID Initials', 'maxlength' => 4]) !!}
    </div>
    {!! APFrmErrHelp::showErrors($errors, 'job_id_initials') !!}
</div>

       

</div>
<div class="modal fade" id="changeSiteLogo" tabindex="-1" role="dialog" aria-labelledby="changeSiteLogoLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="changeSiteLogoLabel">Add or change site Logo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>Logo size information of site logo :-</b></p>
		<p>Logo maximum width should not be greater than 235px <br/>and<br/> maximum height should not be greater than 46px.<br/> You can use any aspect ratio of your logo but keep in mind maximum width and maximum height as explain for better visibility of your company logo.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="newsletterModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newsletterModalLabel">Newsletter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>Newsletter :-</b></p>
		<p>Enabling newsletter allows users to select this button during the registration process</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="fromEmailAddress" tabindex="-1" role="dialog" aria-labelledby="fromEmailAddressLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fromEmailAddressLabel">From Email Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>From Email Address :-</b></p>
		<p>From Email Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="fromEmailName" tabindex="-1" role="dialog" aria-labelledby="fromEmailNameLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fromEmailNameLabel">From Email Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>From Email Name :-</b></p>
		<p>From Email Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="toEmailAddress" tabindex="-1" role="dialog" aria-labelledby="toEmailAddressLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="toEmailAddressLabel">To Email Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>To Email Address :-</b></p>
		<p>To Email Address Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="toEmailName" tabindex="-1" role="dialog" aria-labelledby="toEmailNameLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="toEmailNameLabel">To Email Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>To Email Name :-</b></p>
		<p>To Email Name Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ccEmailAddress" tabindex="-1" role="dialog" aria-labelledby="ccEmailAddressLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ccEmailAddressLabel">CC Email Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>CC Email Address :-</b></p>
		<p>CC Email Address Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ccEmailName" tabindex="-1" role="dialog" aria-labelledby="ccEmailNameLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ccEmailNameLabel">CC Email Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>CC Email Name :-</b></p>
		<p>CC Email Name Description</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>