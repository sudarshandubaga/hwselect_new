{!! APFrmErrHelp::showErrorsNotice($errors) !!}

@include('flash::message')

<div class="form-body">
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'site_google_map') !!}">
        <a href="#googlemapModal" data-toggle="modal" class="pull-right">
            <i class="icon-question"> HELP</i>
        </a>
        {!! Form::label('site_google_map', 'Map for Contact us page', ['class' => 'bold']) !!}                    

        {!! Form::textarea('site_google_map', null, array('class'=>'form-control', 'id'=>'site_google_map', 'placeholder'=>'Add map iframe here')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'site_google_map') !!}
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'autocomplete_api') !!}">
        {!! Form::label('autocomplete_api', 'Autocomplete API Key', ['class' => 'bold']) !!}                    

        {!! Form::text('autocomplete_api', null, array('class'=>'form-control', 'id'=>'autocomplete_api', 'placeholder'=>'Location API Key')) !!}

        {!! APFrmErrHelp::showErrors($errors, 'autocomplete_api') !!}
    </div>
</div>

<div class="modal fade" id="googlemapModal" tabindex="-1" role="dialog" aria-labelledby="googlemapModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="googlemapModalLabel">Google Map Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>Step by Step guide that how you can add address map to contact us page :-</b></p>
		<!--p><img src="{{url('images/help/recaptcha-step-2.png')}}" /></p-->
		<p>Step 1: Go on Google Maps website <a href="https://www.google.com/maps" target="_blank">https://www.google.com/maps</a></p>
		<p>Step 2: Type the address here as shown in screenshot which you want to display on contact page.</p>
		<p><img src="{{url('images/help/googlemap01.png')}}" /></p>
		<p>Step 3: For ex. I entered address <b>Ripon Gardens, Aldwick, Bognor Regis, West Sussex, PO21 3RF</b> and it showed me map as below :-</p>
		<p><img src="{{url('images/help/googlemap02.png')}}" /></p>
		<p>Step 4: Now click on <b>share button</b> as shown below:- </p>
		<p><img src="{{url('images/help/googlemap03.png')}}" /></p>
		<p>Step 5: Now click on <b>Embed a map</b> as shown below:- </p>
		<p><img src="{{url('images/help/googlemap04.png')}}" /></p>
		<p>Step 6: Now click on <b>COPY HTML</b> as shown below:- </p>
		<p><img src="{{url('images/help/googlemap05.png')}}" /></p>
		<p>Step 7: Now paste the copied code in your <b> admin > Site Settings > Manage Site Settings > Google Maps & API  as shown below </b> :- </p>
		<p><img src="{{url('images/help/googlemap06.png')}}" /></p>
		<p>Step 8: Now click on <b>Update</b> button as shown below, That's it..!! </p>
		<p><img src="{{url('images/help/googlemap07.png')}}" /></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>