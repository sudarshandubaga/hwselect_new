@extends('admin.layouts.admin_layout')
@section('content')
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <a href="{{ route('list.users') }}">Manage Job Seeker</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Edit Job Seeker</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR --> 
        <!-- BEGIN PAGE TITLE-->
        <!--<h3 class="page-title">Edit User <small>Users</small> </h3>-->
        <!-- END PAGE TITLE--> 
        <!-- END PAGE HEADER-->
        <br />
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase">Job Seeker Form</span>&nbsp &nbsp &nbsp       {{$user->first_name}} {{$user->last_name}}</div>            
                    </div>
                    <div class="portlet-body form">          
                        <ul class="nav nav-tabs">              
                            <li class="active"> <a href="#Details" data-toggle="tab" aria-expanded="false"> Details </a> </li>
                            <li><a href="#Summary" data-toggle="tab" aria-expanded="false">Summary</a></li>
                            <li><a href="#CV" data-toggle="tab" aria-expanded="false">C.V</a></li>
                            <li style="display: none;"><a href="#Projects" data-toggle="tab" aria-expanded="false">Projects</a></li>
                            <li style="display: none;"><a href="#Experience" data-toggle="tab" aria-expanded="false">Experience</a></li>
                            <li style="display: none;"><a href="#Education" data-toggle="tab" aria-expanded="false">Education</a></li>
                            <li><a href="#Skills" data-toggle="tab" aria-expanded="false">Skills</a></li>
                            <li><a href="#Languages" data-toggle="tab" aria-expanded="false">Languages</a></li>
                        </ul>

                        <div class="tab-content">              
                            <div class="tab-pane fade active in" id="Details"> @include('admin.user.forms.form') </div>
                            <div class="tab-pane fade" id="Summary"> @include('admin.user.forms.summary') </div>
                            <div class="tab-pane fade" id="CV"> @include('admin.user.forms.cv.cvs') </div>
                            <div class="tab-pane fade" id="Projects"> @include('admin.user.forms.project.projects') </div>
                            <div class="tab-pane fade" id="Experience"> @include('admin.user.forms.experience.experience') </div>
                            <div class="tab-pane fade" id="Education"> @include('admin.user.forms.education.education') </div>
                            <div class="tab-pane fade" id="Skills"> @include('admin.user.forms.skill.skills') </div>
                            <div class="tab-pane fade" id="Languages"> @include('admin.user.forms.language.languages') </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY --> 
    </div>
    @endsection

    @push('scripts')
<script type="text/javascript">
        if ($("form").length > 0) {
    var msg = 'Please Enter a Valid Mobile Number and Click Verify to Continue';
    @if(!$siteSetting->verify_registration_phone_number)
        var msg = 'Please enter a valid phone number';
    @endif
     $("form").validate({
                validateHiddenInputs: true,
                ignore: "",

                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    },

                    last_name: {
                        required: true,
                        minlength: 2,
                        digits: false,
                    }, 
                   
                    phone: {
                        required: true,
                        minlength: 11,
                    },

                    email: {
                        required: true,
                        email:true,
                    },
                    eligible_uk: {
                        required: true,
                    },
                    eligible_eu: {
                        required: true,
                    },
                    
                },
                messages: {

                    first_name: {
                        required: "First Name required",
                    },
                    last_name: {
                        required: "Last Name required",
                    },


                    email: {
                        required: "Email required",
                    }, 

                    phone: {
                        required: "Phone Number required",
                        minlength: msg,
                    },

                },
                errorPlacement: function(error, element)  {
                    if ( element.is(":radio") ) 
                    {
                        error.appendTo( element.parents('.radio-list') );
                    }
                    else 
                    { // This is the default behavior 
                        error.insertAfter( element );
                    }
                }
            
            })
        }
</script>
@endpush