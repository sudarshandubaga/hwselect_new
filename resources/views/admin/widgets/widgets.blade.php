@extends('admin.layouts.admin_layout')
@section('content')
<style type="text/css">
    .table td, .table th {
        font-size: 12px;
        line-height: 2.42857 !important;
    } 
    .dataTable td,
    .dataTable th {
        line-height: 1 !important;
    }
    .fade {
    opacity: 1 !important;
}  
</style>
<div class="page-content-wrapper"> 
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content"> 
        <!-- BEGIN PAGE HEADER--> 
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li> <a href="{{ route('admin.home') }}">Home</a> <i class="fa fa-circle"></i> </li>
                <li> <span>Page Content</span> </li>
            </ul>
        </div>
        <!-- END PAGE BAR --> 
        <!-- BEGIN PAGE TITLE-->
        <!-- END PAGE TITLE--> 
        <!-- END PAGE HEADER-->
        <br />
       @if(session()->has('message.added'))
        <div class="alert alert-success alert-dismissible fade show d-flex align-items-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <i class="mdi mdi-checkbox-marked-circle font-32"></i><strong class="pr-1">Success !</strong> {!! session('message.content') !!}.
        </div>
        @endif
        <div class="row">
            <div class="col-md-12"> 
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Manage Page Content</span> </div>
                        
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">
                            <form method="post" role="form" id="job-search-form">
                                <table class="table table-striped table-bordered table-hover"  id="jobDatatableAjax">



                                        <thead>
                            <tr>
                                
                                <th>Title</th>                             
                                <th>Page Location</th>                             
                                                          
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($user as $widget)
                                <tr class="item{{$widget->id}}">
                                   
                                    <td>{{$widget->heading}}</td>
                                    <td>{{$widget->location}}</td>

                                    <td>
                                         <a id="popup" target="_blank" class="edit-modal btn btn-success" href="{{$widget->prev_link}}"><span class="fa fa-eye"></span> Preview</a>

                                         <a id="popup" class="edit-modal btn btn-success" href="{{route('edit-widget',$widget->id)}}"><span class="fa fa-pencil"></span> Edit</a>
<!--                                         <button id="popup" class="delete-modal btn btn-danger" onClick="delete_widget({{$widget->id}});"><span class="fa fa-trash"></span> Delete</button>-->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                                    </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY --> 
</div>
@endsection
@push('scripts') 

@endpush