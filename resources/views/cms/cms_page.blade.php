@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>$cmsContent->page_title])

<!-- Inner Page Title end -->

<div class="about-wraper">

    <div class="container">

                <p>{!! $cmsContent->page_content !!}</p>

                   

    </div>  

</div>

@include('includes.footer')

@endsection