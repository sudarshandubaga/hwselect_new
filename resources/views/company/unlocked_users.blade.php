@extends('layouts.app')



@section('content') 



<!-- Header start --> 



@include('includes.header') 



<!-- Header end --> 



<!-- Inner Page Title start --> 

<style type="text/css">
    .uniform-inlineCheckbox1 .error{
            position: absolute;
            margin-left: 275px;
    }

    .error{
        color: red;
    }

</style>

@include('includes.inner_page_title', ['page_title'=>__('Short Listed Seekers')]) 

<!-- Inner Page Title end -->



<div class="listpgWraper">



    <div class="container">



        <div class="row">



            @include('includes.company_dashboard_menu')







            <div class="col-md-9 col-sm-8"> 



                <div class="myads">

                    <?php 



                    $st = '';

                    if($status=='called_for_interview'){

                        $st = 'Called for Interview';

                    }else if($status=='interviewed'){

                        $st = 'Interviewed';

                    }else if($status=='recruited'){

                        $st = 'Recruited';

                    }else if($status=='resigned'){

                        $st = 'Resigned';

                    }else if($status=='rejected'){

                        $st = 'Rejected';

                    }else{

                        $st = 'All';

                    }



                     ?>

                    

                    <h3>{{__('Shortlisted Candidates')}} ({{$st}})</h3>



                    <ul class="searchList">



                        <!-- job start --> 



                        @if(isset($users) && count($users))



                        @foreach($users as $user)



                        <li>



                            <div class="row">



                                <div class="col-md-9 col-sm-9">



                                    <div class="jobimg">{{$user->printUserImage(100, 100)}}</div>



                                    <div class="jobinfo">



                                        <h3><a href="{{route('user.profile', $user->id)}}">{{$user->getName()}}</a></h3>



                                        <div class="location"> {{trim($user->getLocation(),',')}}</div>



                                    </div>



                                    <div class="clearfix"></div>



                                </div>



                                <div class="col-md-3 col-sm-3">



                                    <div class="listbtn"><a href="javascript:;" onclick="view_message({{$user->id}},{{$job_id}})">{{__('View Note')}}</a></div>



                                     <div class="listbtn"><a href="javascript:;" onclick="send_message({{$user->id}})">{{__('Add a Note')}}</a></div>



                                </div>



                            </div>



                            <p>{{\Illuminate\Support\Str::limit($user->getProfileSummary('summary'),150,'...')}}</p>



                        </li>



                        <!-- job end --> 



                        @endforeach



                        @endif



                    </ul>



                </div>



            </div>



        </div>



    </div>



</div>

<div class="modal fade" id="sendmessage" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

            <form action="" id="send-form">

                @csrf

                <input type="hidden" name="seeker_id" id="seeker_id">

                <div class="modal-header">                    

                    <h4 class="modal-title">Add Note</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">

                    <div class="form-group">

                        <div class="form-check form-check-inline">

                          <input class="form-check-input" required="required" type="radio" id="inlineCheckbox1" value="called_for_interview" name="option">

                          <label class="form-check-label" for="inlineCheckbox1">Called for interview</label>

                        </div>

                        <div class="form-check form-check-inline">

                          <input class="form-check-input" required="required" type="radio" id="inlineCheckbox2" value="interviewed" name="option">

                          <label class="form-check-label" for="inlineCheckbox2">Interviewed</label>

                        </div>

                    </div>

                    <input type="hidden" name="job_id" value="{{$job_id}}">

                    <div class="form-group">

                        <input type="text" name="date" class="form-control datepicker">

                    </div>



                    <div class="form-group">

                        <textarea class="form-control" name="message" placeholder="Add a Note" id="message" cols="10" rows="3"></textarea>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>

            </form>

        </div>



    </div>

</div>



<div class="modal fade" id="showmessage" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

                <div class="modal-header">                    

                    <h4 class="modal-title">All Notes</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">

                    <div class="clearfix message-content">

                      <div class="message-details">

                      <h4> </h4>

                        <div id="append_messages"></div>

                      </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

        </div>



    </div>

</div>



@include('includes.footer')



@endsection



@push('scripts')

<script type="text/javascript">

    function send_message(id) {

        const el = document.createElement('div')

        el.innerHTML = "Please <a class='btn' href='{{route('login')}}' onclick='set_session()'>log in</a> as a Employer and try again."

        @if(null!==(Auth::guard('company')->user()))

        $('#seeker_id').val(id);

        $('#sendmessage').modal('show');

        @else

        swal({

            title: "You are not Loged in",

            content: el,

            icon: "error",

            button: "OK",

        });

        @endif

    }

    if ($("#send-form").length > 0) {

        $("#send-form").validate({

            validateHiddenInputs: true,

            ignore: "",



            rules: {

                option: {

                    required: true,

                },

                date: {

                    required: true,

                },

                message: {

                    required: true,

                    maxlength: 5000

                },

            },

            messages: {



                option: {

                    required: "Please select an option",

                },

                date: {

                    required: "Date is required",

                },

                message: {

                    required: "Message is required",

                }



            },

            submitHandler: function(form) {

                $.ajaxSetup({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                @if(null !== (Auth::guard('company')->user()))

                $.ajax({

                    url: "{{route('submit-action-seeker')}}",

                    type: "POST",

                    data: $('#send-form').serialize(),

                    success: function(response) {

                        $("#send-form").trigger("reset");

                        $('#sendmessage').modal('hide');

                        swal({

                            title: "Success",

                            text: response["msg"],

                            icon: "success",

                            button: "OK",

                        });

                    }

                });

                @endif

            }

        })

    }



    function view_message(id,job_id){

         $.ajax({

                url: "{{route('view-action-seeker')}}?id="+id+"&job_id="+job_id,

                type: "GET",

                success: function(response) {

                    $('#append_messages').html(response);

                    $('#showmessage').modal('show');

                }

            });

    }


  $(".datepicker").datetimepicker();

</script>

@endpush