@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Contact Us')])

<!-- Inner Page Title end -->

<div class="inner-page">

    <div class="container">

        <div class="contact-wrap">

            <div class="title"> <span>&nbsp;</span>

                <h2>{{get_widget(19)->heading}}</h2>

               {!!get_widget(19)->content!!}

            </div>      

        </div>

    </div>

</div>

@include('includes.footer')

@endsection