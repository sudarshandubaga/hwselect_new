@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Report Abuse')])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        @include('flash::message')

       

            <div class="userfrnd">

                <div class="userccount">

                    <h5>{{__('Thanks')}}!</h5>

                    <p>{{__('Thank you for contacting us, we will check this page conforms to our terms of service.')}}<br /><br />{{ $siteSetting->site_name }}</p>

                </div>

            </div>

        

    </div>

</div>

<script>
    setTimeout(() => {
        window.location = "{{ url('/jobs') }}";
    }, 3000);
</script>

@include('includes.footer')

@endsection