@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">
    <tr>
        <td>
		<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello Admin</h1>
		
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #666;text-align: left; margin: 0;">Date of Registration {{date('d-M-Y')}}</p>
		
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0  0;">Company "<strong>{{ $name }}</strong>" has registered with "{{ $siteSetting->site_name }}"</p>
		<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">Link to Company Profile:</p>	
			
		<a href="{{ $link_admin }}" style="background:#17d27c; color: #fff; display: inline-block; padding: 10px 25px; font-size: 14px; font-weight: 700; margin-top: 10px; border-radius: 5px; text-decoration: none;">View Profile</a>
			
		
		</td>
    </tr>
	
</table>
@endsection