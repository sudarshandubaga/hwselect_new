@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td class="content-wrapper" style="padding-left:24px;padding-right:24px">
			
		<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Hello {{$full_name}}</h1>
			
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;"><strong>Subject</strong>: JOB DELETION</p>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;"><strong>Job</strong>: <span style="color: #0444DE">"{{$job}}"</span> deleted by Admin on {{date('d-M-Y')}}</p>	
			
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:20px 0 0 0;">Thank you from the Team at ({{ $siteSetting->site_name }})</p>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;">www.hwselect.com</p>
	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0 0;">Office Number: {{ $siteSetting->site_phone_primary }}</p>	
		
		
		</td>
    </tr>
	
	
</table>
@endsection