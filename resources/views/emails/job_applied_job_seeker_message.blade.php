@extends('admin.layouts.email_template')
@section('content')
<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">
    <tr>
        <td>
			
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Dear {{$user_name}}</h1>			
			
          	<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:0 0 10px 0;">You have applied for the  job of : {{ $job->title }}</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #777;text-align: left; margin:0 0 5px 0;">Applied for Job Link:</p>
			
			<a style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block;" href="{{ $job_link }}">{{ $job_link }}</a>
		
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:15px 0 0 0; color: #777;">
			Thank you from the team at ({{ $siteSetting->site_name }})
			</p>
			
		</td>
    </tr>

</table>
@endsection