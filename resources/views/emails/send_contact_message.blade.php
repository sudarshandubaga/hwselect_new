@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%; border-bottom: solid 1px #ccc;">

    <tr>

        <td>
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 15px 0;">Hello Admin</h1>
			
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:0 0 0  0;"><strong>Subject</strong>: Contact Us form submitted by {{$type}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0  0;"><strong>Full Name</strong>: {{$full_name}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0  0;"><strong>Email</strong>: {{$email}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 0  0;"><strong>Phone</strong>: {{$phone}}</p>
<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:10px 0 10px 0;"><strong>Message</strong>:<br> {{$message_txt}}</p>
		
<a href="mailto:{{$email}}" style="font-family:Helvetica, Arial, sans-serif; color: #0036CA; display: inline-block;">You can respond to "{{$full_name}}" by clicking on this Link.</a>		
		
		</td>

    </tr>

	
</table>

@endsection