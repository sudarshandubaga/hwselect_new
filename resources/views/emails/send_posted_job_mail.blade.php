
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{ $siteSetting->site_name }}</title>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="font-family: Arial, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'">
<tbody>
<tr>
<td height="30">&nbsp;</td>
</tr>
<tr>
<td>
<!-- header_center -->
<table width="650" border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#043C55">
<tbody>
<tr>
<td style="padding:20px 40px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td><img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}" alt="{{ $siteSetting->site_name }}" width="273" height="76"></td>
<td align="right"><a href="https://www.hwselect.com/admin" target="_blank" style="color: #fff; font-size: 14px;">Login to Admin</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- hero_welcome -->
<table align="center" width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td><table role="presentation" class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="left" bgcolor="#043C55" style="padding:40px 40px;">
<p style="color: #ffffff; font-size: 16px; margin-top: 0; line-height: 24px; margin-bottom: 0; font-weight:normal; margin-bottom: 0;">
This message is sent in confidence for the addressee only. It may contain legally privileged information. The contents must not be disclosed to anyone other than the addressee. Unauthorised recipients are requested to preserve this confidentiality and to advise the sender immediately of any error in transmission.
<br><br>
{{ $siteSetting->company_details }}</p>
</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>
<!-- content -->
<table align="center" width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr>
<td align="left" style="padding-top:40px; padding-left: 40px;">
<h1 style="margin: 0; font-size: 18x">Details of Job Posting</h1>

<tr>
<td align="left" style="padding:20px 40px;">
<h3 style="font-size: 20px; color: #043C55; margin-bottom: 0;">{{$job->title}}</h3>
<h4 style="font-size: 16px; color: #555; margin-bottom: 10px; margin-top: 5px; margin-bottom: 5px;">{{$job->getLocation()}}</h4>
<p style="margin-top: 5px; font-size: 14px; color: #555; margin-bottom: 5px;">Contractual Hours: {{$job->getJobType('job_type')}}</p>
	
	<?php 
                    	$all_bouns = '';
                    	if(null!==($job->bonus)){
                    		$bon = json_decode($job->bonus);
                    		if(null!==($bon)){
                    			foreach ($bon as $key => $value) {
                    				$bonus = App\Bonus::findorFail($value);
                    				$all_bouns .=$bonus->bonus.', ';
                    			}
                    		}

                    	}


                    	$all_benifits = '';
                    	if(null!==($job->benifits)){
                    		$beni = json_decode($job->benifits);
                    		if(null!==($beni)){
                    			foreach ($beni as $key => $val) {
                    				$benifits = App\Benifits::findorFail($val);
                    				$all_benifits .=$benifits->benifits.', ';
                    			}
                    		}

                    	}
                     ?>
	<?php 

                                    if($job->salary_type == 'single_salary'){
                                        if(null!==($job->salary_from)){
                                            $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
                                        }else{
                                            $salary = '';
                                        }
                                        
                                    }else if($job->salary_type == 'salary_in_range'){
                                        //echo $job->salary_from;
                                        $salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
                                        $salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span> '.number_format($job->salary_to).'</strong>':null;
                                        $salary = $salary_from.$salary_to;

                                    }else{
                                        if(null!==($job->salary_from)){
                                        $salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
                                        }else{
                                            $salary = '';
                                        }
                                    } 


                                    ?>
	
	
	
<p style="margin-top: 5px; font-size: 14px; color: #555;">Salary: {!!$salary!!}</p>
<p style="font-size: 14px; color: #777; line-height: 22px; margin-bottom: 30px;">{!! $job->description !!}</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 14px; color: #666; line-height: 26px;">
<tbody>
<tr>
<td style="border-top: 1px solid #eee; padding: 5px 0; font-weight: bold;">Posted by</td>
<td style="border-top: 1px solid #eee; padding: 5px 0;">{{$company->name}}</td>
</tr>
<tr>
<td style="border-top: 1px solid #eee; padding: 5px 0; font-weight: bold;">Contact Person</td>
<td style="border-top: 1px solid #eee; padding: 5px 0;">{{$company->ceo}}</td>
</tr>
<tr>
<td style="border-top: 1px solid #eee; padding: 5px 0; font-weight: bold;">Company Email</td>
<td style="border-top: 1px solid #eee; padding: 5px 0;">{{$company->email}}</td>
</tr>
<tr>
<td style="border-top: 1px solid #eee; padding: 5px 0; font-weight: bold;">Company Phone</td>
<td style="border-top: 1px solid #eee; padding: 5px 0;">{{$company->phone}}</td>
</tr>
<tr>
<td style="border-top: 1px solid #eee; padding: 5px 0; font-weight: bold;">Job Expiry</td>
<td style="border-top: 1px solid #eee; padding: 5px 0;">{{date('d-M-Y',strtotime($job->expiry_date))}}</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding: 20px 40px;">
<a href="{{route('public.job', [$job->id])}}" style="background-color:#17d27c; color: #fff; padding: 12px 25px; display: inline-block; border-radius: 5px; text-decoration: none;">View Job Details</a>
</td>
</tr>
</tbody>
</table>
<!-- footer_alt -->
<table align="center" width="650" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="email_body email_end tc"><table role="presentation" class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td height="16">&nbsp;</td>
</tr>
<tr>
<td>
<p style="margin: 0; color: #666; font-size: 14px;">
<strong>Thanks,</strong><br>
{{ $siteSetting->site_name }} Team
</p>
</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</body>
</html>