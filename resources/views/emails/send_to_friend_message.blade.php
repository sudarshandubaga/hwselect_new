@extends('admin.layouts.email_template')

@section('content')

<table border="0" cellpadding="15" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">

    <tr>

        <td>
			
			<h1 style="font-family:Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; color:#000; text-align:left; margin:0 0 10px 0;">Dear {{ $friend_name }}</h1>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #444;text-align: left; margin:0 0 10px 0;">{{ $your_name }} {!! __('has shared a Job (<strong>' . $job_title . '</strong>) that may be of interest to you') !!}.</p>
			
 			<a style="font-family:Helvetica, Arial, sans-serif; color: #fff; background: #17d27c; display: inline-block; padding: 10px 40px; border-radius: 5px;" href="{{ $job_url }}">View Job Details</a>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px;font-weight:400;color: #000;text-align: left; margin:15px 0 0 0;">{{__('Search Hundreds of IT and Technology Jobs across London and the UK.')}}</p>
			
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:15px 0 0 0; color: #777;">
			Thank you from the team at ({{ $siteSetting->site_name }})
			</p>
			
			<p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; margin:10px 0 0 0;">Office Number: {{ $siteSetting->site_phone_primary }}</p>

		</td>
    </tr>
</table>

@endsection