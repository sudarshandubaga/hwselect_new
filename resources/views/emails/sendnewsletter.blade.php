<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{$title}}</title>
</head>

<body>
    <div style="background-color: #dfdfdf; font-family: arial; padding: 20px;">
    
        <table width="603" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff;border-radius:5px;margin:10px auto 0 auto;">
  <tbody>
    <tr>
      <td width="265" align="left" style="color:#1d1d1b;font-size:24px;line-height:24px;padding:43px 0px 32px 30px"> 
          <img src="{{url('/')}}/sitesetting_images/thumb/hw-select-1589825177-369.jpg" />
          <br><br>
          Hi {{$name}}, 
        </td>
    </tr>
    <tr>
      <td style="background-color:#45b343;color:rgb(255,255,255);font-size:22px;padding:10px 0 10px 30px" colspan="2">{{$title}}</td>
    </tr>
    <tr>
      <td style="padding:10px 30px 10px 30px;font-size:14px; line-height: 24px;" colspan="2">
        {!!$description!!}
          
        </td>
    </tr>
    @if($type=='seeker')
    <tr>
      <td colspan="2">
        <h3 style="border-top:solid 1px #efefef;padding:15px 30px 15px 30px; margin-bottom: 0;">Suggestion Jobs</h3>
        </td>
      </tr>

    @if(isset($jobs) && null!==($jobs)) 
    @foreach($jobs as $job) 
    <tr>
      <td align="left" style="padding:5px 30px 15px 30px" colspan="2"><table width="520" cellspacing="0" cellpadding="0" border="0" align="left">
          <tbody>
            <tr>
              <td align="left" style="font-size:17px;color:#007dc3" colspan="2"><a style="color:#1565c0;text-decoration:none" title="Click here to Apply Now" href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}" target="_blank">{{$job->title}}</a></td>
            </tr>
            <tr>
              <td align="left" style="color:#212121;font-size:13px" colspan="2"><strong>Exp.</strong> $job->getJobExperience('job_experience')}}</td>
            </tr>
            <tr>
              <td align="left" style="color:#212121;font-size:13px" colspan="2"><strong>Salary</strong> is greater than your current salary. </td>
            </tr>
            <tr>
              <td align="left" style="color:#1d1d1b;font-size:15px;font-weight:bold;padding-top:15px" colspan="2"> Location </td>
            </tr>
            <tr>
              <td align="left" style="color:#212121;font-size:13px;padding-bottom:6px" colspan="2"> {{$job->getLocation()}} </td>
            </tr>
            <tr>
              <td style="float:left;margin:15px 2px 25px 0px" colspan="2"><span style="padding:12px 25px;background-color:#043c55;color:rgb(255,255,255);text-align:center;border-radius:3px"> <a style="text-decoration:none;color:rgb(255,255,255);font-size:15px" href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" target="_blank">Apply</a></span> &nbsp;</td>
            </tr>
            <tr>
              <td style="margin:10px 2px 12px 0px;font-size:15px" colspan="2"><strong>Apply by </strong> {{$job->created_at->format('d-M-Y')}} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    @endforeach

    @endif

    @endif
    
    <tr>
      <td align="left" style="border-bottom:solid 1px #efefef;padding:10px 0 10px 0" colspan="2"></td>
    </tr>
    
    <tr>
      <td colspan="2" align="center" style="margin:0px;padding:10px;border-top:1px solid #efefef"><a href="#" target="_blank"><img style="margin:6px;width:30px" src="https://ci4.googleusercontent.com/proxy/TXtbyHo9jveJ2bozst-g96Se9r9WN8h-ZuNpi1zclNkJFMBf-AUby-wvv-ndy0zfi8qB=s0-d-e1-ft#https://nl.rozee.pk/c/fb.png?v=1" alt="Like us on Facebook" class="CToWUd"></a> <a href="#" target="_blank"><img style="margin:6px;width:30px" src="https://ci3.googleusercontent.com/proxy/OXeVDvVMTzzRcQ5pP3RguJ3hrrNVkv77OC_MxxaKtDaCvNR6rnD8EwOQBjQmh_K0DTnM=s0-d-e1-ft#https://nl.rozee.pk/c/tw.png?v=1" alt="Follow us on Twitter" class="CToWUd"></a> <a href="#" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.linkedin.com/company/rozee-pk&amp;source=gmail&amp;ust=1598419910063000&amp;usg=AFQjCNHmph7khuVyvUFYX-PEnY6idqfXqw"><img style="margin:6px;width:30px" src="https://ci4.googleusercontent.com/proxy/put4tJPHu0vqPIuRHIetobuyVW9ZdAuuoejm3TsP8-fa7uUwpIbs9gfLDHGvUr9-o1SW=s0-d-e1-ft#https://nl.rozee.pk/c/li.png?v=1" alt="Follow us on LinkedIn" class="CToWUd"></a> <a href="#" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.youtube.com/channel/UCkk_nr_owMOb7VdEy0f2dxQ&amp;source=gmail&amp;ust=1598419910063000&amp;usg=AFQjCNFohGnNscgyd7NQ0TBXrGOZ0ukRAQ"><img style="margin:6px;width:30px" src="https://ci5.googleusercontent.com/proxy/UQLvgbtw7JdIfFbtwpuV7CAAJjY2PvPFuyCouf_ZOKEs1QSmxjhfiY28K7e5veUuEO2I=s0-d-e1-ft#https://nl.rozee.pk/c/yt.png?v=1" width="27" height="28" alt="Subscribe us on Youtube" class="CToWUd"></a> 
        <br>
        <p style="font-size: 12px;">This email was sent to you from {{ $siteSetting->site_name }},<br>
Contact Number {{ $siteSetting->site_phone_primary }}</p>
        
        
        </td>
    </tr>
  </tbody>
</table>
    
</div>
</body>
</html>
