@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')

<style>
.searchbar .form-control::placeholder {
    color: inherit !important;
}
</style>
<!-- Header end --> 
<!-- Inner Page Title start -->
<!-- Inner Page Title end -->
<div class="about-wraper page-404" style="background-image: url('{{ url('company_logos/hw-main-banner-1616940090.jpg') }}'); background-size: cover;"> 
    <!-- About -->
    <div class="container text-center">
        <div style="margin: 30px 0;">
            <div class="text-white" style="font-size: 192px; font-weight: bold;">
                404
            </div>
            <h2 class="text-white">{{__('Oops! Page Not Found')}}</h2>
            <p class="text-white">{{__('Sorry, the page you are looking for could not be found')}}</p>

            <div style="max-width: 700px; margin: auto;">
                @include('includes.search_form')
            </div>


            <p class="mt-3">
                <a href="{{ url('/') }}" class="btn btn-primary">Back To Home Page</a>
            </p>
        </div>     
    </div>  
</div>
@include('includes.footer')
@endsection


@php
$skills_arr = \Cache::rememberForever('skills_arr', function() {
    return \App\JobSkill::select('job_skills.job_skill')->isDefault()->active()->sorted()->pluck('job_skills.job_skill')->toArray();
});

$latestJobs = \App\Job::select('jobs.title')->active()->notExpire()->orderBy('title', 'desc')->pluck('jobs.title')->toArray();

$skills = array_merge($skills_arr,$latestJobs);


$skills = json_encode($skills);
@endphp


@push('scripts') 
<script>
  $( function() {

    var mySource  = {!!$skills!!};
    $( "#jbsearch" ).autocomplete({
    minLength: 1,
    source: function(req, resp) {
      var q = req.term;
      var myResponse = [];
      $.each(mySource, function(key, item) {
        if (item.toLowerCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.toUpperCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.indexOf(q) === 0){
          myResponse.push(item);
        }
      });
      resp(myResponse);
    },
    
  });
    });
  </script>	

@endpush