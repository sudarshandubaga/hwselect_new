<div class="section greybg">

	<div class="container"> 

		

		

		<div class="row">

			<div class="col-lg-8 col-md-7">			
				<div class="seekerbghome">
					<div class="wlogo"><img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}" alt="{{ $siteSetting->site_name }}" /></div>
					
					<h4><span>We just made recruitment easier for</span> Companies & Jobseekers </h4>
					
					<div class="row">
						<div class="col-md-4">
							@if(!Auth::guard('company')->user())
							
							<a href="{{route('login')}}?type=emp">
								
								<img src="{{asset('/')}}images/register-cv-btn.png" alt=""></a>
								
								
								@else
								<a href="{{ route('post.job') }}"><img src="{{asset('/')}}images/register-cv-btn.png" alt=""></a>
								@endif
							</div>
							<div class="col-md-4"></div>
							<div class="col-md-4">
								@if(!Auth::user() && !Auth::guard('company')->user())
								<a href="javascript:void(0)" data-toggle="modal" data-target="#regcvpop">
									<img src="{{asset('/')}}images/postjob-btn.png" alt=""></a>
									
									<div class="modal fade" id="regcvpop" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered">
											<div class="modal-content">      
												<div class="modal-body">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<i class="fas fa-times"></i>
													</button>

													<div class="invitereval">
														<h3>{{__('Register Your CV')}}</h3>

														<p> {{__('You need to be Registered and signed into your account  to upload a CV.')}}</p>
														<div class="btn2s">
															<a href="{{route('login')}}">{{__('Login')}}</a>
															<a href="{{route('register')}}">{{__('Register')}}</a>
														</div>
													</div>

												</div>

											</div>
										</div>
									</div>
									@else
									<a href="{{ route('my.profile') }}">
										<img src="{{asset('/')}}images/postjob-btn.png" alt=""></a>
										@endif
									</div>
								</div>
								
							</div>
							
							
							

						</div>

						

						<div class="col-lg-4 col-md-5">

							

							<div class="featuredjobsarea">

								<div class="titlebox">

									<h3>{{__('Featured IT Jobs')}}</h3>

									<div class="viewlink"><a href="{{route('job.featuredJobs')}}">{{__('View All')}}</a></div>

									<div class="clearfix"></div>

								</div>



								<!--Featured Job start-->

								<ul class="featjobs">

									@if(isset($featuredJobs) && count($featuredJobs))

									@foreach($featuredJobs as $featuredJob)

									<?php 

									if($featuredJob->salary_type == 'single_salary'){

										$featuredJobsalary = '<strong><span class="symbol">'.$featuredJob->salary_currency.'</span>'.number_format($featuredJob->salary_from).'</strong>';
									}else if($featuredJob->salary_type == 'salary_in_range'){

										$featuredJobsalary = '<strong><span class="symbol">'.$featuredJob->salary_currency.'</span>'.number_format($featuredJob->salary_from).' - <span class="symbol">'.$featuredJob->salary_currency.'</span>'.number_format($featuredJob->salary_to).'</strong>';

									}else{
										$featuredJobsalary = '<strong><span class="symbol">'.$featuredJob->salary_currency.'</span>'.$featuredJob->salary_from.'</strong>';
									} 


									?>

									<?php $company = $featuredJob->getCompany(); ?>

									@if(null !== $company)

									<!--Job start-->
									<li>

										<div class="jobint">

											<h4><a href="{{route('job.detail', [\Str::slug($featuredJob->getFunctionalArea('functional_area')),$featuredJob->slug])}}" title="{{$featuredJob->title}}">{{$featuredJob->title}}</a></h4>
											
											<div class="salary">{!!$featuredJobsalary!!} @if($featuredJob->salary_type != 'negotiable') {{$featuredJob->getSalaryPeriod('salary_period')}} @endif @if($featuredJob->is_bonus) + <span>{{$featuredJob->is_bonus}}</span>@endif 
												@if($featuredJob->is_benifits) + <span>{{$featuredJob->is_benifits}}</span>@endif 


											</div>

											<div class="jobloc"><i class="fas fa-map-marker-alt"></i> {{$featuredJob->getCity('city')}}</div>                              

										</div>

									</li>

									<!--Job end--> 

									@endif

									@endforeach

									@endif



								</ul>

								<!--Featured Job end--> 

							</div>

						</div>

						

						

						



					</div>

				</div>

			</div>