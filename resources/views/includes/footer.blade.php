<?php if(Request::segment(1)==null && $siteSetting->index_page_below_cities_ads==1){?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(1)=='hire-it-talent' && $siteSetting->hire_it_talent_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='why-us' && $siteSetting->why_us_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='jobs' && $siteSetting->jobs_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='contact-us' && $siteSetting->contact_us_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='blog' && $siteSetting->blog_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(2)!=null && $siteSetting->blog_details_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='job' && $siteSetting->jobs_detail_ads==1){ ?>


<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>

<?php }else if(Request::segment(1)=='login' && $siteSetting->login_ads==1){ ?>

<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>
<?php }else if(Request::segment(1)=='register' && $siteSetting->register_ads==1){ ?>
<div class="largebanner shadow3">
  <div class="adin">
    {!! $siteSetting->above_footer_ad !!}
  </div>
  <div class="clearfix"></div>
</div>
<?php } ?>


<div class="footerWrap">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}"
          alt="{{ $siteSetting->site_name }}" />

        <!-- Social Icons -->
        <div class="social">@include('includes.footer_social')</div>
        <!-- Social Icons end -->
      </div>


      <!--Quick Links-->
      <div class="col-lg-3 col-md-6">
        <h5>{{__('HW Search')}}</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">

          {!!get_menus_footer()!!}

        </ul>
      </div>
      <!--Quick Links menu end-->

      <div class="col-lg-3 col-md-6">
        <h5>{{__('Jobseeker')}}</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          {!!get_menus_seeker()!!}

          @if(!Auth::user() && !Auth::guard('company')->user())
          <li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadresume">Upload CV</a></li>
          @else
          <li><a href="{{url('my-profile#cvs')}}">Upload CV</a></li>
          @endif

        </ul>
      </div>

      <!--Jobs By Industry-->
      <div class="col-lg-3 col-md-6">
        <h5>{{__('Employer')}}</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">

          {!!get_menus_employerr()!!}

        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>




    </div>
  </div>
</div>
<!--Footer end-->
<!--Copyright-->
<div class="copyright">
  <div class="container">
    <div class="bttxt">{{__('Copyright')}} &copy; {{date('Y')}} {{ $siteSetting->site_name }}.
      {{__('All Rights Reserved')}}. {{__('Created by')}}: <a href="https://www.seoguru.co.uk" target="_blank"
        style="color: #fff;">SEOGURU</a></div>

  </div>
</div>

{!! $siteSetting->google_tag_manager_for_body !!}



<div class="modal fade" id="uploadresume" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fas fa-times"></i>
        </button>

        <div class="invitereval">
          <h3>{{__('Upload Resume')}}</h3>

          <p> {{__('You need to be Registered and Signed into your account to Upload Resume.')}}</p>
          <div class="btn2s">
            <a href="{{route('login')}}">{{__('Login')}}</a>
            <a href="{{route('register')}}">{{__('Register')}}</a>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="uploadjobdetail" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fas fa-times"></i>
        </button>

        <div class="invitereval">
          <h3>{{__('Upload Job Details')}}</h3>

          <p> {{__('Please upload your job specifications and our team will post on your behalf.')}}</p>
          <form method="post" action="{{route('apply-to-admin')}}" enctype='multipart/form-data'>
            @csrf
            <div class="formpanel">
              <div class="formrow">
                <label for="">Choose FIle</label>
                <input type="file" class="form-control" name="job_detail" id="job_detail" required="required"
                  oninvalid="this.setCustomValidity('Please select pdf, doc, docx files')"
                  oninput="this.setCustomValidity('')">
                <p>Please upload only PDF, Word Doc or Docx file</p>
              </div>
              <div class="formrow">
                <input type="submit" class="btn" name="" id="" value="Submit">
              </div>
            </div>
          </form>


        </div>

      </div>

    </div>
  </div>
</div>






<script>
  Array.prototype.forEach.call(document.querySelectorAll('.clearable-input'), function (el) {
    var input = el.querySelector('input');

    conditionallyHideClearIcon();
    input.addEventListener('input', conditionallyHideClearIcon);
    el.querySelector('[data-clear-input]').addEventListener('click', function (e) {
      input.value = '';
      conditionallyHideClearIcon();
    });

    function conditionallyHideClearIcon(e) {
      var target = (e && e.target) || input;
      target.nextElementSibling.style.display = target.value ? 'block' : 'none';
    }
  });
</script>