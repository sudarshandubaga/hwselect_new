<div class="section howitwrap">

<div class="container">

        <ul class="howlist row">

            <!--step 1-->

            <li class="col-md-4 col-sm-4">
                <div class="iconcircle"><img src="{{asset('/')}}images/alert-icon.png" alt="">
                </div>
                <h4>{{get_widget(2)->heading}}</h4>
                <p>{!!get_widget(2)->content!!}</p>
				
				@if(!Auth::user() && !Auth::guard('company')->user())
				<a href="javascript:void(0)" data-toggle="modal" data-target="#emailafriend" class="btn">Set up alert</a>
				
				<div class="modal fade" id="emailafriend" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">      
					  <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <i class="fas fa-times"></i>
						</button>

						  <div class="invitereval">
							<h3>{{__('Set Up Job Alerts')}}</h3>

							  <p> {{__('You need to be Registered and Signed into your account to set up Job Alerts.')}}</p>
							  <div class="btn2s">
							  <a href="{{route('login')}}">{{__('Login')}}</a>
							  <a href="{{route('register')}}">{{__('Register')}}</a>
							</div>
						  </div>

					  </div>

					</div>
				  </div>
				</div>
				
				
				@else
				<a href="{{ route('my-alerts') }}" class="btn">Set up alert</a>
				@endif
				
				
            </li>

            <!--step 1 end-->

            <!--step 2-->

            <li class="col-md-4 col-sm-4">
                <div class="iconcircle"><img src="{{asset('/')}}images/user-register-icon.png" alt="">
                </div>
                <h4>{{get_widget(3)->heading}}</h4>
                <p>{!!get_widget(3)->content!!}</p>
				
				@if(!Auth::user() && !Auth::guard('company')->user())
				<a href="{{route('register')}}" class="btn">Register and Upload your CV</a>
				@else
				<a href="{{ route('my.profile') }}" class="btn">Edit Profile</a>
				@endif
				
				
            </li>

            <!--step 2 end-->

            <!--step 3-->

            <li class="col-md-4 col-sm-4">

                <div class="iconcircle"><img src="{{asset('/')}}images/great-advice-icon.png" alt="">
                </div>
                <h4>{{get_widget(4)->heading}}</h4>
                <p>{!!get_widget(4)->content!!}</p>
				
				@if(!Auth::user() && !Auth::guard('company')->user())
				<a href="javascript:void(0)" data-toggle="modal" data-target="#uploadresume" class="btn">Upload Resume</a>
				
				
				
				@else
				<a href="{{url('my-profile#cvs')}}" class="btn">Upload Resume</a>
				@endif
				
				
				
				
            </li>

            <!--step 3 end-->

        </ul>

    </div>

</div>