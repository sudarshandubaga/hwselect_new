<script type="text/javascript">

    $('#is_immediate_available').on('change',function(){
        if($('input[name="is_immediate_available"]:checked').val() == 'on'){
            $('#immediate_form').hide();
        }else{
            $('#immediate_form').show();
        }
        
    })

    function changeImmediateAvailableStatus(user_id, old_status) {

        

        $.post("{{ route('update.immediate.available.status') }}", {user_id: user_id, old_status: old_status, _method: 'POST', _token: '{{ csrf_token() }}'})

                .done(function (response) {

                    if (responce == 'ok') {

                        if (old_status == 0)

                            $('#is_immediate_available').attr('checked', 'checked');

                        else

                            $('#is_immediate_available').removeAttr('checked');

                    }

                });



    }

</script>