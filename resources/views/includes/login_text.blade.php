
<div class="userloginbox">
	<div class="container">		
		<div class="titleTop">

           <h3>{{get_widget(1)->heading}} </h3>

        </div>

		<p>{!!get_widget(1)->content!!}</p>
		
		@if(!Auth::user() && !Auth::guard('company')->user())
		<div class="viewallbtn"><a href="{{route('register')}}"> {{__('Get Started Today')}} </a></div>
		@else
		<div class="viewallbtn"><a href="{{url('my-profile')}}">{{__('Build Your CV')}} </a></div>
		@endif
	</div>
</div>
