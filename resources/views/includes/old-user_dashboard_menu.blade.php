<div class="col-lg-3">

	<div class="usernavwrap">

    <div class="switchbox">

        <div class="txtlbl">{{__('Available for Work Immediate')}} <i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{__('Are you immediate available')}}?" data-original-title="{{__('Are you immediate available')}}?" title="{{__('Are you immediate available')}}?"></i>

        </div>

        <div class="">

            <label class="switch switch-green"> @php

                $checked = ((bool)Auth::user()->is_immediate_available)? 'checked="checked"':'';

                @endphp

                <input type="checkbox" name="is_immediate_available" id="is_immediate_available" class="switch-input" {{$checked}} onchange="changeImmediateAvailableStatus({{Auth::user()->id}}, {{Auth::user()->is_immediate_available}});">

                <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span> </label>

        </div>

        <div class="clearfix"></div>
        <form id="immediate_form" action="{{ route('update.immediate.available.status') }}" method="post" <?php if((bool)Auth::user()->is_immediate_available){echo 'style="display:none"';}else{} ?>>
        	@csrf
        	<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        	<input type="hidden" name="old_status" value="1">
              <div class="form-group" style="text-align: left">
                <label for="date_from" style="margin-bottom: 5px;">Date From</label>
                <input type="date" class="form-control" id="date_from" name="date_from" aria-describedby="emailHelp" value="{{null!==(Auth::user()->immediate_date_from)?date('Y-m-d',strtotime(Auth::user()->immediate_date_from)):date('Y-m-d')}}" placeholder="Date From">
              </div>
              <div class="form-group" style="text-align: left">
                <label for="date_to" style="margin-bottom: 5px;">Date To</label>
                <input type="date" class="form-control"  id="date_to" name="date_to" value="{{null!==(Auth::user()->immediate_date_to)?date('Y-m-d',strtotime(Auth::user()->immediate_date_to)):date('Y-m-d')}}" placeholder="Date To">
              </div>
              <button type="submit" class="btn btn-success">Save</button>
        </form>

    </div>
    

    <ul class="usernavdash">

        <li class="{{ Request::url() == route('home') ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fas fa-tachometer-alt"></i> {{__('Dashboard')}}</a>
        </li>

        <li class="{{ Request::url() == route('my.profile') ? 'active' : '' }}"><a href="{{ route('my.profile') }}"><i class="fas fa-user-edit"></i> {{__('Edit Profile')}}</a>
        </li>
		
		<li class="{{ Request::url() == url('my-profile#cvs') ? 'active' : '' }}"><a href="{{url('my-profile#cvs')}}"><i class="fas fa-file-alt"></i> {{__('Manage Resume')}}</a>
        </li>
		
        <li class="{{ Request::url() == route('view.public.profile', Auth::user()->id) ? 'active' : '' }}"><a href="{{ route('view.public.profile', Auth::user()->id) }}"><i class="fas fa-eye"></i> {{__('View  Profile')}}</a>
        </li>

        <li class="{{ Request::url() == route('my.job.applications') ? 'active' : '' }}"><a href="{{ route('my.job.applications') }}"><i class="fas fa-desktop"></i> {{__('My Job Applications')}}</a>
        </li>

        <li class="{{ Request::url() == route('my.favourite.jobs') ? 'active' : '' }}"><a href="{{ route('my.favourite.jobs') }}"><i class="fas fa-heart"></i> {{__('My Saved Jobs')}}</a>
        </li>

        <li class="{{ Request::url() == route('my-alerts') ? 'active' : '' }}"><a href="{{ route('my-alerts') }}"><i class="fas fa-bullhorn"></i> {{__('My Job Alerts')}}</a>
        </li>
        
		
		<li><a href="javascript:;" onclick="view_message_request()"><i class="fas fa-times"></i> {{__('Request to Close Account')}}</a></li>
		

        <?php /*?><li><a href="{{route('my.messages')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('My Messages')}}</a>

        </li>

        <li><a href="{{route('my.followings')}}"><i class="fa fa-user-o" aria-hidden="true"></i> {{__('My Followings')}}</a>

        </li><?php */?>

        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> {{__('Logout')}}</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                {{ csrf_field() }}

            </form>

        </li>

    </ul>
	
		
	<hr>
    <div class="switchbox log2part">
        <div class="txtlbl"><i class="fas fa-lock"></i> {{__('Security Settings')}}
        </div>
		
		<p>Turn ON or OFF 2 Step Verification for login to make your account more secure.</p>
		
        <div class="">
            <label class="switch switch-green"> @php
                $checked = ((bool)Auth::user()->two_step_verification)? 'checked="checked"':'';
                @endphp
                <input type="checkbox" name="two_step_verification" id="two_step_verification" class="switch-input" {{$checked}} onchange="changeTwoStepVerificationStatus({{Auth::user()->id}}, {{Auth::user()->two_step_verification}});">
                <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
        </div>
        <div class="clearfix"></div>
    </div>
		
		</div>

    

		

</div>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">Enter Your Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('update.user.twoStep.verification.status') }}">
        @csrf
        <input type="hidden" name="user_id" id="user_id">
        <input type="hidden" name="old_status" id="old_status">
        <div class="modal-body">
          <div class="form-group">
            <input type="password" required class="form-control" id="password1" name="password" placeholder="Password">
            
          </div>
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')
<script type="text/javascript">
    function changeTwoStepVerificationStatus(user_id, old_status) {
        $('#old_status').val(old_status);
        $('#user_id').val(user_id);
        $('#form').modal('show');

    }

    @if(session()->has('message.added'))
       swal({
            title: "Success",
            text: '{!! session('message.added') !!}',
            icon: "success",
            button: "OK",
        });

      

    @endif

    @if(session()->has('message.error'))
       swal({
            title: "Error",
            text: '{!! session('message.error') !!}',
            icon: "error",
            button: "OK",
        });

      

    @endif

    $('#form').on('hidden.bs.modal', function () {
        location.reload();
    });
</script>
@endpush