@push('styles')
<style type="text/css">
	.searchwrap {
	    background: url({{asset('/')}}/company_logos/{{get_widget(10)->image}}) no-repeat;
	    background-size: cover;
	}
</style>
@endpush
@if(Auth::guard('company')->check())


<div class="uploadjobinfo">
	<p>Don't have time to post a job? Don't worry just upload your specifications and we will take care of it.</p>
	<a href="javascript:void(0)" data-toggle="modal" data-target="#uploadjobdetail">Upload Job Details</a>
</div>


@else

<form action="{{route('job.list')}}" method="get">

    <div class="searchbar">

		<div class="srchbox">		

		<div class="row">

			<div class="col-lg-12 col-md-12 mb-3">

				<label for=""> {{__('Enter: Keywords / Job Title')}}</label>
				<div class="clearable-input">
				<input type="text"  name="search" id="jbsearch" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('e.g. Project Manager')}}" autocomplete="off" />
				<span data-clear-input><i class="fas fa-times"></i></span>
				</div>
			</div>

			

			<div class="col-lg-10 col-md-4">
				<div class="location-input">
				<label for=""> {{__('Where')}}</label>
				<input type="text"  name="address" id="current_location" value="{{Request::get('address', '')}}" class="form-control"  placeholder="{{__('Town, City or Postcode')}}" autocomplete="off" />
			</div>
			<div class="radius">
				<label for=""> {{__('Radius')}}</label>
				<select name="radius" class="form-control search-radius search-locationsearchtype" id="Radius">
					<option value="0">0 miles</option>
					<option value="5">5 miles</option>
					<option value="10">10 miles</option>
					<option selected="selected" value="20">20 miles</option>
					<option value="30">30 miles</option>
				</select>
			</div>
			<div class="clearfix"></div>
			</div>

			
			<input type="hidden" name="longitude" id="longitude" value="{{Request::get('longitude', '')}}">
			<input type="hidden" name="latitude" id="latitude" value="{{Request::get('latitude', '')}}">
			

			<div class="col-lg-2 col-md-3">

				<label for="">&nbsp;</label>

				<button type="submit" class="btn"><i class="fas fa-search"></i></button></div>
				
		</div>

		</div>	

		<div class="jobcount">{{get_widget(10)->content}}</div>

    </div>

</form>

@endif