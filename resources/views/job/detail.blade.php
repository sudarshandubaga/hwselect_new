@extends('layouts.app')

@if($job->is_active == 0 )
@section('seo_other_meta')
<meta name="robots" content="noindex, nofollow" />
@endsection
@endif

@section('content') 

<!-- Header start --> 

@include('includes.header') 

<!-- Header end --> 

<!-- Inner Page Title start --> 

@include('includes.inner_page_title', ['page_title'=>__('Job Details')]) 

<!-- Inner Page Title end -->

@include('flash::message')

@include('includes.inner_top_search')

@php
$company = $job->getCompany();

@endphp

<?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
	if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?> 



<div class="listpgWraper" @if(!$job->is_active) rel="noindex" @endif>

    <div class="container"> 

        @include('flash::message')

       



        <!-- Job Detail start -->

        <div class="row">

            <div class="col-lg-7"> 

				

				 <!-- Job Header start -->

        <div class="job-header">

            <div class="jobinfo">
				<h2>{{$job->title}} (Job ID: {{ $job->job_id }})</h2>
				<div class="ptext">{{__('Date Posted')}}: {{$job->created_at->format('d-M-Y')}}</div>

        <?php 
              $all_bouns = '';
              if(null!==($job->bonus)){
                $bon = json_decode($job->bonus);
                if(null!==($bon)){
                  foreach ($bon as $key => $value) {
                    $bonus = App\Bonus::where('id',$value)->first();
                            if(null!==($bonus)){
                                $all_bouns .=$bonus->bonus.', '; 
                            }
                  }
                }

              }


              $all_benifits = '';
              if(null!==($job->benifits)){
                $beni = json_decode($job->benifits);
                if(null!==($beni)){
                  foreach ($beni as $key => $val) {
                    $benifits = App\Benifits::where('id',$val)->first();
                            if(null!==($benifits)){
                                $all_benifits .=$benifits->benifits.', ';
                            }
                  }
                }

              }
              ?>

        @if($job->salary_type === 'negotiable')
          <div class="salary">
            <div>
              Salary: {{$job->getSalaryPeriod('salary_period')}} ({{ $job->salary_currency }}) {{ $job->salary_from }}
            </div>
            <div>
              @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ')
                <span>{{trim($all_benifits,', ')}}</span>
              @endif
            </div>
          </div>
        @else
				<div class="salary">
            {!!$salary!!}
					- {{$job->getSalaryPeriod('salary_period')}} @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
          @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif </div>
          @endif
        </div>


			<!-- Job Detail start -->

                <div class="jobmainreq">

                    <div class="jobdetail">

                       <h3><i class="fas fa-align-left" aria-hidden="true"></i> {{__('Job Details')}}</h3>

						

							

							 <ul class="jbdetail">

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Location')}}:</div>

                                <div class="col-md-8 col-7">

                                    @if((bool)$job->is_freelance)

                                    <span>Freelance</span>

                                    @else

                                    <span>{{ltrim($job->getLocation(),',')}}</span>

                                    @endif

                                </div>

                            </li>                           

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Contractual Hours')}}:</div>

                                <div class="col-md-8 col-7"><span class="permanent">{{$job->getJobType('job_type')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Working Hours')}}:</div>

                                <div class="col-md-8 col-7"><span class="freelance">{{$job->getJobShift('job_shift')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Career Level')}}:</div>

                                <div class="col-md-8 col-7"><span>{{$job->getCareerLevel('career_level')}}</span></div>

                            </li>

								

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Length of Experience')}}:</div>

                                <div class="col-md-8 col-7"><span>{{$job->getJobExperience('job_experience')}}</span></div>

                            </li>

                            

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Qualification Requirements')}}:</div>

                                <div class="col-md-8 col-7"><span>{{$job->getDegreeLevel('degree_level')}}</span></div>

                            </li>
								 
								 <li class="row">

                                <div class="col-md-4 col-5">{{__('Positions Available')}}:</div>

                                <div class="col-md-8 col-7"><span>{{$job->num_of_positions}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-4 col-5">{{__('Apply Before')}}:</div>

                                <div class="col-md-8 col-7"><span>{{$job->expiry_date->format('d-M-Y')}} {{ucfirst($job->is_extended)}}</span></div>

                            </li> 

                            

                        </ul>

							

							

                       

                    </div>

                </div>

			

			<hr>
      
      <div class="jobButtons">
				@if(!Auth::user() && !Auth::guard('company')->user() && $job->status != 'rejected' && $job->status != 'blocked' && $job->request_to_delete != 'yes')
            <a href="javascript:void(0)" data-toggle="modal" data-target="#emailafriend" class="btn"><i class="fas fa-envelope" aria-hidden="true"></i> {{__('Email a Friend')}}</a>
				
				
				<div class="modal fade" id="emailafriend" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">      
					  <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <i class="fas fa-times"></i>
						</button>

						  <div class="invitereval">
							<h3>{{__('Email a Friend')}}</h3>

							  <p> {{__('You need to be registered and logged in to use the send to a friend feature.')}}</p>
							  <div class="btn2s">
							  <a href="{{route('login')}}">{{__('Login')}}</a>
							  <a href="{{route('register')}}">{{__('Register')}}</a>
							</div>
						  </div>

					  </div>

					</div>
				  </div>
				</div>
				
				
				@else
				<a href="{{route('email.to.friend', $job->slug)}}" class="btn"><i class="fas fa-envelope" aria-hidden="true"></i> {{__('Email a Friend')}}</a>
				@endif
				
				@if($job->expiry_date <  \Carbon\Carbon::now() || $job->is_active != 1)
				          <a href="javascript:void(0)" data-toggle="modal" data-target="#savejob" class="btn"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>			
				
				<div class="modal fade" id="savejob" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">      
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <i class="fas fa-times"></i>
						</button>
		  <div class="invitereval">
		  	<h3>{{__('No longer Available')}}</h3>
			 
			  <p> {{__('Job Vacancy No longer Available')}}</p>
			  
		  </div>
		  
      </div>
    
    </div>
  </div>
</div>
				@else
				<a href="javascript:void(0)" id="job_{{$job->id}}" data-id="{{$job->id}}" class="btn save_job"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>
				@endif
				
				
				
				
				
				@if(!Auth::user() && !Auth::guard('company')->user())	
                <a href="javascript:void(0)" class="btn report" data-toggle="modal" data-target="#reportabuse"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{__('Report Abuse')}}</a>				
				
				
				<div class="modal fade" id="reportabuse" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">      
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <i class="fas fa-times"></i>
		</button>
		  <div class="invitereval">
		  	<h3>{{__('Report Abuse')}}</h3>
			 
			  <p> {{__('You must be registered and logged into your account to report this Job profile')}}</p>
			  <div class="btn2s">
			  <a href="{{route('login')}}">{{__('Login')}}</a>
			  <a href="{{route('register')}}">{{__('Register')}}</a>
			</div>
		  </div>
		  
      </div>
    
    </div>
  </div>
</div>				
				
				
				@else
				<a href="{{route('report.abuse', $job->slug)}}" class="btn report"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{__('Report Abuse')}}</a>
				@endif
            </div>

        </div>

				

				

				

                <!-- Job Description start -->

                <div class="job-header">

                    <div class="contentbox">

                        <h3><i class="fas fa-file-alt" aria-hidden="true"></i> {{__('Job Description')}}</h3>

                        <p>{!! $job->description !!}</p> 

						

						<hr>

						<h3><i class="fas fa-puzzle-piece"></i> {{__('Skills Required')}}</h3>

                        <ul class="skillslist">

                            {!!$job->getJobSkillsList()!!}

                        </ul>

						

                    </div>

                </div>

				

                <!-- Job Description end --> 



                

            </div>

            <!-- related jobs end -->



            <div class="col-lg-5"> 
                <form id="new_alert" style="display: none;">
				@csrf
				<div id="job-detail-alert" class="container-fluid mb-4">
                	<div class="row wrapper">
                    	<div class="col-12 col-sm-12 col-md-12 text-center mb-2">
                        	<i class="fal fa-bells"></i>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="mb-2 text-center">Get similar jobs like this sent straight to your inbox!</div>
                            <div class="row">
                            	<div class="col-12 col-sm-12 col-md-12">
                                <input type="text" class="form-control" name="email" @if(Auth::check()) value="{{Auth::user()->email}}" @endif placeholder="Enter Your Email Address" aria-label="Enter Your Email Address">
                                <input type="hidden" name="search" value="{{$job->title}}">
                                </div>
                            	<div class="col-8 col-sm-12 col-md-12 mx-auto">
                                <button class="btn btn-alert" type="submit" id="">Create alert</button>
                            	</div>
                            </div>
                        </div>
						<div class="job-detail-alert-terms">By creating a job alert or receiving recommended jobs, you agree to our Terms. You can change your consent settings at any time by unsubscribing or as detailed in our terms.</div>
                    </div>
				</div>
				</form>
        @if((!Auth::guard('company')->check() || $job->company_id != Auth::guard('company')->user()->id) &&  $job->status != 'rejected' && $job->status != 'blocked' && $job->request_to_delete != 'yes')
				<div class="jobButtons applybox">

				@if(!Auth::user() && !Auth::guard('company')->user())
				@if($job->expiry_date <  \Carbon\Carbon::now() || $job->is_active != 1 || $job->admin_reviewed !=1 || $job->is_rejected ==1)
				@if($job->status =='rejected' )
					<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('On-hold')}}</span>
				@else

				@if($job->status =='admin_review' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Awaiting Approval')}}</span>
				@endif
				@if($job->status =='request_to_delete' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Awaiting admin review for deletion')}}</span>
				@endif
				@endif
				
				@else
				@if($job->getCompany())
				<a href="javascript:void(0)" onclick="set_session()" data-toggle="modal" data-target="#applyonjob" class="btn apply"><i class="fas fa-envelope" aria-hidden="true"></i> {{__('Apply Now')}}</a>
				@else
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Company No longer Available')}}</span>
				@endif
				@endif
				
				
				
				<div class="modal fade" id="applyonjob" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">      
					  <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <i class="fas fa-times"></i>
						</button>

						  <div class="invitereval">
							<h3>{{__('Apply for this Job')}}</h3>
							  <p>You are applying for the role of:</p>
							  <div class="jobinfo">
								  
				<h2>{{$job->title}}</h2>
				<div class="ptext"><strong>{{__('Date Posted')}}:</strong> {{$job->created_at->format('d-M-Y')}}</div>
				<div class="ptext">
					{!!$salary!!}  @if($job->salary_type != 'negotiable') {{$job->getSalaryPeriod('salary_period')}} @endif @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif </div>
								  
				<div class="ptext"><strong>{{__('Location')}}:</strong> 
							  	@if((bool)$job->is_freelance)
								<span>Freelance</span>
								@else
								<span>{{$job->getLocation()}}</span>
								@endif</div>		  
								  
            </div>
							  
							  
							  
							  
							  
							  
							  <div class="logalert"> {{__('You need to be registered and logged in to apply for this job.')}}</div>
							  <div class="btn2s">
							  <a href="{{route('login')}}">{{__('Login')}}</a>
							  <a href="{{route('register')}}">{{__('Register')}}</a>
							</div>
						  </div>

					  </div>

					</div>
				  </div>
				</div>

				
					
					
				@else
				@if($job->isJobExpired())
                <span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Vacancy No longer Available')}}</span>
                @elseif(Auth::check() && Auth::user()->isAppliedOnJob($job->id))
                <a href="javascript:;" class="btn apply applied"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Already Applied')}}</a>
                @else
                @if(null !==(Auth::user()) && count(Auth::user()->profileCvs))
                @if($job->expiry_date <  \Carbon\Carbon::now() || $job->is_active != 1 || $job->admin_reviewed !=1)
				@if($job->status =='admin_review' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Awaiting Approval')}}</span>
				@endif
				@if($job->status =='request_to_delete' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Awaiting admin review for deletion')}}</span>
				@endif

				@if($job->status =='blocked' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Vacancy No longer Available')}}</span>
				@endif
				@else
                <a href="{{route('apply.job', $job->slug)}}" class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Apply Now')}}</a>
                @endif
                @else
                @if($job->expiry_date <  \Carbon\Carbon::now() || $job->is_active != 1 || $job->admin_reviewed !=1 || $job->request_to_delete=='yes')
				@if($job->status =='admin_review' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Awaiting Approval')}}</span>
				@endif
				@if($job->status =='request_to_delete' )
				<!-- <span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Awaiting admin review for deletion')}}</span> -->
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job No longer Available')}}</span>
				@endif
				@if($job->status =='blocked' )
				<span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job Vacancy No longer Available')}}</span>
				@endif
				@else
                <a href="javascript:void(0)" data-toggle="modal" data-target="#cvnotfound" class="btn apply"><i class="fas fa-envelope" aria-hidden="true"></i> {{__('Apply Now')}}</a>
                <div class="modal fade" id="cvnotfound" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">      
					  <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <i class="fas fa-times"></i>
						</button>

						  <div class="invitereval">
							<h3>{{__('Apply for this Job')}}</h3>
							  <p>You are applying for the role of:</p>
							  <div class="jobinfo">
								  
				<h2>{{$job->title}}</h2>
				<div class="ptext"><strong>{{__('Date Posted')}}:</strong> {{$job->created_at->format('d-M-Y')}}</div>
				<div class="ptext">
					{!!$salary!!} @if($job->salary_type != 'negotiable') {{$job->getSalaryPeriod('salary_period')}} @endif @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif</div>
								  
				<div class="ptext"><strong>{{__('Location')}}:</strong> 
							  	@if((bool)$job->is_freelance)
								<span>Freelance</span>
								@else
								<span>{{$job->getLocation()}}</span>
								@endif</div>		  
								  
            </div>
							  
							  
							  
							  
							  
							  
							  <div class="logalert"> {{__('You must have a CV uploaded to apply for this Job Vacancy.')}}</div>
							  <div class="btn2s">
							  <a  href="javascript:;" onclick="set_sessions()">{{__('Upload CV')}}</a>
							</div>
						  </div>

					  </div>

					</div>
				  </div>
				</div>
                @endif
                @endif
                @endif
				@endif
					
				
					
					
					
					
				</div>

        @elseif(Auth::guard('company')->check() && $job->company_id == Auth::guard('company')->user()->id)

          <div class="jobButtons applybox">
            <a href="javascript:void(0)" class="btn apply disabled" data-toggle="tooltip" title="You cannot apply for Jobs registered to you" style="pointer-events: auto;" >
              Apply Now
            </a>
          </div>  

				@endif

        @if($job->status == 'rejected')

        <div class="jobButtons applybox">
          <strong href="javascript:void(0)" class="btn btn-danger">
            On-hold by admin on {{date('d-M-Y',strtotime($job->updated_at))}}
          </strong>
				</div>
        
        <div class="jobButtons applybox">
          <div class="text-left">
            <strong class="font-weight-bold">Reason: </strong> <br> {{ $job->rejected_reason }}
          </div>

        </div>


        @endif

        @if($job->status == 'blocked')

        <div class="jobButtons applybox">
          <span href="javascript:void(0)" class="btn btn-danger">
            {{__('Job No Longer Available')}}
          </span>
				</div>

        @endif

        @if($job->request_to_delete == 'yes')

        <div class="jobButtons applybox">
          <span href="javascript:void(0)" class="text-danger font-weight-bold">
            {{__('Job Awaiting Deletion by Admin')}}
          </span>
				</div>

        @endif

				<!-- Google Map start -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i>
                        	@if(isset($job))
	                        	@if($job->is_active == 0)
	                        		{{__('Job Location Shown On Map')}}
	                        	@else
	                        		{{__('Job Location Shown On Map')}}
	                        	@endif  
	                    	@endif
                        </h3>

                        <div class="gmap">
                        	<iframe src="https://maps.google.it/maps?q={{urlencode(strip_tags($job->getLocation()))}}&output=embed" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen=""></iframe>

                        </div>

                    </div>

                </div>

				

				

				<!-- related jobs start -->

                <div class="relatedJobs">

                    <h3>{{__('Related Jobs')}}</h3>

                    <ul class="searchList">

                        @if(isset($relatedJobs) && count($relatedJobs))

                        @foreach($relatedJobs as $relatedJob)


<?php 

if($relatedJob->salary_type == 'single_salary'){

    $relatedJobsalary = '<strong><span class="symbol">'.$relatedJob->salary_currency.'</span>'.number_format($relatedJob->salary_from).'</strong>';
}else if($relatedJob->salary_type == 'salary_in_range'){

    $relatedJobsalary = '<strong><span class="symbol">'.$relatedJob->salary_currency.'</span>'.number_format($relatedJob->salary_from).' - <span class="symbol">'.$relatedJob->salary_currency.'</span>'.number_format($relatedJob->salary_to).'</strong>';

}else{
    $relatedJobsalary = '<strong><span class="symbol">'.$relatedJob->salary_currency.'</span>'.$relatedJob->salary_from.'</strong>';
} 


?>



                        <?php $relatedJobCompany = $relatedJob->getCompany(); ?>

                        @if(null !== $relatedJobCompany  && $relatedJob->id!=$job->id)

                        <!--Job start-->

                        <li>

                           

                                    

                                    <div class="jobinfo">

                                        <h3><a href="{{route('job.detail', [\Str::slug($relatedJob->getFunctionalArea('functional_area')),$relatedJob->slug])}}" title="{{$relatedJob->title}}">{{$relatedJob->title}}</a></h3> 
										
										<div class="jobitem" title="Salary">{!! $relatedJobsalary !!}




												  @if($relatedJob->salary_type != 'negotiable') {{$relatedJob->getSalaryPeriod('salary_period')}} @endif @if($relatedJob->is_bonus) + <span>{{$relatedJob->is_bonus}}</span>@endif 
                                                @if($relatedJob->is_benifits) + <span>{{$relatedJob->is_benifits}}</span>@endif 
                                            </div>
										 
										<div class="location"><i class="fas fa-map-marker-alt"></i> {{$relatedJob->getCity('city')}}</div>

                                        

                                    </div>

                                    <div class="clearfix"></div>

                                                           

                        </li>

                        <!--Job end--> 

                        @endif

                        @endforeach

                        @endif



                        <!-- Job end -->

                    </ul>

                </div>

                

               

            </div>

        </div>

    </div>

</div>


@include('includes.footer')

@endsection

@push('styles')

<style type="text/css">

    .view_more{display:none !important;}

</style>

@endpush

@push('scripts') 

<script>


    $(document).ready(function ($) {

        $("form").submit(function () {

            $(this).find(":input").filter(function () {

                return !this.value;

            }).attr("disabled", "disabled");

            return true;

        });

        $("form").find(":input").prop("disabled", false);



        $(".view_more_ul").each(function () {

            if ($(this).height() > 100)

            {

                $(this).css('height', 100);

                $(this).css('overflow', 'hidden');

                //alert($( this ).next());

                $(this).next().removeClass('view_more');

            }

        });







    });

</script> 
<script>


        //This is not production quality, its just demo code.
      
    //list.clear();

    $('.jobButtons').on('click', '.save_job', function() {
      var id = $(this).data('id');
      if(getCookie('laravel_cookie_consent')==1){
      	

      var count = list.items();
      var link = "{{ route('my.favourite.jobs') }}"; 

      var limit = count.length;
      @if(Auth::user())
      if(limit>19){
        swal({



            title: "Limit Exceeded",



            text: 'Registered users can store a maximum of 20 jobs, Unregistered users can only save a maximum of 5 jobs.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
      	list.add(id);
      	@if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      	
      }
      @else
      if(limit>4){
        swal({



            title: "Unregistered Limit Exceeded",



            text: 'Unregistered Users can save up to 5 jobs using cookies. To save more Jobs and Job Alerts, track, edit, and delete, you must be registered and signed in to your account.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
      	list.add(id);
      	@if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      }
      @endif
      if(count.length>1){
            $('.addsaved').html('<a style="color:yellow"  href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' jobs Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }else{
        $('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' job Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }
       setInterval("hide_savedAlert()",7000);
      $( "#job_" + id ).removeClass('save_job');
        $( "#job_" + id ).addClass('remove_job');
        $( "#job_" + id ).attr('title','Remove from saved Jobs');
        $( "#job_" + id+' i' ).removeAttr('class');
        $( "#job_" + id ).html('<i class="fas fa-star"></i> Saved');
    }else{
    	swal({



                        title: "Allow Cookies",



                        text: 'Cookies is currently disabled, to save up to 5 Jobs without registering or logging into your account,  you will need to enable cookies within your web browser.',



                        icon: "error",



                        button: "OK",



                    });
    }
       
      })


     $( function() {
        var array = list.items();
        jQuery.each( array, function( i, val ) {
        $( "#job_" + val ).removeClass('save_job');
        $( "#job_" + val ).addClass('remove_job');
        $( "#job_" + val ).attr('title','Remove from saved Jobs');
        $( "#job_" + val ).html('<i class="fas fa-star"></i> Saved');
        }); 
     })
     $('.jobButtons').on('click', '.remove_job', function() {
      var id = $(this).data('id');
      list.remove(id);
      @if(Auth::user())
      $.ajax({
        type: "GET",
        url: "{{route('remove.from.favourite')}}",
        data: {id : id},
      });
      @endif
      var count = list.items();
      if(count.length>0){
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span>');
      }else{
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="far fa-star"></i></a>');
      }
       
        $( "#job_" + id ).removeClass('remove_job');
        $( "#job_" + id ).addClass('save_job');
        $( "#job_" + id ).attr('title','Save Job');
        $( "#job_" + id ).html('<i class="far fa-star" aria-hidden="true"></i> Save'); 
      })



 @if(session()->get('message'))
    	swal("Thank You", "{{session()->get('message')}}", "success");
  	@endif

  	function set_sessions() {
        var current_url = "{{url()->current()}}";
        //alert(current_url);
        $.ajax({
            type: 'POST',
            url: "{{route('set-sessionss')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'set_session': current_url
            },
            success: function(data) {
                window.location.href = "{{route('my.profile')}}#cvs";
            },
        });
        
        return false;

    }


    function set_session() {
        var current_url = "{{url()->current()}}";
        //alert(current_url);
        $.ajax({
            type: 'POST',
            url: "{{route('set-sessionss')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'set_session': current_url
            }
        });
        return false;

    }


</script>

@endpush