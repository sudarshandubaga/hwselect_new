@extends('layouts.app')



@section('content') 

<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 

@include('flash::message')



@include('includes.inner_top_search')

<!-- Inner Page Title end -->



<div class="listpgWraper">
    <div class="container-fluid">

        <form action="{{route('job.list')}}" method="get" id="search-job-list">



            <!-- Search Result and sidebar start -->



            <div class="row"> 



                @include('includes.job_list_side_bar')



                



                <div class="col-lg-9 col-sm-12"> 



                    <!-- Search List -->

					

					

					

					

		<div class="alert jobalert">

		<span class="email-jobs-inline email-jobs-center signed-out gtmEmailMeJobsInline btn-job-alert">

			
			@if (Request::get('search') != '' || Request::get('location') != '')
			<a class="btn btn-secondary btn-job-alert" href="javascript:;"><i class="far fa-bell"></i> Save Job Alerts </a>
			@else
			<a class="btn btn-secondary btn-job-alert-disabled" disabled href="javascript:;"><i class="far fa-bell"></i> Save Job Alerts</a>
            @endif

		</span>

			Get the latest Job Alerts sent directly to your inbox.

	</div>

					

					<div class="displayresult">

				
{{__('Displaying Results')}} : {{ $jobs->firstItem() }} - {{ $jobs->lastItem() }} {{__('Total')}} {{ $jobs->total() }}

				</div>

					



                    <ul class="searchList">



                        <!-- job start --> 



                        @if(isset($jobs) && count($jobs)) <?php $count_1 = 1; ?> 

                        @foreach($jobs as $job) @php $company =



                            $job->getCompany();  
                            if($job->admin_reviewed !== 1){
                            continue;
                        }
						@endphp
<?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?>  
                          
    


                            <?php if($count_1 == 7) {?>
                                
                                <li @if($job->is_featured) class="feat" @endif>
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
                                         <?php
                                                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $job->created_at);
                                                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d H:i:s'));
                                                    $diff_in_hours = $to->diffInHours($from);
                                                    ?>
                                                    <?php if (null !== ($diff_in_hours) && $diff_in_hours < 24) { ?>
                                                    <span class="jbtype featured">New</span>
                                                    <?php } ?>
											
											<span class="jbtype featbadge">Featured</span>
                                        </h3>

                                     

                                        <div class="row">

                                            <div class="col-lg-6">

                                            <div class="jobitem itemsalary" title="Salary">{!!$salary!!}
                                            <?php 
                        $all_bouns = '';
                        if(null!==($job->bonus)){
                            $bon = json_decode($job->bonus);
                            if(null!==($bon)){
                                foreach ($bon as $key => $value) {
                                    $bonus = App\Bonus::where('id',$value)->first();
                                    if(null!==($bonus)){
                                       $all_bouns .=$bonus->bonus.', '; 
                                    }
                                    
                                }
                            }

                        }
                        //dd($all_bouns);


                        $all_benifits = '';
                        if(null!==($job->benifits)){
                            $beni = json_decode($job->benifits);
                            if(null!==($beni)){
                                foreach ($beni as $key => $val) {
                                    $benifits = App\Benifits::where('id',$val)->first();
                                    if(null!==($benifits)){
                                       $all_benifits .=$benifits->benifits.', ';
                                    }
                                }
                            }

                        }
                     ?>


                     @if($job->salary_type != 'negotiable')  @endif @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif
                                            </div>

                                                <div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
                                                @if(null!==($job->getCareerLevel('career_level')))
                                        <div class="jobitem" title="Experience Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
                                        @endif

                                                

                                            </div>

                                            <div class="col-lg-6">

                                            <div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getLocation()}}</div>

                                        

                                        <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i> {{$job->created_at->format('d-M-Y')}}</div>
                                            </div>
                                        </div>

                                    <p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn">                                       
                                    <a href="javascript:void(0)" id="job_{{$job->id}}" data-id="{{$job->id}}" class="btn save_job"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>
                                    

                                       <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="btn apply"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('Apply Now')}}</a>

                                    </div>
                                </div>
                            </div>
                        </li>



                            <?php }else{ ?>
                        <li @if($job->is_featured) class="feat" @endif>
                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="jobinfo">
                                        <h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a>
										 <?php
                                                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $job->created_at);
                                                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d H:i:s'));
                                                    $diff_in_hours = $to->diffInHours($from);
                                                    ?>
                                                    <?php if (null !== ($diff_in_hours) && $diff_in_hours < 24) { ?>
                                                    <span class="jbtype featured">New</span>
                                                    <?php } ?>
											
											<span class="jbtype featbadge">Featured</span>
											
										</h3>

                                        

										<div class="row">

											<div class="col-lg-6">

											<div class="jobitem itemsalary" title="Salary">{!!$salary!!}
                    <?php 
                        $all_bouns = '';
                        if(null!==($job->bonus)){
                            $bon = json_decode($job->bonus);
                            if(null!==($bon)){
                                foreach ($bon as $key => $value) {
                                    $bonus = App\Bonus::where('id',$value)->first();
                                    if(null!==($bonus)){
                                       $all_bouns .=$bonus->bonus.', '; 
                                    }
                                }
                            }

                        }
                        

                        $all_benifits = '';
                        if(null!==($job->benifits)){
                            $beni = json_decode($job->benifits);
                            if(null!==($beni)){
                                foreach ($beni as $key => $val) {
                                    $benifits = App\Benifits::where('id',$val)->first();
                                    if(null!==($benifits)){
                                       $all_benifits .=$benifits->benifits.', ';
                                    }
                                    
                                }
                            }

                        }
                     ?>


                      - {{$job->getSalaryPeriod('salary_period')}} @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                                                @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif 
                                            </div>

												<div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>

											
										
										@if(null!==($job->getCareerLevel('career_level')))	
										<div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> 
											{{$job->getCareerLevel('career_level')}}
										</div>
										@endif
												

											</div>

											<div class="col-lg-6">

											<div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getLocation()}}</div>

										

										<div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i> {{$job->created_at->format('d-M-Y')}} {{ucfirst($job->is_extended)}}</div>
											</div>
										</div>

									<p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>

                                    <div class="clearfix"></div>
                                </div>
							</div>

                                <div class="col-lg-2 col-md-3">
                                   <div class="listbtn">									   
                                    <a href="javascript:void(0)" id="job_{{$job->id}}" data-id="{{$job->id}}" class="btn save_job"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>
									

									   <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="btn apply"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('Apply Now')}}</a>

									</div>
                                </div>
                            </div>
                        </li>
						 <?php } ?>

                            <?php $count_1++; ?>
                        <!-- job end --> 

                        @endforeach @endif
                            <!-- job end -->
						
						
						@if(isset($jobs) && count($jobs))

                                @else
                                <li class="text-center norecord">
                                  <p>There are currently no jobs available for ({{Request::get('search')?Request::get('search'):null }}{{ Request::get('address')?'  '.Request::get('address'):null }}) – Please try another Job title or Location.</p>
                                </li>

                                @endif
						
                    </ul>
                                
                    <!-- Pagination Start -->



                    <div class="pagiWrap">



                        <div class="row">



                            <div class="col-lg-5">



                                <div class="showreslt">



                                    {{__('Displaying Results')}} : {{ $jobs->firstItem() }} - {{ $jobs->lastItem() }} {{__('Total')}} {{ $jobs->total() }}



                                </div>



                            </div>





                            <div class="col-lg-7">



                                @if(isset($jobs) && count($jobs))



                                {{ $jobs->appends(request()->query())->links() }}

                             

                                @endif



                            </div>



                        </div>



                    </div>



                    <!-- Pagination end --> 



                   







                </div>



				



				



            </div>



        </form>



    </div>



</div>

<div class="modal fade" id="show_alert" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="submit_alert">
          @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="registerModalLabel">Receive the latest job vacancies directly to your email</h5>
        <button style="margin-top: -16px !important; " type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">



                <input type="hidden" name="country_id" value="@if(isset(Request::get('country_id')[0])) {{ Request::get('country_id')[0] }} @endif">



                <input type="hidden" name="state_id" value="@if(isset(Request::get('state_id')[0])){{ Request::get('state_id')[0] }} @endif">



                <input type="hidden" name="city_id" value="@if(isset(Request::get('city_id')[0])){{ Request::get('city_id')[0] }} @endif">



                <input type="hidden" name="functional_area_id" value="@if(isset(Request::get('functional_area_id')[0])){{ Request::get('functional_area_id')[0] }} @endif">
        
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter Job Description Keywords</label>
            <input type="text" name="search" maxlength="50" class="form-control"  placeholder="{{__('Example: Project Manager, Line Manager')}}" id="search" value="{{ Request::get('search')?Request::get('search'):null }}">
          </div>
          
          <div class="form-group">
                <label for="recipient-name" class="col-form-label">Email</label>
                <input type="text" class="form-control" name="email" id="email" maxlength="50" placeholder="Enter Email Address"value="@if( Auth::check() ){{Auth::user()->email}}@endif" readonly="readonly">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </form>
    </div>
  </div>
</div>





@include('includes.footer')



@endsection



@push('styles')



<style type="text/css">





    .hide_vm_ul{



        height:100px;



        overflow:hidden;



    }



    .hide_vm{



        display:none !important;



    }



    .view_more{



        cursor:pointer;



    }



</style>



@endpush



@push('scripts') 



<script>



    //$('.btn-job-alert').on('click', function() {



        //$('#show_alert').modal('show');



    //})



    $(document).ready(function ($) {



        $("#search-job-list").submit(function () {



            $(this).find(":input").filter(function () {



                return !this.value;



            }).attr("disabled", "disabled");



            return true;



        });



        $("#search-job-list").find(":input").prop("disabled", false);







        $(".view_more_ul").each(function () {



            if ($(this).height() > 100)



            {



                $(this).addClass('hide_vm_ul');



                $(this).next().removeClass('hide_vm');



            }



        });



        $('.view_more').on('click', function (e) {



            e.preventDefault();



            $(this).prev().removeClass('hide_vm_ul');



            $(this).addClass('hide_vm');



        });







    });



    if ($("#submit_alert").length > 0) {



    $("#submit_alert").validate({







        rules: {



            email: {



                required: true,



                maxlength: 5000,



                email: true



            },

            search: {
                required: true,
                remote: {
                            type: 'get',
                            url: "/check-alert",
                            data: {
                              search: function() {
                                return $( "#search" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
            }



        },



        messages: {



            email: {



                required: "Email Address is Required",



            },
            search: {



                required: "Description Required",
                remote: "This description already exists in your saved Job Alerts."



            }







        },



        submitHandler: function(form) {



            $.ajaxSetup({



                headers: {



                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')



                }



            });



            $.ajax({



                url: "{{route('subscribe.alert')}}",



                type: "GET",



                data: $('#submit_alert').serialize(),



                success: function(response) {



                    $("#submit_alert").trigger("reset");



                    $('#show_alert').modal('hide');

                    var result = response["msg"].match(/\((.*)\)/);

                    swal({



                        title: "Job Alert: "+result[1]+" Successfully Added",



                        text: response["msg"],



                        icon: "success",



                        button: "OK",



                    });



                }



            });



        }



    })



}



$('.btn-job-alert').on('click', function() {
    @if(Auth::user())
    $('#show_alert').modal('show');
    @else
    swal({
        title: "Save Job Alerts",

        text: "To save Job Alerts you must be Registered and Logged in",

        icon: "error",

        buttons: {
	    Login: "Login",
	    register: "Register",
	    hello: "OK",
	  },
});
    @endif

})

   if ($("#new_alert").length > 0) {

    $("#new_alert").validate({



        rules: {

            email: {

                required: true,

                maxlength: 5000,

                email: true

            }

        },

        messages: {

            email: {

                required: "Email Address Required",

            }



        },

        submitHandler: function(form) {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $.ajax({

                url: "{{route('subscribe.alert')}}",

                type: "GET",

                data: $('#new_alert').serialize(),

                success: function(response) {

                    swal({

                        title: "Success",

                        text: response["msg"],

                        icon: "success",

                        button: "OK",

                    });

                }

            });

        }

    })

}

if ($("#submit_alert").length > 0) {

    $("#submit_alert").validate({



        rules: {

            email: {

                required: true,

                maxlength: 5000,

                email: true

            }

        },

        messages: {

            email: {

                required: "Email Address Required",

            }



        },

        submitHandler: function(form) {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $.ajax({

                url: "{{route('subscribe.alert')}}",

                type: "GET",

                data: $('#submit_alert').serialize(),

                success: function(response) {

                    $("#submit_alert").trigger("reset");

                    $('#show_alert').modal('hide');

                    swal({

                        title: "Success",

                        text: response["msg"],

                        icon: "success",

                        button: "OK",

                    });

                }

            });

        }

    })

}



</script>

<script>


        //This is not production quality, its just demo code.
      
    //list.clear();

    $('.listbtn').on('click', '.save_job', function() {
      var id = $(this).data('id');
      if(getCookie('laravel_cookie_consent')==1){
      	

      var count = list.items();
      var link = "{{ route('my.favourite.jobs') }}"; 

      var limit = count.length;
      @if(Auth::user())
      if(limit>19){
        swal({



            title: "Limit Exceeded",



            text: 'Registered users can store a maximum of 20 jobs, Unregistered users can only save a maximum of 5 jobs.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
        list.add(id);
        @if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      }
      @else
      if(limit>4){
        swal({



            title: "Unregistered Limit Exceeded",



            text: 'Unregistered Users can save up to 5 jobs using cookies. To save more Jobs and Job Alerts, track, edit, and delete, you must be registered and signed in to your account.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
        list.add(id);
        @if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      }
      @endif
      
      if(count.length>1){
            $('.addsaved').html('<a style="color:yellow"  href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' jobs Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }else{
        $('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' job Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }
       setInterval("hide_savedAlert()",7000);
      $( "#job_" + id ).removeClass('save_job');
        $( "#job_" + id ).addClass('remove_job');
        $( "#job_" + id ).attr('title','Remove from saved Jobs');
        $( "#job_" + id+' i' ).removeAttr('class');
        $( "#job_" + id ).html('<i class="fas fa-star"></i> Saved');
    }else{
    	swal({



                        title: "Allow Cookies",



                        text: 'Cookies is currently disabled, to save up to 5 Jobs without registering or logging into your account,  you will need to enable cookies within your web browser.',



                        icon: "error",



                        button: "OK",



                    });
    }
       
      })


     $( function() {
        var array = list.items();
        jQuery.each( array, function( i, val ) {
        $( "#job_" + val ).removeClass('save_job');
        $( "#job_" + val ).addClass('remove_job');
        $( "#job_" + val ).attr('title','Remove from saved Jobs');
        $( "#job_" + val ).html('<i class="fas fa-star"></i> Saved');
        }); 
     })
     $('.listbtn').on('click', '.remove_job', function() {
      var id = $(this).data('id');
      list.remove(id);
      @if(Auth::user())
      $.ajax({
        type: "GET",
        url: "{{route('remove.from.favourite')}}",
        data: {id : id},
      });
      @endif
      var count = list.items();
      if(count.length>0){
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span>');
      }else{
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="far fa-star"></i></a>');
      }
       
        $( "#job_" + id ).removeClass('remove_job');
        $( "#job_" + id ).addClass('save_job');
        $( "#job_" + id ).attr('title','Save Job');
        $( "#job_" + id ).html('<i class="far fa-star" aria-hidden="true"></i> Save'); 
      })



     $(document).on('click','.swal-button--Login',function(){
     	window.location.href = "{{route('login')}}";
     })
     $(document).on('click','.swal-button--register',function(){
     	window.location.href = "{{route('register')}}";
     })


</script>
<script>
  $( function() {

    var mySource  = {!!$skills!!};
    $( "#jbsearch" ).autocomplete({
    minLength: 1,
    source: function(req, resp) {
      var q = req.term;
      var myResponse = [];
      $.each(mySource, function(key, item) {
        if (item.toLowerCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.toUpperCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.indexOf(q) === 0){
          myResponse.push(item);
        }
      });
      resp(myResponse);
    },
    
  });
    });
  </script> 

@include('includes.country_state_city_js')
<style type="text/css">
    .remove_job svg{
        color: "#ffeb3b";
    }
    
</style>


@endpush