@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Jobs Applied for')])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <div class="row">

            @include('includes.user_dashboard_menu')



            <div class="col-md-9 col-sm-8"> 

                <div class="myads">
					<div class="row mb-4">
<div class="col-md-6 col-6"><h3>{{__('Jobs Applied for')}}</h3></div>
<div class="col-md-6 col-6"><div class="delsvedjobs">   
                    <a href="{{route('job_apply.all.delete')}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete all applied for jobs?');"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('Delete All')}}</a></div></div>
</div>
			
		<p class="appliedalert">Jobs applied for that have expired or deactivated are automatically removed, and will reappear if reactivated or expiry length extended.</p>
					

                 
                    @include('flash::message')

                    <ul class="searchList">

                        <!-- job start --> 

                        @if(isset($jobs) && count($jobs))

                        @foreach($jobs as $job)
                        @if($job->expiry_date <  \Carbon\Carbon::now() || $job->is_active != 1)
                        @else
                         <?php 

if($job->salary_type == 'single_salary'){
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from).'</strong>';
}else{
$salary = '';
}

}else if($job->salary_type == 'salary_in_range'){
//echo $job->salary_from;
$salary_from = (null!==($job->salary_from))?'<strong><span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_from):null;
$salary_to = (null!==($job->salary_from))?' - <span class="symbol">'.$job->salary_currency.'</span>'.number_format($job->salary_to).'</strong>':null;
$salary = $salary_from.$salary_to;

}else if($job->salary_type=='negotiable'){
    if(null!==($job->salary_from)){
$salary = '<strong>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
}else{
if(null!==($job->salary_from)){
$salary = '<strong><span class="symbol">'.$job->salary_currency.'</span>'.$job->salary_from.'</strong>';
}else{
$salary = '';
}
} 


?>   

                        @php $company = $job->getCompany(); @endphp

                        @if(null !== $company)

                        
						
						
<li>
    <div class="row">
        <div class="col-lg-10 col-md-9">
            <div class="jobinfo">
				
				<div class="row">
					<div class="col-lg-8"><h3><a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')), $job->slug])}}" title="{{$job->title}}">{{$job->title}} (Job ID: {{ $job->job_id }})</a></h3></div>
					<div class="col-lg-4">
						<?php 
                    $job_apply = App\JobApply::where('job_id',$job->id)->where('user_id',Auth::user()->id)->first();
                 ?>
						<div class="jobitem" title="Date Applied"><i class="far fa-calendar"></i>Applied: {{date('d-M-Y',strtotime($job_apply->created_at))}}</div></div>
				</div>
				
                
                
				
				
				
				<div class="row">
					<div class="col-lg-6">
					<div class="jobitem itemsalary" title="Salary">{!!$salary!!}
                    <?php 
$all_bouns = '';
if(null!==($job->bonus)){
$bon = json_decode($job->bonus);
if(null!==($bon)){
  foreach ($bon as $key => $value) {
    $bonus = App\Bonus::where('id',$value)->first();
            if(null!==($bonus)){
               $all_bouns .=$bonus->bonus.', '; 
            }
  }
}

}


$all_benifits = '';
if(null!==($job->benifits)){
$beni = json_decode($job->benifits);
if(null!==($beni)){
  foreach ($beni as $key => $val) {
    $benifits = App\Benifits::where('id',$val)->first();
            if(null!==($benifits)){
               $all_benifits .=$benifits->benifits.', ';
            }
  }
}

}
?>


 - {{$job->getSalaryPeriod('salary_period')}}  @if(null!==($job->bonus) && isset($all_bouns) && $all_bouns !='' && $all_bouns !=', ') + <span>{{trim($all_bouns,', ')}}</span>@endif 
                        @if(null!==($job->benifits) && isset($all_benifits ) && $all_benifits !='' && $all_benifits !=', ') + <span>{{trim($all_benifits,', ')}}</span>@endif
                        
                    </div>

						<div class="jobitem" title="Employment Type"><i class="far fa-clock"></i> {{$job->getJobType('job_type')}}</div>
				<div class="jobitem" title="Career Level"><i class="fas fa-chart-bar"></i> {{$job->getCareerLevel('career_level')}}</div>
					</div>

					<div class="col-lg-6">
					<div class="jobitem" title="Location"><i class="fas fa-map-marker-alt"></i> {{$job->getCity('city')}}</div>
				  <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>Posted: {{$job->created_at->format('d-M-Y')}}</div>
				  <div class="jobitem" title="Posted date"><i class="far fa-calendar-alt"></i>Expiry Date: {{$job->expiry_date->format('d-M-Y')}}</div>  
					
					</div>
					
					
					
				</div>
			<p>{{\Illuminate\Support\Str::limit(strip_tags($job->description), 150, '...')}}</p>

            <div class="clearfix"></div>
        </div>
	</div>

        <div class="col-lg-2 col-md-3">
           <div class="listbtn">
			<a href="javascript:void(0)" id="job_{{$job->id}}" data-id="{{$job->id}}" class="btn save_job"><i class="far fa-star" aria-hidden="true"></i> {{__('Save')}}</a>

			  <a href="{{route('job.detail', [\Str::slug($job->getFunctionalArea('functional_area')),$job->slug])}}" class="btn apply"><i class="fas fa-paper-plane" aria-hidden="true"></i> {{__('View Details')}}</a>

        <a href="{{route('job_apply.delete', [$job->id, $job->slug])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete {{ $job->title }}');"><i class="fas fa-trash" aria-hidden="true"></i> {{__('Delete')}}</a>

			</div>
        </div>
    </div>
</li>
                        @endif
						
						
                        <!-- job end --> 

                        @endif

                        @endforeach

                        @endif

                    </ul>

                </div>
                <div class="pagiWrap">



                        <div class="row">



                            <div class="col-lg-5">



                                <div class="showreslt">



                                    {{__('Displaying Results')}} : {{ $jobs->firstItem() }} - {{ $jobs->lastItem() }} {{__('Total')}} {{ $jobs->total() }}



                                </div>



                            </div>





                            <div class="col-lg-7">



                                @if(isset($jobs) && count($jobs))



                                {{ $jobs->appends(request()->query())->links() }}

                             

                                @endif



                            </div>



                        </div>



                    </div>

            </div>

        </div>

    </div>

</div>

@include('includes.footer')

@endsection

@push('scripts')

@include('includes.immediate_available_btn')

<script>


        //This is not production quality, its just demo code.
      
    //list.clear();

    @if(session()->get('message'))
    	swal("Thank You", "{{session()->get('message')}}", "success");
  	@endif

    $('.listbtn').on('click', '.save_job', function() {
      var id = $(this).data('id');
      if(getCookie('laravel_cookie_consent')==1){
        

      var count = list.items();
      var link = "{{ route('my.favourite.jobs') }}"; 

      var limit = count.length;
      @if(Auth::user())
      if(limit>19){
        swal({



            title: "Limit Exceeded",



            text: 'Registered users can store a maximum of 20 jobs, Unregistered users can only save a maximum of 5 jobs.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
        list.add(id);
        @if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      }
      @else
      if(limit>4){
        swal({



            title: "Unregistered Limit Exceeded",



            text: 'Unregistered Users can save up to 5 jobs using cookies. To save more Jobs and Job Alerts, track, edit, and delete, you must be registered and signed in to your account.',



            icon: "error",



            button: "OK",



        });
        exit;
      }else{
        list.add(id);
        @if(Auth::user())
          $.ajax({
            type: "GET",
            url: "{{route('add.to.favourite')}}",
            data: {id : id},
          });
        @endif
      }
      @endif
      
      if(count.length>1){
            $('.addsaved').html('<a style="color:yellow"  href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' jobs Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }else{
        $('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">'+count.length+' job Saved</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, log in or register.</p>\
                                </div>');
      }
       setInterval("hide_savedAlert()",7000);
      $( "#job_" + id ).removeClass('save_job');
        $( "#job_" + id ).addClass('remove_job');
        $( "#job_" + id ).attr('title','Remove from saved Jobs');
        $( "#job_" + id+' i' ).removeAttr('class');
        $( "#job_" + id ).html('<i class="fas fa-star"></i> Saved');
    }else{
        swal({



                        title: "Allow Cookies",



                        text: 'Cookies is currently disabled, to save up to 5 Jobs without registering or logging into your account,  you will need to enable cookies within your web browser.',



                        icon: "error",



                        button: "OK",



                    });
    }
       
      })


     $( function() {
        var array = list.items();
        jQuery.each( array, function( i, val ) {
        $( "#job_" + val ).removeClass('save_job');
        $( "#job_" + val ).addClass('remove_job');
        $( "#job_" + val ).attr('title','Remove from saved Jobs');
        $( "#job_" + val ).html('<i class="fas fa-star"></i> Saved');
        }); 
     })
     $('.listbtn').on('click', '.remove_job', function() {
      var id = $(this).data('id');
      list.remove(id);
      @if(Auth::user())
      $.ajax({
        type: "GET",
        url: "{{route('remove.from.favourite')}}",
        data: {id : id},
      });
      @endif
      var count = list.items();
      if(count.length>0){
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span>');
      }else{
        var link = "{{ route('my.favourite.jobs') }}";
        $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="far fa-star"></i></a>');
      }
       
        $( "#job_" + id ).removeClass('remove_job');
        $( "#job_" + id ).addClass('save_job');
        $( "#job_" + id ).attr('title','Save Job');
        $( "#job_" + id ).html('<i class="far fa-star" aria-hidden="true"></i> Save'); 
      })






</script>

@endpush