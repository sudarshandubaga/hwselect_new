<?php

if (!isset($seo)) {

    $seo = (object)array('seo_title' => $siteSetting->site_name, 'seo_description' => $siteSetting->site_name, 'seo_keywords' => $siteSetting->site_name, 'seo_other' => '');

}
$seotitle = explode("-",$seo->seo_title, 3);
if(!empty($seotitle[2])){
  $seotle = explode("-",$seotitle[2]);
  $seoTitle = ucfirst(implode(" ",$seotle));
}

?>

<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" class="{{ (session('localeDir', 'ltr'))}}" dir="{{ (session('localeDir', 'ltr'))}}">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php if($seotitle[0] == 'Jobsector' || $seotitle[0] == 'Jobskill' || $seotitle[0] == 'Joblocation' || $seotitle[0] == 'Jobrole') { ?>{{($seoTitle)}}<?php } else { ?>{{($seo->seo_title)}}<?php } ?></title>

    <meta name="Description" content="{!! $seo->seo_description !!}">

    <meta name="Keywords" content="{!! $seo->seo_keywords !!}">

    {!! $seo->seo_other !!}

    @yield('seo_other_meta')

    <!-- Fav Icon -->

    <link rel="shortcut icon" href="{{asset('/')}}favicon.ico">

    <!-- Slider -->

    <link href="{{asset('/')}}js/revolution-slider/css/settings.css" rel="stylesheet">

    <!-- Owl carousel -->

    <link href="{{asset('/')}}css/owl.carousel.min.css" rel="stylesheet">

    <link href="{{asset('/')}}css/owl.theme.default.min.css" rel="stylesheet">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"> -->


    <!-- Bootstrap -->

    <link href="{{asset('/')}}css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />

    <link href="{{asset('/')}}css/font-awesome.css" rel="stylesheet">

    <!-- Custom Style -->

    <link href="{{asset('/')}}css/main.css" rel="stylesheet">

    @if((session('localeDir', 'ltr') == 'rtl'))

    <!-- Rtl Style -->

    <link href="{{asset('/')}}css/rtl-style.css" rel="stylesheet">

    @endif

   <link href="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

          <script src="{{asset('/')}}js/html5shiv.min.js"></script>

          <script src="{{asset('/')}}js/respond.min.js"></script>

        <![endif]-->

    @stack('styles')
	
	
	{!! $siteSetting->ganalytics !!}

  {!! $siteSetting->google_tag_manager_for_head !!}
	
	

	
	
	
</head>



<body>

    @yield('content')

    @include('cookieConsent::index')




    <div class="modal fade" id="showmodalrequest" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

            <form id="send-form-account-request">

                @csrf


                <div class="modal-header">                    

                    <h4 class="modal-title">Request to Close</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                @php
                  $reasons = \App\Modules_data::whereHas('modules', function ($q) {
                    $q->where('slug', 'account-deleted-reasons');
                  })->pluck('title', 'title');
                  $reasons['other'] = 'Other';
                @endphp

                <div class="modal-body">
                        <p class="mb-3">Please tell us why you would like to close your account, closing your account will delete all your personal information from our database and any saved jobs. You will be logged out permanently.</p>
					
                        <div class="form-group">
                          {{ Form::select('reason', $reasons, null, ['placeholder' => 'Select Reason', 'required' => 'required', 'class' => 'form-control', 'id' => 'reason', 'onchange' => 'checkreason(this.value)']) }}
                        </div>
					
                        <div class="form-group">

                          <input type="hidden" name="option" value="close_account" id="option">

                        <textarea class="form-control" required="required" name="message" placeholder="Why?" id="message" cols="10" rows="3" style="display: none;"></textarea>
						            <div class="advice"> <span class="span-text">You will be logged out permanently and will need to Re-Register to open a new account!</span></div>
                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="button" class="btn btn-primary submitAction">Submit</button>

                </div>

            </form>

        </div>



    </div>

</div>

	

<div class="modal fade" id="requesttodeletejob" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

            <form id="requestdelete">

                @csrf

                <input type="hidden" name="id" id="job_del_id">
                <div class="modal-header">
                    <h4 class="modal-title">Request Admin to Delete Job</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
						<p class="mb-3">Deleting Job will remove all details from our database.</p>
						
                        <label class="job-title-lable pb-2"></label>
                        <textarea class="form-control" required="required" name="reason" placeholder="Enter Reason here." id="message" cols="10" rows="3"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary submitActiontoDelete">Submit</button>
                </div>

            </form>

        </div>



    </div>

</div>

<input type="hidden" id="latitude">
<input type="hidden" id="longitude">

    <!-- Bootstrap's JavaScript -->

    <script src="{{asset('/')}}js/jquery.min.js"></script>
    <script src="{{asset('/')}}js/jquery-ui.js"></script>

    <script src="{{asset('/')}}js/popper.js"></script>
    <script src="{{asset('/')}}js/bootstrap.min.js"></script>


    <script src="{{asset('/')}}js/all.min.js"></script>



    <!-- Owl carousel -->

    
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> -->
 <script src="{{asset('/')}}js/owl.carousel.min.js"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL PLUGINS -->

    <script src="{{ asset('/') }}admin_assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

    <script src="{{ asset('/') }}admin_assets/global/plugins/jquery.scrollTo.min.js" type="text/javascript"></script>

    <!-- Revolution Slider -->

    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>

    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{ asset('/') }}js/jquery.validate.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsKIwI70lePVwWa-T4X8SfnHQRxj7Vw6M&libraries=places"></script> -->
    @if (!empty($setting->autocomplete_api))
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $setting->autocomplete_api }}&libraries=places"></script>
    @endif

     <script src="{{asset('/')}}js/jquery.cookie.js"></script>
     
     <script src="{{ asset('js/share.js') }}"></script>
    {!! NoCaptcha::renderJs() !!}

    <script type="text/javascript">

function checkreason(reason) {
  if(reason === 'other') {
      $('#message').show();
  } else {
    $('#message').hide();
  }
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript" src="//translate.google.com/#en/hi/Hello"></script>

<style type="text/css">
	.goog-te-banner-frame.skiptranslate {
    display: none !important;

    }

    #google_translate_element {margin-top: 22px;}

@media print {
  #google_translate_element {display: none;}
}

</style>
  <script type="text/javascript">
    var cookieList = function(cookieName) {
      //When the cookie is saved the items will be a comma seperated string
      //So we will split the cookie by comma to get the original array
      var cookie = $.cookie(cookieName);
      //Load the items or a new array if null.
      var items = cookie ? cookie.split(/,/) : new Array();

      //Return a object that we can use to access the array.
      //while hiding direct access to the declared items array
      //this is called closures see http://www.jibbering.com/faq/faq_notes/closures.html
      return {
          "add": function(val) {
              //Add to the items.
              items.push(val);
              //Save the items to a cookie.
              //EDIT: Modified from linked answer by Nick see 
              //      http://stackoverflow.com/questions/3387251/how-to-store-array-in-jquery-cookie
              $.cookie(cookieName, items.join(','), { path: '/' });
          },
          "remove": function (val) { 
              //EDIT: Thx to Assef and luke for remove.
              //indx = items.indexOf(val);
              items = jQuery.grep(items, function(value) {
                  return value != val;
                });
              $.cookie(cookieName, items, { path: '/' });        },
          "clear": function() {
              items = null;
              //clear the cookie.
              $.cookie(cookieName, '', { path: '/' });
          },
          "items": function() {
              //Get all the items.
              return items;
          }
        }
      }


    var list = new cookieList("saved_jobs");  

    function setCookie(cname, cvalue, exdays) {

          var d = new Date();

          d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

          var expires = "expires=" + d.toUTCString();

          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

        }
        function getCookie(cname) {

          var name = cname + "=";

          var decodedCookie = decodeURIComponent(document.cookie);

          var ca = decodedCookie.split(';');

          for (var i = 0; i < ca.length; i++) {

            var c = ca[i];

            while (c.charAt(0) == ' ') {

              c = c.substring(1);

            }

            if (c.indexOf(name) == 0) {

              return c.substring(name.length, c.length);

            }

          }

          return "";

        }
  </script>
    @stack('scripts')

    <!-- Custom js -->

    <script src="{{asset('/')}}js/script.js"></script>

    <script type="text/JavaScript">

        $(document).ready(function(){

            $(document).scrollTo('.has-error', 2000);

            });

            function showProcessingForm(btn_id){		

            $("#"+btn_id).val( 'Processing .....' );

            $("#"+btn_id).attr('disabled','disabled');		

            }


        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var longitude = position.coords.longitude;
              var latitude = position.coords.latitude;

              if($('#current_location').val()!=''){

              var geocoder = new google.maps.Geocoder();
              var address = $('#current_location').val();

              geocoder.geocode( { 'address': address}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                  var latitude = results[0].geometry.location.lat();
                  var longitude = results[0].geometry.location.lng();
                  $('#latitude').val(latitude);
                  $('#longitude').val(longitude);
                } 
              }); 


                
               }else{
                $('#longitude').val(longitude);
                $('#latitude').val(latitude);
               }
              
              var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&key=AIzaSyCsKIwI70lePVwWa-T4X8SfnHQRxj7Vw6M";
              var place = $.getJSON(url, function(data){

                $.each(data.results, function(i, v){
                  if( $.inArray( "locality", v.types ) != -1 ){
                     
                    $.each(v.address_components, function(i, c){
                    console.log(v.address_components);
                      if( $.inArray( "locality", c.types ) != -1 ){
                         setCookie("city", c.long_name, 365);
                         if($('#current_location').val()==''){
                          $('#current_location').val(c.long_name);
                         }
                          
                      }
                    })
                  } 
                })
              });
            });
          /*var watchID = navigator.geolocation.watchPosition(function(position) {
            do_something(position.coords.latitude, position.coords.longitude);
          });*/
        } else {
           console.error("geolocation not available in navigator"); 
        }
        @if (!empty($setting->autocomplete_api))
          var input_current_location = document.getElementById('current_location');
          var autocomplete_current_location = new google.maps.places.Autocomplete(input_current_location);

          google.maps.event.addListener(autocomplete_current_location, 'place_changed', function () 
          {
            var place = autocomplete_current_location.getPlace();
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
          });
        @endif

          /*$('#current_location').on('keyup',function(){
            var geocoder = new google.maps.Geocoder();
            var address = $(this).val();

            geocoder.geocode( { 'address': address}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
              } 
            }); 
          })

          $('#current_location').on('change',function(){
            var geocoder = new google.maps.Geocoder();
            var address = $(this).val();

            geocoder.geocode( { 'address': address}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
              } 
            }); 
          })*/
      
        function view_message_request(){

                    $('#showmodalrequest').modal('show');

        }

        $('input[type=radio][name=option]').change(function() {
          if (this.value == 'close_account') {
              $('.append_textarea').html('<textarea class="form-control" required="required" name="message" placeholder="Why?" id="message" cols="10" rows="3"></textarea>')
          }
          else if (this.value == 'suspend_account') {
              $('.append_textarea').html('');
          }
      });






    saved_jobs();
 @if(Auth::user())
            
            
            setInterval("notification_interval()",5000);
            setInterval("notification_interval_badge()",5000);
            
            
            
            function notification_interval() {
                    $.ajax({
                        type: 'get',
                        url: "{{route('seeker-append-only-notifications')}}",
                        success: function(res) {
                                //alert(res);
                                $('.notification').html(res);
                           
                        }
                    });
                }


            function notification_interval_badge() {
                    $.ajax({
                        type: 'get',
                        dataType: 'json',
                        url: "{{route('seeker-append-only-notifications-count')}}",
                        data: {
                            '_token': $('input[name=_token]').val(),
                        },
                        success: function(res) {
                            //console.log(res);
                            if(res!=0){
                               $('.badge-danger').html(res); 
                            }
                            
                        }
                    });
                }
                
        @endif

        function saved_jobs(){
          var count = list.items();
          if(count.length>0 && count[0] != null && count[0] != 'null'){
            var link = "{{ route('my.favourite.jobs') }}";
            if(getCookie('svjobalert')=='yes'){
             	$('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span>');
             }else{
             	if(count.length>1){

            	$('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">You have '+count.length+' Saved jobs</h3><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, login or register.</p>\
                                </div>');
		      }else{
		        $('.addsaved').html('<a style="color:yellow" href="'+link+'" class="nav-link"><i class="fas fa-star"></i></a> <span class="badge badge-light">'+count.length+'</span><div class="svjobalert"><h3 class="counter_jobs">You have '+count.length+' Saved jobs</h3>><a href="'+link+'">View saved jobs</a><p>To view your saved jobs on any device, login or register.</p>\
		                                </div>');
		      }	

		      setCookie("svjobalert", 'yes', 365);	
             }
            
      		
            $('.no-savedjobs').hide();
          }else{
            var link = "{{ route('my.favourite.jobs') }}";
            $('.addsaved').html('<a href="'+link+'" class="nav-link"><i class="far fa-star"></i></a>');
            $('.svjobalert').html('');
            $('.no-savedjobs').show();
          } 
        }
        setInterval("hide_savedAlert()",7000);

        function hide_savedAlert(){
          $(document).find('.svjobalert').hide();
        }


        $(document).ready(function(){


            $.ajax({
                type: 'get',
                url: "{{route('check-time')}}",
                success: function(res) {
                        $('.notification').html(res);
                   
                }
            });
        });







</script>


       



</body>



</html>