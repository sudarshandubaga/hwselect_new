@extends('layouts.app')
@push('styles')
<style type="text/css">
    .select2-container .select2-selection--single {
    height: 37px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 6px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 35px !important;
}
</style>
@endpush
@section('content')
@include('includes.header')
<!-- Header end -->
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Mobile Phone Verification')])
<div class="section">

    <div class="container">
      
        <div class="phoneverifywrap">
          <div class="step_1 text-center">
          <h1>{{__('2 step verification')}}</h1>
          <h2>{{__('A verification code will be sent to your registered number')}}</h2>
          <!-- <ol style="text-align: center;">
            <li>1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In</li>
            <li>3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dolor</li>
          </ol> -->
          <?php 
            if(Auth::user() && Auth::user()->phone !=''){
              $phone = Auth::user()->phone;
            }else if(Auth::guard('company')->check() && Auth::guard('company')->user()->phone !=''){
              $phone = Auth::guard('company')->user()->phone;
            }else{
              $phone = '+';
            }
          ?>
            <div class="verifyfield">             
                <div class="form-wrap">
                  <label for="title" class="smshint">Registered Mobile Number is *****{{substr($phone, -4)}}</label>
                    <input style="display: none;"  class="form-control phone_input phone" readonly="readonly" value="{{$phone}}" title="Enter phone ********{{substr($phone, -4)}}" type="text">   
                </div>             
            </div>
            <div class="send" style="text-align: center;">
               <br>
              <button class="btn btn-primary phone_verify_btn verify sbutn" id="phone_verify">Verify Mobile Number</button>
            </div>

         </div>   

        <div class="step_2 text-center"  style="display: none;">
          <h1>{{__('2 step verification')}}</h1>
          <h2>{{__('Enter verification code in the field below and click verify code')}}</h2>
          <!-- <ol style="text-align: center;">
            <li>1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In</li>
            <li>3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dolor</li>
          </ol> -->
            <div class="verifyfield">             
                <div class="form-wrap">
                  <input placeholder="Enter Verification code" maxlength="15" type="text" class="phone_code_input form-control">
                </div>
              
            </div>
            <div class="send" style="text-align: center;">
              <br>
              <button class="btn btn-primary phone_code_verify_btn sbutn">Verify Code</button>
              <button class="btn btn-warning phone_code_resend sbutn">Resend Code</button>
            </div>
        </div>
        </div>
     
    </div>

  </div>
  @include('includes.footer')
@endsection
@push('scripts')

<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>

<script type="text/javascript">
   var config = {
        // apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
        // authDomain: "hwselect-62f4f.firebaseapp.com",
        // databaseURL: "https://hwselect-62f4f.firebaseio.com",
        // projectId: "hw-select-uk",
        // storageBucket: "hwselect-62f4f.appspot.com",
        // messagingSenderId: "269417794687",
        // appId: "1:401245972574:web:207e49cc038d66ee976ccf",
        // measurementId: "G-TSVHYD80QW"
        apiKey: "AIzaSyDLllx53SZiKUGXetvHv6j59_GOuNaUBEA",
        authDomain: "hw-select-uk.firebaseapp.com",
        projectId: "hw-select-uk",
        storageBucket: "hw-select-uk.appspot.com",
        messagingSenderId: "269417794687",
        appId: "1:269417794687:web:b8aee097fea11a0895434a"
    };
      
    
    

  firebase.initializeApp(config);

  function isPhoneNumberValid(phoneNumber) {
    var pattern = /^\+[0-9\s\-\(\)]+$/;
    return phoneNumber.search(pattern) !== -1;
  }
 
  
  $('#phone_code').select2();
  $(document).on("click", ".phone_code_resend", function(){
    location.reload();
  })
  @if(Auth::user())
  $('.phone').on('keyup',function(){
          var val = $('#phone_code').val();
          if(val==''){
            swal("Error", "Please Select phone code before typing phone number", "error");
            $(this).val('');
          }
   })


  $(document).on("click", ".phone_verify_btn.verify", function(){
    $(this).removeClass("verify");
    var uphone = $.trim($('.phone').val());

    //alert(uphone);
    var val = $('#phone_code').val();
    window.uphone = uphone;
    if(true){
         $.ajax({
            type: "GET",
            url: "{{route('verify-number')}}",
            data: { 
                'phone': $(".phone_input").val(), 
                'phone_code': $("#phone_code").val(), 
            },
            }).done(function (data) {
                if(data=='ok'){
                    var appVerifier = new firebase.auth.RecaptchaVerifier('phone_verify', { 'size': 'invisible' });
                      $(this).attr("disabled", "disabled");
                      firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
                        .then(function (confirmationResult) {
                          window.confirmationResult = confirmationResult;
                          $(".step_1").hide();
                          $(".step_2").show();
                        }).catch(function (error) {
                          console.error('Error during signInWithPhoneNumber', error);
                        });
                }else{
                    $('.phone_verify_btn').addClass("verify");
                     swal("Error", "Your Phone number is not valid. Please enter valid Phone Number.", "error");
                     
                }
        }); 
      
    } else { swal("Error", "Please enter valid phone number.", "error"); $('.phone_verify_btn').addClass("verify"); }
  })
@else
$('.phone').on('keyup',function(){
          var val = $(this).val();
          if(val.charAt(0)!='+'){
            swal("Error", "Please write + instead of any other in the start of Phone Number", "error");
            $(this).val('');
          }
   })
  $(document).on("click", ".phone_verify_btn.verify", function(){
    $(this).removeClass("verify");
    var uphone = $.trim($('.phone').val());

    //alert(uphone);
    window.uphone = uphone;
    if(true){
         $.ajax({
            type: "GET",
            url: "{{route('verify-number')}}",
            data: { 
                'phone': $(".phone_input").val(), 
            },
            }).done(function (data) {
                if(data=='ok'){
                    var appVerifier = new firebase.auth.RecaptchaVerifier('phone_verify', { 'size': 'invisible' });
                      $(this).attr("disabled", "disabled");
                      firebase.auth().signInWithPhoneNumber(uphone, appVerifier)
                        .then(function (confirmationResult) {
                          window.confirmationResult = confirmationResult;
                          $(".step_1").hide();
                          $(".step_2").show();
                        }).catch(function (error) {
                          console.error('Error during signInWithPhoneNumber', error);
                        });
                }else{
                    $('.phone_verify_btn').addClass("verify");
                     swal("Error", "Your Phone number is not valid. Please enter valid Phone Number.", "error");
                     
                }
        }); 
      
    } else { swal("Error", "Please enter valid phone number.", "error"); $('.phone_verify_btn').addClass("verify"); }
  })
@endif
  $(document).on("click", ".phone_code_verify_btn", function(){
      var code = $.trim($(".phone_code_input").val());
      if ( code.length > 3 ) {
        confirmationResult.confirm(code).then(result=>{
          $.ajax({
            type: "GET",
            url: "{{route('verification')}}",
            data: { 
                'phone': $(".phone_input").val(), 
            },
            }).done(function (data) {
            window.location.href = data;
        });
        })
        .catch(error=>{ alert(error.message);
        });
      } else { swal("Error", "Please enter a valid verification code.", "error"); }
  })
</script>
@endpush
