@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')
<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('Sitemap')])

<!-- Inner Page Title end -->

<div class="sitemapwraper">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4>HW Search</h4>
				
				<ul class="sitemapnav">
					<li><a href="{{url('/jobs')}}">Jobs</a></li>
					<li><a href="{{ route('blogs') }}">Blog</a></li>
					<li><a href="{{ route('contact.us') }}">Contact Us</a></li>
					{!!get_menus_footer()!!}
					<li><a href="{{ route('faq') }}">{{__('FAQs')}}</a></li>
					
                </ul>
				
			</div>
			
			<div class="col-md-4">
				<h4>Jobseeker</h4>
				
				<ul class="sitemapnav">
                {!!get_menus_seeker()!!}
                    
				@if(!Auth::user() && !Auth::guard('company')->user())
				<li><a href="javascript:void(0)" data-toggle="modal" data-target="#uploadresume">Upload CV</a></li>
				@else
				<li><a href="{{url('my-profile#cvs')}}">Upload CV</a></li>
				@endif	
				
                </ul>
				
			</div>
			
			<div class="col-md-4">
				<h4>Employer</h4>
				
				<ul class="sitemapnav">

                    {!!get_menus_employerr()!!}
                    
                </ul>
			</div>
			
		</div>
	</div>
</div>






@include('includes.footer')

@endsection

@push('scripts') 


@endpush

