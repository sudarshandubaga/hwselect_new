@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end -->

<!-- Inner Page Title start -->

@include('includes.inner_page_title', ['page_title'=>__('My Job Alerts')])

<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <div class="row"> @include('includes.user_dashboard_menu')

            <div class="col-lg-9 col-sm-8">
				<div class="alert jobalert mb-5">

		<span class="email-jobs-inline email-jobs-center signed-out gtmEmailMeJobsInline btn-job-alert">

			
			@if (Request::get('search') != '' || Request::get('location') != '')
			<a class="btn btn-secondary btn-job-alert" href="javascript:;"><i class="far fa-bell"></i> Save Job Alerts </a>
			@else
			<a class="btn btn-secondary btn-job-alert-disabled" disabled href="javascript:;"><i class="far fa-bell"></i> Save Job Alerts</a>
            @endif

		</span>

			Get the latest Job Alerts sent directly to your inbox.

	</div>
				
				
				
				

                <div class="userdashbox">
				
					<div class="row mb-3">
<div class="col-md-6 col-6"> <h3>{{__('My Job Alerts')}}</h3></div>
<div class="col-md-6 col-6"><div class="delsvedjobs">  
				<button class="btn btn-secondary btn-job-alert"><i class="far fa-bell"></i> Add Job Alerts </button>

	
	<button class="btn btn-danger delete"><i class="fas fa-paper-plane" aria-hidden="true"></i> Delete</button>
	
                   </div></div>
</div>

                    @include('flash::message')

                   
						<div class="table-responsive">
						<table class="table table-striped">

						  <tbody>

							<tr>

							  <th scope="col">Alert Title</th>	

								@if(isset($id) && $id!='')

							  <th scope="col">Location</th>

								@endif

								<th scope="col">Created On</th>

							  <th scope="col">Action</th>
                              <th align="center"><input type="checkbox" class="delete_selection" id="selectall" name="selectall" onclick="toggle(this)" autocomplete="off">   Select All</th>

							</tr>							

							 <!-- job start -->

                        @if(isset($alerts) && count($alerts))

                        @foreach($alerts as $alert)

                        <tr id="delete_{{$alert->id}}">

                            @php

                            if(null!==($alert->search_title)){

                            $title = $alert->search_title;

                            }



                            @endphp

                            @php

                            if(null!==($alert->country_id)){

                            $id = $alert->country_id;

                            }



                            if(isset($title) && $title!='' && isset($id) && $id!=''){

                            $cols = 'col-lg-4';

                            }else{

                            $cols = 'col-lg-8';

                            }

                            @endphp

							

							@if(isset($title) && $title!='')

							<td>{{$title}}</td>

							@endif

                                @if(isset($id) && $id!='')

							  <td> {{$id}}</td>

							@endif

							  <td> {{$alert->created_at->format('d-M-Y')}}</td>

							  <td> <a href="javascript:;" data-alert="{{$title}}" id="alert-{{$alert->id}}"  onclick="delete_alert({{$alert->id}})" class="delete_alert">Delete</a></td>							 

                              <td> <input class="checkboxes" type="checkbox" id="check_{{$alert->id}}" name="user_ids[]" autocomplete="off" value="{{$alert->id}}"></td>                           

                        </tr>

                        <!-- job end -->

                        @endforeach

                        @endif 

							  

							  

						  </tbody>

						</table>
						</div>

                </div>

            </div>

        </div>

    </div>


<div class="modal fade" id="show_alert" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="submit_alert">
          @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="registerModalLabel">Receive the latest job vacancies</h5>
        <button style="margin-top: -16px !important; " type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">



                <input type="hidden" name="country_id" value="@if(isset(Request::get('country_id')[0])) {{ Request::get('country_id')[0] }} @endif">



                <input type="hidden" name="state_id" value="@if(isset(Request::get('state_id')[0])){{ Request::get('state_id')[0] }} @endif">



                <input type="hidden" name="city_id" value="@if(isset(Request::get('city_id')[0])){{ Request::get('city_id')[0] }} @endif">



                <input type="hidden" name="functional_area_id" value="@if(isset(Request::get('functional_area_id')[0])){{ Request::get('functional_area_id')[0] }} @endif">
        
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter Job Description keywords</label>
            <input type="text" name="search" class="form-control" id="search"  placeholder="{{__('Example: Project Manager, Line Manager')}}" value="{{ Request::get('search')?Request::get('search'):null }}">
          </div>
          
          <div class="form-group">
                <label for="recipient-name" class="col-form-label">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address"value="@if( Auth::check() ){{Auth::user()->email}}@endif">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </form>
    </div>
  </div>
</div>

	
	
	
	
	
	
	
	
	
	
    @include('includes.footer')

    @endsection

    @push('scripts')
<script>



    //$('.btn-job-alert').on('click', function() {



        //$('#show_alert').modal('show');



    //})



    $(document).ready(function ($) {



        $("#search-job-list").submit(function () {



            $(this).find(":input").filter(function () {



                return !this.value;



            }).attr("disabled", "disabled");



            return true;



        });



        $("#search-job-list").find(":input").prop("disabled", false);







        $(".view_more_ul").each(function () {



            if ($(this).height() > 100)



            {



                $(this).addClass('hide_vm_ul');



                $(this).next().removeClass('hide_vm');



            }



        });



        $('.view_more').on('click', function (e) {



            e.preventDefault();



            $(this).prev().removeClass('hide_vm_ul');



            $(this).addClass('hide_vm');



        });







    });

    function capitalize(str) {
        strVal = '';
        str = str.split(' ');
        for (var chr = 0; chr < str.length; chr++) {
            strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
        }
        return strVal
    }

    if ($("#submit_alert").length > 0) {



    $("#submit_alert").validate({







        rules: {



            email: {



                required: true,



                maxlength: 5000,



                email: true



            },

            search: {
                required: true,
                remote: {
                            type: 'get',
                            url: "/check-alert",
                            data: {
                              search: function() {
                                return $( "#search" ).val();
                              }
                            },  
                            dataType: 'json'
                        },
            }



        },



        messages: {



            email: {



                required: "Email Address is Required",



            },
            search: {



                required: "Keyword is Required",
                remote: "This description already exists in your saved Job Alerts."



            }







        },



        submitHandler: function(form) {



            $.ajaxSetup({



                headers: {



                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')



                }



            });



            $.ajax({



                url: "{{route('subscribe.alert')}}",



                type: "GET",



                data: $('#submit_alert').serialize(),



                success: function(response) {
                    var result = response["msg"].match(/\((.*)\)/);
                    console.log(response);

                    $("#submit_alert").trigger("reset");



                    $('#show_alert').modal('hide');



                    swal({



                        title: "Job Alert: "+capitalize(result[1])+" Successfully Added",



                        text: response["msg"],



                        icon: "success",



                        button: "OK",



                    });

                    //location.reload();



                }



            });



        }



    })



}

$(document).on('click','.swal-button--confirm',function(){
                location.reload();
          });

 toggle = function(source) {
    var checkboxes = document.querySelectorAll('.checkboxes');
    for (var i = 0; i < checkboxes.length; i++) {

    if (checkboxes[i] != source)

    checkboxes[i].checked = source.checked;
    }
    }
    $('.delete').on('click',function(){
        swal({

            title: "Are you sure?",

            text: "You want to delete all saved jobs.",

            icon: "warning",

            buttons: true,

            dangerMode: true

        }).then((deleted) => {
            if(deleted) {
                var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
                    return this.value;
                }).get();
                var ids = checkedVals.join(",");
                $.ajax({
                  method: "GET",
                  url: "{{route('alerts.all.delete')}}",
                  data: { ids: ids}
                })
                .done(function( msg ) {
                    location.reload();
                });
            }
        });
    })

$('.btn-job-alert').on('click', function() {
    @if(Auth::user())
    $('#show_alert').modal('show');
    @else
    swal({
        title: "Save Job Alerts",

        text: "In order to save job alerts you must be registered and logged in",

        icon: "error",

        button: "OK",
});
    @endif

})

   if ($("#new_alert").length > 0) {

    $("#new_alert").validate({



        rules: {

            email: {

                required: true,

                maxlength: 5000,

                email: true

            }

        },

        messages: {

            email: {

                required: "Email Address Required",

            }



        },

        submitHandler: function(form) {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $.ajax({

                url: "{{route('subscribe.alert')}}",

                type: "GET",

                data: $('#new_alert').serialize(),

                success: function(response) {

                    swal({

                        title: "Success",

                        text: response["msg"],

                        icon: "success",

                        button: "OK",

                    });

                }

            });

        }

    })

}

if ($("#submit_alert").length > 0) {

    $("#submit_alert").validate({



        rules: {

            email: {

                required: true,

                maxlength: 5000,

                email: true

            }

        },

        messages: {

            email: {

                required: "Email Address Required",

            }



        },

        submitHandler: function(form) {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $.ajax({

                url: "{{route('subscribe.alert')}}",

                type: "GET",

                data: $('#submit_alert').serialize(),

                success: function(response) {

                    $("#submit_alert").trigger("reset");

                    $('#show_alert').modal('hide');

                    swal({

                        title: "Success",

                        text: response["msg"],

                        icon: "success",

                        button: "OK",

                    });

                }

            });

        }

    })

}



</script>
    <script>

        function delete_alert(id) {

            swal({

                title: "Are you sure?",

                text: "You want to delete this saved jobs.",

                icon: "warning",

                buttons: true,

                dangerMode: true

            }).then((deleted) => {
                if(deleted) {
                    $.ajax({
    
                        type: 'GET',
        
                        url: "{{url('/')}}/delete-alert/" + id,
        
                        success: function(response) {
        
                            if (response["status"] == true) {
        
                                $('#delete_' + id).hide();
        
                                var alertm = $('#alert-'+id).data('alert'); 
        
                                var msg = 'Job alert for '+alertm+' deleted';
        
                                swal({
                                    title: "Success",
                                    text: msg,
                                    icon: "success",
                                    button: "OK",
                                });
                            } else {
        
                                swal({
                                    title: "Already exist",
                                    text: response["msg"],
                                    icon: "error",
                                    button: "OK",
                                });
        
                            }
                        }
        
                    });
                }
            });

        }

    </script>

    @include('includes.immediate_available_btn')

    @endpush