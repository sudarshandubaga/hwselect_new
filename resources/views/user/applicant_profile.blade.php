@extends('layouts.app')

@section('content') 

<!-- Header start --> 

@include('includes.header') 

<!-- Header end --> 

<!-- Inner Page Title start --> 

<div class="pageTitle">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h1 class="page-heading">{{$page_title}}</h1>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadCrumb"></div>
            </div>
        </div>
    </div>
</div>
<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">  

        @include('flash::message')  
       
        @if(Auth::user())
        <div class="row"> @include('includes.user_dashboard_menu')

            <div class="col-lg-9">

        @endif

        <!-- Job Detail start -->

        <div class="row">

            <div class="col-lg-8"> 

				

				<!-- Job Header start -->

        <div class="job-header">

            <div class="jobinfo">

                        <!-- Candidate Info -->

                        <div class="candidateinfo">

                            <div class="userPic">{{$user->printUserImage()}}</div>

                            <div class="title">

                                {{$user->getName()}}

                                <?php /*?>@if((bool)$user->is_immediate_available)
                                <sup style="font-size:12px; color:#090;">{{__('Immediate Available For Work')}}</sup>
                                @endif<?php */?>

                            </div>

                            <div class="desi">{{$user->getLocation()}}</div>

                            <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> {{__('Member Since')}}, {{$user->created_at->format('d-M-Y')}}</div>

                            

                            <div class="clearfix"></div>

                        </div>
            </div>
            

        </div>

				

				

				

				

                <!-- About Employee start -->

                <div class="job-header">

                    <div class="contentbox">

                        <h3>{{__('Summary')}}</h3>

                        <p>{{$user->getProfileSummary('summary')}}</p>

                    </div>

                </div>



                <!-- Education start -->

                <div class="job-header" style="display: none;">

                    <div class="contentbox">

                        <h3>{{__('Education')}}</h3>

                        <div class="" id="education_div"></div>            

                    </div>

                </div>



                <!-- Experience start -->

                <div class="job-header" style="display: none;">

                    <div class="contentbox">

                        <h3>{{__('Experience')}}</h3>

                        <div class="" id="experience_div"></div>            

                    </div>

                </div>



                <!-- Portfolio start -->

                <div class="job-header" style="display: none;">

                    <div class="contentbox">

                        <h3>{{__('Portfolio')}}</h3>

                        <div class="" id="projects_div"></div>            

                    </div>

                </div>

            </div>

            <div class="col-lg-4"> 

				

				 

				

				

                <!-- Candidate Detail start -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Personal Details')}}</h3>

                        <ul class="jbdetail">



                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Eligible to work in the UK:')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->eligible_uk)? 'Yes':'No'}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Eligible to work in the EU:')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->eligible_eu)? 'Yes':'No'}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Available Immediately')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{((bool)$user->is_immediate_available)? 'Yes':'No'}}</span>
                                @if(!(bool)$user->is_immediate_available)
                                <br><span style="white-space: nowrap">({{null!==(Auth::user()->immediate_date_from)?date('d-M-Y',strtotime(Auth::user()->immediate_date_from)):date('d-M-Y')}})</span>
                                @endif
                            </div>

                            </li>



                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Age')}}</div>

                                <div class="col-md-6 col-xs-6">
                                    <span>
                                        @if(!empty($user->getAge()))
                                            {{$user->getAge()}} Years
                                        @else
                                            {{ '-' }}
                                        @endif
                                    </span>
                                </div>

                            </li>

                            <li class="row" style="display: none;">

                                <div class="col-md-6 col-xs-6">{{__('Gender')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getGender('gender')}}</span></div>

                            </li>

                            <li class="row" style="display: none;">

                                <div class="col-md-6 col-xs-6">{{__('Marital Status')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getMaritalStatus('marital_status')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Experience')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getJobExperience('job_experience')}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Career Level')}}</div>

                                <div class="col-md-6 col-xs-6"><span>{{$user->getCareerLevel('career_level')}}</span></div>

                            </li>             

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Current Salary')}}</div>

                                <div class="col-md-6 col-xs-6"><span class="permanent">{{$user->salary_currency}} {{$user->current_salary}}</span></div>

                            </li>

                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Expected Salary')}}</div>

                                <div class="col-md-6 col-xs-6"><span class="freelance">{{$user->salary_currency}} {{$user->expected_salary}}</span></div>

                            </li>  
							<?php /*?>@if($profileCv->title!=='')
                            <li class="row">

                                <div class="col-md-6 col-xs-6">{{__('Default Cv')}}</div>
                                <div class="col-md-6 col-xs-6"><a href="{{asset('cvs/'.$profileCv->cv_file)}}"><span class="freelance">{{$profileCv->title}}</span></a></div>

                            </li>  
                            @endif    <?php */?>        

                        </ul>

                    </div>

                </div>



                <!-- Google Map start -->

                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Skills')}}</h3>

                        <div id="skill_div"></div>            

                    </div>

                </div>



                <div class="job-header">

                    <div class="jobdetail">

                        <h3>{{__('Languages')}}</h3>

                        <div id="language_div"></div>            

                    </div>

                </div>

               

            </div>

        </div>

        @if(Auth::user())
</div>
        </div>
                @endif
        

    </div>

</div>

<div class="modal fade" id="sendmessage" role="dialog">

    <div class="modal-dialog">



        <!-- Modal content-->

        <div class="modal-content">

            <form action="" id="send-form">

                @csrf

                <input type="hidden" name="seeker_id" id="seeker_id" value="{{$user->id}}">

                <div class="modal-header">                    

                    <h4 class="modal-title">Send Message</h4>

					<button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">

                    <div class="form-group">

                        <textarea class="form-control" name="message" id="message" cols="10" rows="7"></textarea>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>

            </form>

        </div>



    </div>

</div>

@include('includes.footer')

@endsection

@push('styles')

<style type="text/css">

    .formrow iframe {

        height: 78px;

    }

</style>

@endpush

@push('scripts') 

<script type="text/javascript">

    $(document).ready(function () {

    $(document).on('click', '#send_applicant_message', function () {

    var postData = $('#send-applicant-message-form').serialize();

    $.ajax({

    type: 'POST',

            url: "{{ route('contact.applicant.message.send') }}",

            data: postData,

            //dataType: 'json',

            success: function (data)

            {

            response = JSON.parse(data);

            var res = response.success;

            if (res == 'success')

            {

            var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';

            $('#alert_messages').html(errorString);

            $('#send-applicant-message-form').hide('slow');

            $(document).scrollTo('.alert', 2000);

            } else

            {

            var errorString = '<div class="alert alert-danger" role="alert"><ul>';

            response = JSON.parse(data);

            $.each(response, function (index, value)

            {

            errorString += '<li>' + value + '</li>';

            });

            errorString += '</ul></div>';

            $('#alert_messages').html(errorString);

            $(document).scrollTo('.alert', 2000);

            }

            },

    });

    });

    showEducation();

    showProjects();

    showExperience();

    showSkills();

    showLanguages();

    });

    function showProjects()

    {

    $.post("{{ route('show.applicant.profile.projects', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#projects_div').html(response);

            });

    }

    function showExperience()

    {

    $.post("{{ route('show.applicant.profile.experience', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#experience_div').html(response);

            });

    }

    function showEducation()

    {

    $.post("{{ route('show.applicant.profile.education', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#education_div').html(response);

            });

    }

    function showLanguages()

    {

    $.post("{{ route('show.applicant.profile.languages', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

            $('#language_div').html(response);

            });

    }

    function showSkills()

    {

    $.post("{{ route('show.applicant.profile.skills', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})

            .done(function (response) {

                $('#skill_div').html(response);

            });

    }



    function send_message() {

        const el = document.createElement('div')

        el.innerHTML = "Please <a class='btn' href='{{route('login')}}' onclick='set_session()'>log in</a> as a Employer and try again."

        @if(null!==(Auth::guard('company')->user()))

        $('#sendmessage').modal('show');

        @else

        swal({

            title: "You are not Loged in",

            content: el,

            icon: "error",

            button: "OK",

        });

        @endif

    }

    if ($("#send-form").length > 0) {

        $("#send-form").validate({

            validateHiddenInputs: true,

            ignore: "",



            rules: {

                message: {

                    required: true,

                    maxlength: 5000

                },

            },

            messages: {



                message: {

                    required: "Message is required",

                }



            },

            submitHandler: function(form) {

                $.ajaxSetup({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                @if(null !== (Auth::guard('company')->user()))

                $.ajax({

                    url: "{{route('submit-message-seeker')}}",

                    type: "POST",

                    data: $('#send-form').serialize(),

                    success: function(response) {

                        $("#send-form").trigger("reset");

                        $('#sendmessage').modal('hide');

                        swal({

                            title: "Success",

                            text: response["msg"],

                            icon: "success",

                            button: "OK",

                        });

                    }

                });

                @endif

            }

        })

    }

</script> 

@endpush