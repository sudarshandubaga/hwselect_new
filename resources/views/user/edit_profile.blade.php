@extends('layouts.app')
@push('styles')
<link href="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin_assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
<style type="text/css">
    .error{color: red !important}
</style>
@endpush
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('My Jobseeker Profile')]) 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">
            @include('includes.user_dashboard_menu')

            <div class="col-md-9 col-sm-8"> 
              
                        <div class="userccount">
                            <div class="formpanel mt0"> @include('flash::message') 
                                <!-- Personal Information -->
                                @include('user.inc.profile')                              
                            </div>
                        </div>
						
						
						
						 <div class="userccount" id="cvs">
                            <div class="formpanel mt0">
                                <!-- Personal Information -->
                                <div  >
                                @include('user.forms.cv.cvs')</div>
                                <div style="display: none;">@include('user.forms.project.projects')</div>
                                
                                <div style="display: none;">@include('user.forms.experience.experience')</div>
                               <div style="display: none;"> @include('user.forms.education.education')</div>
                                @include('user.forms.skill.skills')
                                @include('user.forms.language.languages')
                            </div>
                        </div>
            </div>
        </div>
    </div>  
</div>
@include('includes.footer')
@endsection
@push('styles')
<style type="text/css">
    .userccount p{ text-align:left !important;}
</style>
@endpush
@push('scripts')

@include('includes.immediate_available_btn')
<script src="{{asset('admin_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endpush