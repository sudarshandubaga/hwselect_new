<div class="modal-body">
    <div class="form-body">
        @if(isset($profileSkill))
        <div class="formrow" id="div_job_skill_id">
                <?php
                    $job_skill_id = (isset($profileSkill) ? $profileSkill->job_skill_id : null);
                ?>
                {!! Form::select('job_skill_id', [''=>__('Select skill')]+$jobSkills, $job_skill_id, array('class'=>'form-control', 'id'=>'job_skill_id')) !!} 

                <span class="help-block job_skill_id-error"></span> 
        </div>
        @else
        <div class="row">
            <div class="col-md-10">

                <div class="formrow" id="div_job_skill_id">
                    <?php
                        $job_skill_id = (isset($profileSkill) ? $profileSkill->job_skill_id : null);
                    ?>
                    {!! Form::select('job_skill_id', [''=>__('Select skill')]+$jobSkills, $job_skill_id, array('class'=>'form-control', 'id'=>'job_skill_id')) !!} 

                    <span class="help-block job_skill_id-error"></span> 
            </div>
        </div>
            <div class="col-md-2">
                <a href="javascript:;" style="font-size: 11px;" class="btn btn-primary add-new-skill">Add New Skill</a>
            </div>
        </div>
        @endif
        
        
        <div class="formrow" id="div_add_new_skill" style="display: none;">
            <input type="text" name="add_new_skill" id="add_new_skill" maxlength="50" class="form-control" placeholder="New Job Skill">

             <span class="help-block add_new_skill-error"></span> 
         </div>

        <div class="formrow" id="div_job_experience_id">
            <?php
            $job_experience_id = (isset($profileSkill) ? $profileSkill->job_experience_id : null);
            ?>
            {!! Form::select('job_experience_id', [''=>__('Select Length of Experience')]+$jobExperiences, $job_experience_id, array('class'=>'form-control', 'id'=>'job_experience_id')) !!} <span class="help-block job_experience_id-error"></span> </div>
    </div>
</div>

