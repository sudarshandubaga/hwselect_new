<h5>{{__('Benefits of Adding a summary to your profile')}}</h5>

<p>A summary is a statement of between two or three sentences, giving a professional introduction highlighting your most valuable skills, experiences and assets.</p>
<p>A CV summary can help employers quickly learn whether you have the skills and background they require.</p>
<p id="limit_message">A maximum of 1000 characters including spaces can be input, approximately 170 words</p>
<div class="row">
    <div class="col-md-12">
        <form class="form" id="add_edit_profile_summary" method="POST" action="{{ route('update.front.profile.summary', [$user->id]) }}">
            {{ csrf_field() }}
            <div class="form-body">
                <div id="success_msg"></div>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'summary') !!}">
                    <textarea name="summary" class="form-control" id="summary" placeholder="{{__('Profile Summary')}}" maxlength="1000">{{ old('summary', $user->getProfileSummary('summary')) }}</textarea>
                    <span class="help-block summary-error"></span> </div>
                <button type="button" class="btn btn-large btn-primary" onClick="submitProfileSummaryForm();">{{__('Update Summary')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
            </div>
        </form>
    </div>
</div>
@push('scripts') 
<script type="text/javascript">
    $('#summary').on('keyup blur',function(){
        if($(this).val().length>170){
            $('#limit_message').css("color","red");
        }else{
            $('#limit_message').css("color","");
        }
    })
    function submitProfileSummaryForm() {
        var form = $('#add_edit_profile_summary');
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            dataType: 'json',
            success: function (json) {
                $("#success_msg").html("<span class='text text-success'>{{__('Summary updated successfully')}}</span>");
            },
            error: function (json) {
                if (json.status === 422) {
                    var resJSON = json.responseJSON;
                    $('.help-block').html('');
                    $.each(resJSON.errors, function (key, value) {
                        $('.' + key + '-error').html('<strong>' + value + '</strong>');
                        $('#div_' + key).addClass('has-error');
                    });
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }
            }
        });
    }
</script> 
@endpush            