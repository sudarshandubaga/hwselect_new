@extends('admin.layouts.email_template')

@section('content')

@php

if(!isset($user->first_name) && null==($user->first_name)){

$link = route('company.email-verification.check', $user->verification_token);

}elseif(null!==($user->first_name)){

$link = route('email-verification.check', $user->verification_token);

}

@endphp

<table border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 100%;    border-bottom: solid 1px #ccc;">

    <tr>

        <td class="content-wrapper" style="padding-left:24px;padding-right:24px"><br>

            <div class="title" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px;font-weight:400;color: #000;text-align: left; padding-top: 20px;">Hello {{ $user->name }}</div></td>
	
    </tr>
	
	<tr>

        <td class="content-wrapper" style="padding-left:24px;padding-right:24px">
            <div class="title" style="font-family: Helvetica, Arial, sans-serif; font-size: 14px;font-weight:400;color: #000;text-align: left; padding-top: 20px;">
                Date of Registration {{date('d-M-Y')}}<br>
                Thank you for registering an account with {{ $siteSetting->site_name }}
            </div>
        </td>

    </tr>

    <tr>

        <td class="cols-wrapper" style="padding-left:12px;padding-right:12px">

            <!--[if mso]>

             <table border="0" width="576" cellpadding="0" cellspacing="0" style="width: 576px;">

                <tr>

                   <td width="192" style="width: 192px;" valign="top">

                      <![endif]-->

            <table border="0" cellpadding="0" cellspacing="0" align="left" class="force-row" style="width: 100%;">

                <tr>

                    <td class="row" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">

                            <tr>

                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#333;padding-bottom:30px; text-align: left;">
                                <a href="{{ $link . '?email=' . urlencode($user->email) }}" style="color: #fff;text-decoration: none;background: #f25a55; padding:10px 20px;text-align: center;display: inline-block;margin-top: 10px; border-radius: 5px; display: inline-block;">Click here to verify your account</a>
                            </td>

                            </tr>
							
							<tr>

                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#333;white-space: normal;padding-bottom:30px; text-align: left;">
                                    If the above link doesn’t work, copy and paste this URL into your web browser.
                                    <br> {{ $link . '?email=' . urlencode($user->email) }}
                                    <br><br>
                                    The URL will expire in 24 hours for security reasons. If you didn’t make this request, simply ignore this message.<br>
                                    @if($siteSetting->is_newsletter_active==1)
                                        @if($user->is_subscribed)
                                            <strong>During the registration process you accepted to receive our newsletter, you may unsubscribe from the newsletter at any time from within your account profile.</strong><br>
                                        @endif 
                                    @endif 
                                    If you have any questions please feel free in contacting us on <strong>{{ $siteSetting->site_phone_primary }}</strong> or through our contact page on the website
								</td>

                            </tr>

                            <tr>

                                <td style="font-family: Helvetica, Arial, sans-serif;font-size: 14px;line-height: 22px;font-weight: 400;color: #333; padding-bottom: 0;text-align: left;">Thanks, <br>

                                    {{ $siteSetting->site_name }} Team</td>

                            </tr>

                        </table>

                     </td>

                </tr>

            </table>      

            <!--[if mso]>

               </td>

            </tr>

         </table>

         <![endif]--></td>

    </tr>

</table>

@endsection

