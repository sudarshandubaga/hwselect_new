@extends('layouts.app')

@section('content')

<!-- Header start -->

@include('includes.header')

<!-- Header end --> 

<!-- Search start -->

@include('includes.search')

<!-- Search End --> 

<!-- Popular Searches start -->

@include('includes.popular_searches')

<!-- Popular Searches ends --> 

<!-- Featured Jobs start -->

@include('includes.featured_jobs')

<!-- Featured Jobs ends -->


<!-- testimonials start -->
@if($siteSetting->is_testimonial_active)
@include('includes.testimonials')
@endif

<!-- testimonials Ends -->

<!-- How it Works start -->

@include('includes.how_it_works')

<!-- How it Works Ends -->

<!-- Testimonials start -->
@if($siteSetting->is_blog_active)
@include('includes.home_blogs')
@endif

<!-- Testimonials End -->

@include('includes.footer')

@endsection

@push('scripts') 

<script>

    $(document).ready(function ($) {

        $("form").submit(function () {

            $(this).find(":input").filter(function () {

                return !this.value;

            }).attr("disabled", "disabled");

            return true;

        });

        $("form").find(":input").prop("disabled", false);

    });

</script>
<script>
  $( function() {

    var mySource  = {!!$skills!!};
    $( "#jbsearch" ).autocomplete({
    minLength: 1,
    source: function(req, resp) {
      var q = req.term;
      var myResponse = [];
      $.each(mySource, function(key, item) {
        if (item.toLowerCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.toUpperCase().indexOf(q) === 0) {
          myResponse.push(item);
        }else if (item.indexOf(q) === 0){
          myResponse.push(item);
        }
      });
      resp(myResponse);
    },
    
  });
    });
  </script>	

@include('includes.country_state_city_js')
<script type="text/javascript">
  (function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);
        return this.each(function() {
            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);
                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
})(jQuery);
  $(function() {
    $('#job_detail').checkFileType({
        allowedExtensions: ['pdf','doc','docx'],
        error: function() {
            
            $('#job_detail').val('');
            alert('File Formats Allowed: .pdf, .doc, .docx');
        }
    });
});
</script>

@endpush

