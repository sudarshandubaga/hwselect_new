<?php



$all_users = ['allowed_roles' => ['SUP_ADM', 'SUB_ADM']];

$sup_only = ['allowed_roles' => 'SUP_ADM'];

Route::get('/home', array_merge(['uses' => 'Admin\HomeController@index'], $all_users))->name('admin.home');

Route::post('tinymce-image_upload', array_merge(['uses' => 'Admin\TinyMceController@uploadImage'], $all_users))->name('tinymce.image_upload');

/* * ********************************* */

$real_path = realpath(__DIR__) . DIRECTORY_SEPARATOR . 'admin_routes' . DIRECTORY_SEPARATOR;

Route::get('list-contact-enquiries', array_merge(['uses' => 'Admin\AdminController@indexContactEnquiries'], $sup_only))->name('list.admin.contact');

Route::delete('list-contact-enquiries', array_merge(['uses' => 'Admin\AdminController@multipleDeletesContactEnquiries'], $sup_only))->name('list.admin.contact');

include_once($real_path . 'admin_user.php');

include_once($real_path . 'site_user.php');

include_once($real_path . 'faq.php');

include_once($real_path . 'seo.php');

include_once($real_path . 'cms.php');

include_once($real_path . 'site_setting.php');

include_once($real_path . 'career_level.php');

include_once($real_path . 'country.php');

include_once($real_path . 'country_detail.php');

include_once($real_path . 'functional_area.php');

include_once($real_path . 'gender.php');

include_once($real_path . 'bonus.php');

include_once($real_path . 'benifits.php');

include_once($real_path . 'industry.php');

include_once($real_path . 'job_experience.php');

include_once($real_path . 'job_skill.php');

include_once($real_path . 'job_title.php');

include_once($real_path . 'job_type.php');

include_once($real_path . 'job_shift.php');

include_once($real_path . 'degree_level.php');

include_once($real_path . 'degree_type.php');

include_once($real_path . 'major_subject.php');

include_once($real_path . 'language.php');

include_once($real_path . 'state.php');

include_once($real_path . 'city.php');

include_once($real_path . 'result_type.php');

include_once($real_path . 'language_level.php');

include_once($real_path . 'marital_status.php');

include_once($real_path . 'company.php');

include_once($real_path . 'ownership_type.php');

include_once($real_path . 'job.php');

include_once($real_path . 'salary_period.php');

include_once($real_path . 'package.php');

include_once($real_path . 'video.php');

include_once($real_path . 'testimonial.php');

include_once($real_path . 'slider.php');

include_once($real_path . 'newsletter.php');



Route::group(['namespace' => 'Admin'], function () {

    Route::get('/blog_category', 'Blog_categoriesController@index');

    Route::post('/blog_category/create', 'Blog_categoriesController@create');

    Route::post('/blog_category', 'Blog_categoriesController@update');

    Route::delete('/blog_category/{blog_category}', 'Blog_categoriesController@destroy');

    Route::get('/blog_category/get_blog_category_by_id/{blog_category}', 'Blog_categoriesController@get_blog_category_by_id');



    Route::get('/blog', 'BlogsController@index')->name('blog');

    Route::get('/blog/add-new-blog', 'BlogsController@show_form')->name('add-new-blog');

    Route::post('/blog/create', 'BlogsController@create');

    Route::post('/blog/update', 'BlogsController@update');

    Route::delete('/blog/{blog}', 'BlogsController@destroy');
    Route::get('/delete-blog/{blog}', 'BlogsController@destroyBlog')->name('delete-blog');

    Route::get('/blog/remove_blog_feature_image/{blog}', 'BlogsController@remove_blog_feature_image');

    Route::get('/blog/get_blog_by_id/{blog}', 'BlogsController@get_blog_by_id');

    Route::get('/blog/edit-blog/{blog}', 'BlogsController@get_blog')->name('edit-blog');





    /*widgets*/

    Route::get('/widget', 'WidgetsController@index')->name('widget');

    Route::get("/add-widget", function () {

        return View::make("admin.widgets.add");

    })->name('add-widget');

    Route::get('/edit-widget/{widget}', 'WidgetsController@edit_widget')->name('edit-widget');

    Route::post('/widget/create', 'WidgetsController@create')->name('create.widget');

    Route::post('/widget', 'WidgetsController@update')->name('update.widget');

    Route::delete('/widget/{widget}', 'WidgetsController@destroy')->name('destroy.widget');

    Route::get('/widget/get_widget_by_id/{widget}', 'WidgetsController@get_widget_by_id');





    Route::get('/menu', 'MenuController@getIndex')->name('footer-menus');

    Route::post('/menu/post_index','MenuController@post_index');

    Route::post('/menu/new', 'MenuController@postNew');

    Route::delete('/menu/{id}', 'MenuController@destroy');

    Route::get('/menu/edit/{id}', 'MenuController@getEdit');

    Route::post('/menu/edit/', 'MenuController@postEdit');



    /*Modules*/
    Route::get('/modules', 'ModulesController@index')->name('modules');
    Route::get('/module/add-module', 'ModulesController@add_module')->name('add-module');
    Route::post('/module/post-module', 'ModulesController@post_module')->name('post-module');
    Route::get('/module/edit-module/{module}', 'ModulesController@edit_module')->name('edit-module');
    Route::get('/module/delete/{module}', 'ModulesController@destroy')->name('delete-modules');


    Route::get('ips', 'Modules_dataController@ips');
    Route::get('fetch-ips', 'Modules_dataController@fetchIps')->name('fetch.data.ips');
    Route::get('/{module}', 'Modules_dataController@index')->name('modules-data');
    Route::get('/newsletter/subscribers', 'NewsletterController@subscriber')->name('newsletter.subscribers');
    Route::get('/newsletter/subscribers/get', 'NewsletterController@fetchSubscriber')->name('newsletter.subscribers.fetch');
    Route::get('/cms-module/{module}', 'Modules_dataController@add_module_data')->name('add-module-data');
    Route::post('/store-module-data', 'Modules_dataController@post_data')->name('store-module-data');
    Route::get('/edit-module-data/{module}', 'Modules_dataController@edit_data')->name('edit-module-data');
    Route::post('/update-module-data', 'Modules_dataController@update_data')->name('update-module-data');
    Route::get('/delete-data/{module}', 'Modules_dataController@destroy')->name('delete-data');
    Route::get('/data-status/{module}/{status}', 'Modules_dataController@update_status');

    
    
});


Route::get('/admin/{any}','Admin\HomeController@index');
