<?php

/* * ******  Company Start ********** */
Route::get('list-companies', array_merge(['uses' => 'Admin\CompanyController@indexCompanies'], $all_users))->name('list.companies');
Route::get('create-company', array_merge(['uses' => 'Admin\CompanyController@createCompany'], $all_users))->name('create.company');
Route::get('delete-companies', array_merge(['uses' => 'Admin\CompanyController@deleteCompanies'], $all_users))->name('delete.companies');
Route::post('register-company', array_merge(['uses' => 'Admin\CompanyController@register'], $all_users))->name('company.job.register');
Route::post('store-company', array_merge(['uses' => 'Admin\CompanyController@storeCompany'], $all_users))->name('store.company');
Route::get('edit-company/{id}', array_merge(['uses' => 'Admin\CompanyController@editCompany'], $all_users))->name('edit.company');
Route::put('update-company/{id}', array_merge(['uses' => 'Admin\CompanyController@updateCompany'], $all_users))->name('update.company');
Route::delete('delete-company', array_merge(['uses' => 'Admin\CompanyController@deleteCompany'], $all_users))->name('delete.company');
Route::get('fetch-companies', array_merge(['uses' => 'Admin\CompanyController@fetchCompaniesData'], $all_users))->name('fetch.data.companies');
Route::put('make-active-company', array_merge(['uses' => 'Admin\CompanyController@makeActiveCompany'], $all_users))->name('make.active.company');
Route::put('make-not-active-company', array_merge(['uses' => 'Admin\CompanyController@makeNotActiveCompany'], $all_users))->name('make.not.active.company');
Route::put('make-featured-company', array_merge(['uses' => 'Admin\CompanyController@makeFeaturedCompany'], $all_users))->name('make.featured.company');
Route::put('make-not-featured-company', array_merge(['uses' => 'Admin\CompanyController@makeNotFeaturedCompany'], $all_users))->name('make.not.featured.company');
/* * ****** End Company ********** */




Route::get('short-listed-users/{id}', 'Admin\CompanyController@unlocked_users')->name('admin.company.unlock-users');

Route::get('short-listed-seekers/{id}/{status}', 'Admin\CompanyController@unlocked_users')->name('admin.company.unloced-users');

Route::get('short-list', 'Admin\CompanyController@unlock')->name('admin.company.unlock');

Route::get('show-hide', 'Admin\CompanyController@show_hide')->name('admin.company.show_hide');

Route::get('list-applied-users/{job_id}', 'Admin\CompanyController@listAppliedUsers')->name('admin.list.applied.users');

Route::get('short-list-by-future-job', 'Admin\CompanyController@unlock_by_future_job')->name('admin.company.unlock.future');

Route::get('short-list-lock', 'Admin\CompanyController@lock')->name('admin.company.lock');





Route::post('submit-action-seeker', 'Admin\CompanyController@submitnew_action_seeker')->name('admin.submit-action-seeker');

Route::get('view-action-seeker', 'Admin\CompanyController@view_action_seeker')->name('admin.view-action-seeker');


Route::get('view-notes', 'Admin\CompanyController@view_notes')->name('admin.view-notes');