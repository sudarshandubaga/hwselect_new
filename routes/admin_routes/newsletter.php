<?php

/* * ******  Testimonial Start ********** */
Route::get('send-newsletter/{id}', array_merge(['uses' => 'Admin\NewsletterController@sendNewsLetter'], $all_users))->name('send-newsletter');
Route::post('store-newsletter', array_merge(['uses' => 'Admin\NewsletterController@storeNewsletter'], $all_users))->name('store.newsletter');

/* * ****** End Testimonial ********** */