<?php



Route::get('check-email', 'AjaxController@checkEmail')->name('check-email');
Route::get('check-phone', 'AjaxController@checkPhone')->name('check-phone');
Route::get('check-career_level', 'AjaxController@career_level')->name('career_level');
Route::get('check-functional_area', 'AjaxController@functional_area')->name('functional_area');
Route::get('check-job_type', 'AjaxController@job_type')->name('job_type');
Route::get('check-job_shift', 'AjaxController@job_shift')->name('job_shift');
Route::get('check-degree_level', 'AjaxController@degree_level')->name('degree_level');
Route::get('check-job_experience', 'AjaxController@job_experience')->name('job_experience');
Route::get('check-employer_name', 'AjaxController@employer_name')->name('employer_name');
Route::get('check-employer_email', 'AjaxController@employer_email')->name('employer_email');
Route::get('check-salary-period', 'AjaxController@checkSalaryPeriod')->name('check-salary-period');

Route::get('check-jobskill', 'AjaxController@checkSkill')->name('check-jobskill');


Route::post('store-bonus', 'AjaxController@storeBonusForm')->name('store-bonus');
Route::get('check-bonus', 'AjaxController@checkBonus')->name('check-bonus');
Route::get('check-alert', 'AjaxController@checkAlert')->name('check-alert');

Route::post('store-benifits', 'AjaxController@storeBenifitsForm')->name('store-benifits');
Route::get('check-benifits', 'AjaxController@checkBenifits')->name('check-benifits');

Route::post('filter-default-cities-dropdown', 'AjaxController@filterDefaultCities')->name('filter.default.cities.dropdown');


Route::post('filter-default-cities-dropdown', 'AjaxController@filterDefaultCities')->name('filter.default.cities.dropdown');

Route::post('filter-default-status-dropdown', 'AjaxController@filterDefaultStatus')->name('filter.default.status.dropdown');

Route::post('filter-default-jobs-dropdown', 'AjaxController@filterDefaultJobs')->name('filter.default.jobs.dropdown');


Route::post('filter-default-companies-dropdown', 'AjaxController@filterDefaultCompanies')->name('filter.default.companies.dropdown.all');

Route::post('filter-recruitment-jobs-dropdown', 'AjaxController@filterRecruitmentJobs')->name('filter.recruitment.jobs.dropdown');

Route::post('filter-default-jobs-dropdown-all', 'AjaxController@filterDefaultJobsAll')->name('filter.default.jobs.dropdown.all');

Route::post('filter-default-states-dropdown', 'AjaxController@filterDefaultStates')->name('filter.default.states.dropdown');

Route::post('filter-lang-cities-dropdown', 'AjaxController@filterLangCities')->name('filter.lang.cities.dropdown');

Route::post('filter-lang-states-dropdown', 'AjaxController@filterLangStates')->name('filter.lang.states.dropdown');

Route::post('filter-cities-dropdown', 'AjaxController@filterCities')->name('filter.cities.dropdown');

Route::post('filter-states-dropdown', 'AjaxController@filterStates')->name('filter.states.dropdown');

Route::post('filter-degree-types-dropdown', 'AjaxController@filterDegreeTypes')->name('filter.degree.types.dropdown');

Route::get('check-lock-unlock', 'AjaxController@checkLockUnloc')->name('check-lock-unlock');

