<?php



Route::get('save-emp-form', 'Company\Auth\RegisterController@saveForm')->name('refresh-form-emp');

Route::get('admin/public-company/{id}','AjaxController@companyProfile')->name('public.company');

Route::get('admin/job/{id}','AjaxController@jobDetail')->name('public.job');

Route::get('company-home', 'Company\CompanyController@index')->name('company.home');

Route::get('companies', 'Company\CompaniesController@company_listing')->name('company.listing');

Route::post('submit-action-request-company', 'Company\CompanyController@submitActionRequestUser')->name('submit-action-request-company');

Route::get('company-profile', 'Company\CompanyController@companyProfile')->name('company.profile');

Route::put('update-company-profile', 'Company\CompanyController@updateCompanyProfile')->name('update.company.profile');
Route::put('update-company-image', 'Company\CompanyController@updateCompanyImage')->name('update.company.image');

Route::get('company/change-password', 'Company\CompanyController@showChangePasswordForm')->name('company.change-passwords');
Route::post('company/change-Password','Company\CompanyController@changePassword')->name('company.changePassword');

Route::post('update-twoStep-verification-status', 'Company\CompanyController@updateTwoStepVerificationStatus')->name('update.twoStep.verification.status');

Route::get('posted-jobs', 'Company\CompanyController@postedJobs')->name('posted.jobs');
Route::get('refused-jobs', 'Company\CompanyController@rejectedJobs')->name('rejected.jobs');

Route::get('company/{slug}', 'Company\CompanyController@companyDetail')->name('company.detail')->middleware('company');

Route::post('contact-company-message-send', 'Company\CompanyController@sendContactForm')->name('contact.company.message.send');

Route::post('contact-applicant-message-send', 'Company\CompanyController@sendApplicantContactForm')->name('contact.applicant.message.send');

Route::get('list-applied-users/{job_id}', 'Company\CompanyController@listAppliedUsers')->name('list.applied.users');

Route::get('list-favourite-applied-users/{job_id}', 'Company\CompanyController@listFavouriteAppliedUsers')->name('list.favourite.applied.users');

Route::get('add-to-favourite-applicant/{application_id}/{user_id}/{job_id}/{company_id}', 'Company\CompanyController@addToFavouriteApplicant')->name('add.to.favourite.applicant');

Route::get('remove-from-favourite-applicant/{application_id}/{user_id}/{job_id}/{company_id}', 'Company\CompanyController@removeFromFavouriteApplicant')->name('remove.from.favourite.applicant');

Route::get('applicant-profile/{application_id}', 'Company\CompanyController@applicantProfile')->name('applicant.profile');

Route::get('user-profile/{id}', 'Company\CompanyController@userProfile')->name('user.profile');

Route::get('company-followers', 'Company\CompanyController@companyFollowers')->name('company.followers');

/* Route::get('company-messages', 'Company\CompanyController@companyMessages')->name('company.messages'); */

Route::post('submit-message-seeker', 'CompanyMessagesController@submitnew_message_seeker')->name('submit-message-seeker');



Route::get('company-messages', 'CompanyMessagesController@all_messages')->name('company.messages');

Route::get('append-messages', 'CompanyMessagesController@append_messages')->name('append-message');

Route::get('append-only-messages', 'CompanyMessagesController@appendonly_messages')->name('append-only-message');

Route::post('company-submit-messages', 'CompanyMessagesController@submit_message')->name('company.submit-message');

Route::get('company-message-detail/{id}', 'Company\CompanyController@companyMessageDetail')->name('company.message.detail');



Route::post('submit-action-seeker', 'CompanyMessagesController@submitnew_action_seeker')->name('submit-action-seeker');

Route::get('view-action-seeker', 'CompanyMessagesController@view_action_seeker')->name('view-action-seeker');

Route::get('short-listed-seekers/{id}/{status}', 'Company\CompanyController@unlocked_users')->name('company.unloced-users');

Route::get('short-list', 'Company\CompanyController@unlock')->name('company.unlock');

Route::get('make-unshort', 'Company\CompanyController@lock')->name('company.lock');


Route::get('settings', 'Company\CompanyController@settings')->name('company.settings');
Route::get('evaluate-questions', 'Company\CompanyController@evaluateQuestions')->name('company.evaluate-questions');
Route::get('invite-candidates', 'Company\CompanyController@inviteCandidates')->name('company.invite-candidates');
Route::get('assesment-results', 'Company\CompanyController@assesmentResults')->name('company.assesment-results');
Route::get('overview', 'Company\CompanyController@overview')->name('company.overview');

