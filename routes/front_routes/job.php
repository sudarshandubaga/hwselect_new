<?php

Route::get('jobs-featured', 'Job\JobController@jobsByFeatured')->name('job.featuredJobs');

Route::post('apply-to-admin', 'Job\JobController@applyToAdmin')->name('apply-to-admin');

Route::get('jobs/{category}/{slug}', 'Job\JobController@jobDetail')->name('job.detail');

Route::get('apply/{slug}', 'Job\JobController@applyJob')->name('apply.job');

Route::get('delete-apply/{id}/{slug}', 'Job\JobController@deleteapplyJob')->name('job_apply.delete');

Route::get('delete-all-apply', 'Job\JobController@deleteAllapplyJob')->name('job_apply.all.delete');

Route::post('apply/{slug}', 'Job\JobController@postApplyJob')->name('post.apply.job');



Route::get('job/{category}/{fun_id}', 'Job\JobController@jobsByCategory')->name('job.category');

Route::get('add-to-favourite-job', 'Job\JobController@addToFavouriteJob')->name('add.to.favourite');

Route::get('remove-from-favourite-job', 'Job\JobController@removeFromFavouriteJob')->name('remove.from.favourite');
Route::get('remove-all-from-favourite-job', 'Job\JobController@removeAllFromFavouriteJob')->name('remove.all.from.favourite');

Route::get('my-job-applications', 'Job\JobController@myJobApplications')->name('my.job.applications');

Route::get('my-favourite-jobs', 'Job\JobController@myFavouriteJobs')->name('my.favourite.jobs');

Route::get('post-job', 'Job\JobPublishController@createFrontJob')->name('post.job');

Route::post('store-front-job', 'Job\JobPublishController@storeFrontJob')->name('store.front.job');

Route::get('edit-job-details/{id}', 'Job\JobPublishController@editFrontJob')->name('edit.front.job');

Route::put('update-front-job/{id}', 'Job\JobPublishController@updateFrontJob')->name('update.front.job');

Route::post('delete-front-job', 'Job\JobPublishController@deleteRequestJob')->name('delete.front.job');

Route::get('job-seekers', 'Job\JobSeekerController@jobSeekersBySearch')->name('job.seeker.list');


Route::post('store-front-job-form-skill', 'Job\JobController@storeJobSkillForm')->name('skill.front.job.add');



Route::post('submit-message', 'Job\SeekerSendController@submit_message')->name('submit-message');



Route::get('subscribe-job-alert', 'SubscriptionController@submitAlert')->name('subscribe.alert');

Route::put('make-not-active-job', 'Job\JobController@makeNotActiveJob')->name('front.make.not.active.job');

Route::put('make-active-job', 'Job\JobController@makeActiveJob')->name('front.make.active.job');
Route::get('jobs', 'Job\JobController@jobsBySearch')->name('job.list');

