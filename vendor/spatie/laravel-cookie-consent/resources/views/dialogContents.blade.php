<div class="js-cookie-consent cookie-consent">
<div class="container">
    <p class="cookie-consent__message">
        {!! get_widget(12)->content !!}
    </p>
	
	<div class="cookiebtn">
    <button class="js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>
	</div>	
</div>
</div>
